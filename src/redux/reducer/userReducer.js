import {
  DATA_LOCATION,
  VALUE_STATE,
  UPDATE_ARRAY,
  REVIEWED_WORDS,
  REVIEWED_WORDS_INDIRECTORY,
  TYPE_WORLD,
  BOOLEAN_VOICE,
  ARRAY_FOR_NEWWOLRD_FOOTER,
  ARAY_NEW_WORLD_TYPE_BLAK,
  SPEAKING_PICTURE,
  SPEAKING_WORD,
  SET_COORECT_ANSWER,
  QUESTION_TYPE,
  LANGUAGE_TYPE,
  WOERD_IN_ANSWER,
  FLASHCARD_CORRECTANSWER,
  WORDS_MAIN,
  TITLE,
  CORECTANWERNEW,
  VALIDATION,
  ANSWER_ALL_TYPE,
  VOIC_FILE_LISTENING,
  TEST,
  VOICE,
  CORRECT_ANSWER,
  ANSWER,
  WORDS,
  QUESTIONS,
  MATCHINGWORD,
  CALL_DATA_WITH_EDITE,
  SETLANGMAIN,
  CHECKPOINT_SKILL_ID,
  LEVEL_LESSON_ID,
  CLEAR_APP,
  TYPE_FOR_NEW_ARRAY,
  NEW_ARRAY_FOOTER_UI,
  WORDLIST,
  CHANGE_LANG_T_WB,
  TYPE_QUESTION_MAIN,
  CLEAR_QUESTION,
  BOOLEAN_VOICE_LISTENING,
} from "../action/types";

const initialState = {
  Questions: [],
  checkpoint_skill_id: {},
  level_lesson_id: {},
  speaking: {},
  picture: "",
  correct_answer: [],
  typeQuestionMain: "",
  answers: [],
  words: [],
  voice: null,
  valueAutocompelte: [],
  valueRevieweWordsIndirectory: [],
  typeVoice: [],
  booleanVoice: false,
  arraynewworldfooter: [],
  nweArrayBlankType: [],
  correctAnswer: [],
  questionType: [],
  typeLanguage: "fa",
  wordsMain: [],
  title: "",
  typeWrld: "other",
  nweCorrectAnswer: [],
  validation: true,
  answerAllType: [],
  wordAnswer: [],
  voiceFileListening: "",
  wordMatching: [],
  editeIcion: "",
  langChangeType: "fa",
  conditionalType: "",
  newArrayFooterUi: [],
  WordList: [],
  changeLang_T_Wb: "",
  voiceBoolean: "",
};

function userReducer(state, action) {
  if (typeof state === "undefined") {
    return initialState;
  }

  switch (action.type) {
    case DATA_LOCATION: {
      return {
        ...state,
        location: action.payload,
      };
    }
    case VALUE_STATE: {
      return {
        ...state,
        answers: [...state.answers, action.payload],
      };
    }
    case UPDATE_ARRAY: {
      return {
        ...state,
        answers: action.payload,
      };
    }
    case REVIEWED_WORDS: {
      return {
        ...state,
        valueAutocompelte: action.payload,
      };
    }
    case REVIEWED_WORDS_INDIRECTORY: {
      return {
        ...state,
        valueRevieweWordsIndirectory: action.payload,
      };
    }
    case TYPE_WORLD: {
      return {
        ...state,
        typeWrld: action.payload,
      };
    }
    case BOOLEAN_VOICE: {
      return {
        ...state,
        booleanVoice: action.payload,
      };
    }
    case ARRAY_FOR_NEWWOLRD_FOOTER: {
      return {
        ...state,
        arraynewworldfooter: action.payload,
      };
    }
    case ARAY_NEW_WORLD_TYPE_BLAK: {
      return {
        ...state,
        nweArrayBlankType: action.payload,
      };
    }
    case SPEAKING_PICTURE: {
      return {
        ...state,
        picture: action.payload,
      };
    }
    case SPEAKING_WORD: {
      return {
        ...state,
        speaking: {
          ...state.speaking,
          word: action.payload,
        },
      };
    }

    case SET_COORECT_ANSWER: {
      return {
        ...state,

        correctAnswer: action.payload,
      };
    }

    case QUESTION_TYPE: {
      return {
        ...state,
        questionType: action.payload,
      };
    }
    case LANGUAGE_TYPE: {
      return {
        ...state,
        typeLanguage: action.payload,
      };
    }
    case WOERD_IN_ANSWER: {
      return {
        ...state,
        wordAnswer: action.payload,
      };
    }
    case FLASHCARD_CORRECTANSWER: {
      return {
        ...state,
        correct_answer: action.payload,
      };
    }
    case WORDS_MAIN: {
      return {
        ...state,
        wordsMain: action.payload,
      };
    }
    case TITLE: {
      return {
        ...state,
        title: action.payload,
      };
    }
    case CORECTANWERNEW: {
      return {
        ...state,
        nweCorrectAnswer: action.payload,
      };
    }
    case VOICE: {
      return {
        ...state,
        voice: action.payload,
      };
    }
    case CORRECT_ANSWER: {
      return {
        ...state,
        correct_answer: action.payload,
      };
    }
    case ANSWER: {
      return {
        ...state,
        answers: action.payload,
      };
    }
    case WORDS: {
      return {
        ...state,
        words: action.payload,
      };
    }
    case VALIDATION: {
      return {
        ...state,
        validation: action.payload,
      };
    }
    case ANSWER_ALL_TYPE: {
      return {
        ...state,
        answerAllType: action.payload,
      };
    }
    case VOIC_FILE_LISTENING: {
      return {
        ...state,
        voiceFileListening: action.payload,
      };
    }

    case QUESTIONS: {
      return {
        ...state,
        Questions: action.payload,
      };
    }
    case MATCHINGWORD: {
      return {
        ...state,
        wordMatching: action.payload,
      };
    }
    case SETLANGMAIN: {
      return {
        ...state,
        langChangeType: action.payload,
      };
    }

    case CHECKPOINT_SKILL_ID: {
      return {
        ...state,
        checkpoint_skill_id: action.payload,
      };
    }
    case LEVEL_LESSON_ID: {
      return {
        ...state,
        level_lesson_id: action.payload,
      };
    }
    case TYPE_FOR_NEW_ARRAY: {
      return {
        ...state,
        conditionalType: action.payload,
      };
    }
    case NEW_ARRAY_FOOTER_UI: {
      return {
        ...state,
        newArrayFooterUi: action.payload,
      };
    }
    case WORDLIST: {
      return {
        ...state,
        WordList: action.payload,
      };
    }
    case CHANGE_LANG_T_WB: {
      return {
        ...state,
        changeLang_T_Wb: action.payload,
      };
    }
    case TYPE_QUESTION_MAIN: {
      return {
        ...state,
        typeQuestionMain: action.payload,
      };
    }
    case CLEAR_QUESTION: {
      return {
        ...state,
        typeQuestionMain: action.payload,
      };
    }
    case BOOLEAN_VOICE_LISTENING: {
      return {
        ...state,
        voiceBoolean: action.payload,
      };
    }
    case CLEAR_APP:
      return {
        ...state,
        nweArrayBlankType: [],
        wordAnswer: [],
        arraynewworldfooter: [],
        checkpoint_skill_id: {},
        level_lesson_id: {},
        speaking: {},

        correct_answer: [],
        answers: [],
        words: [],
        voice: null,
        valueAutocompelte: [],
        valueRevieweWordsIndirectory: [],
        typeVoice: [],
        booleanVoice: false,

        questionType: [],
        correctAnswer: [],
        wordsMain: [],
        title: "",
        typeWrld: "",
        nweCorrectAnswer: [],
        validation: true,
        answerAllType: [],
        typeQuestionMain: "",
        voiceFileListening: "",
        wordMatching: [],
        editeIcion: "",
        langChangeType: "",
        newArrayFooterUi: [],
      };

    default:
      return state;
  }
}

export default userReducer;
