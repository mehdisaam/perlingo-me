import {
  DATA_LOCATION,
  UPDATE_ARRAY,
  VALUE_STATE,
  REVIEWED_WORDS,
  REVIEWED_WORDS_INDIRECTORY,
  TYPE_WORLD,
  BOOLEAN_VOICE,
  ARRAY_FOR_NEWWOLRD_FOOTER,
  ARAY_NEW_WORLD_TYPE_BLAK,
  SPEAKING_PICTURE,
  SPEAKING_WORD,
  SET_COORECT_ANSWER,
  QUESTION_TYPE,
  LANGUAGE_TYPE,
  WOERD_IN_ANSWER,
  FLASHCARD_CORRECTANSWER,
  WORDS_MAIN,
  TITLE,
  CORECTANWERNEW,
  VALIDATION,
  ANSWER_ALL_TYPE,
  LANGUAGE,
  VOIC_FILE_LISTENING,
  QUESTIONS,
  MATCHINGWORD,
  CALL_DATA_WITH_EDITE,
  SETLANGMAIN,
  CHECKPOINT_SKILL_ID,
  LEVEL_LESSON_ID,
  CLEAR_APP,
  TYPE_FOR_NEW_ARRAY,
  NEW_ARRAY_FOOTER_UI,
  WORDLIST,
  CHANGE_LANG_T_WB,
  TYPE_QUESTION_MAIN,
  CLEAR_QUESTION,
  BOOLEAN_VOICE_LISTENING,
} from "./types";

export const setValueState = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: VALUE_STATE, payload });
  };
};
export const updateArray = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: UPDATE_ARRAY, payload });
  };
};
export const setRevieweWords = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: REVIEWED_WORDS, payload });
  };
};
export const setRevieweWordsIndirectory = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: REVIEWED_WORDS_INDIRECTORY, payload });
  };
};
export const setTypeFooter = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: TYPE_WORLD, payload });
  };
};
export const setBooleanViceFooter = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: BOOLEAN_VOICE, payload });
  };
};
export const setArrayNewWorldFooter = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: ARRAY_FOR_NEWWOLRD_FOOTER, payload });
  };
};
export const setArrayNewBlankType = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: ARAY_NEW_WORLD_TYPE_BLAK, payload });
  };
};
export const setSpeakingPic = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: SPEAKING_PICTURE, payload });
  };
};
export const setSpeakingWord = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: SPEAKING_WORD, payload });
  };
};

export const setCorrectAnswer = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: SET_COORECT_ANSWER, payload });
  };
};
export const seQuestion = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: QUESTION_TYPE, payload });
  };
};
export const setLanguageType = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: LANGUAGE_TYPE, payload });
  };
};
export const setWordInAnswer = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: WOERD_IN_ANSWER, payload });
  };
};
export const setFlashcardCorrectAnswer = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: FLASHCARD_CORRECTANSWER, payload });
  };
};
export const setWordsMain = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: WORDS_MAIN, payload });
  };
};
export const setTitle = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: TITLE, payload });
  };
};
export const setCorrectAnswerNew = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: CORECTANWERNEW, payload });
  };
};
export const setCheckpoint_id = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: DATA_LOCATION, payload });
  };
};

export const setValidation = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: VALIDATION, payload });
  };
};
export const setAllAnswer = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: ANSWER_ALL_TYPE, payload });
  };
};
export const setVoiceFile = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: VOIC_FILE_LISTENING, payload });
  };
};
export const setQuestion = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: QUESTIONS, payload });
  };
};
export const setWordMatching = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: MATCHINGWORD, payload });
  };
};
export const setLoadingLang = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: SETLANGMAIN, payload });
  };
};
// Abolfazl
export const setCheckpoint_skill_id = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: CHECKPOINT_SKILL_ID, payload });
  };
};
export const setLevel_lesson_id = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: LEVEL_LESSON_ID, payload });
  };
};
export const setEmptyArray = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: CLEAR_APP, payload });
  };
};
export const setTypeForFotterArray = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: TYPE_FOR_NEW_ARRAY, payload });
  };
};
export const setNewArrayFooterUi = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: NEW_ARRAY_FOOTER_UI, payload });
  };
};
export const setWORDLIST = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: WORDLIST, payload });
  };
};
export const setlangFor_T_Wb = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: CHANGE_LANG_T_WB, payload });
  };
};
export const settypeMainQuestion = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: TYPE_QUESTION_MAIN, payload });
  };
};
export const setClearQuestion = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: CLEAR_QUESTION, payload });
  };
};
export const setVoiseBoolean = (payload) => {
  return async (dispatch) => {
    await dispatch({ type: BOOLEAN_VOICE_LISTENING, payload });
  };
};
