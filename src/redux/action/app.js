import { CLEAR_APP } from "./types";
export const clearApp = () => ({
  type: CLEAR_APP,
});
