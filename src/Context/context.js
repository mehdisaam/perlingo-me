import React, { useState, useEffect } from "react";
export const AouthProviderContext = React.createContext();
export const AouthProviderContextDispatch = React.createContext();

function AouthProvider({ children }) {
  const [state, setState] = useState("ssss");

  return (
    <>
      <AouthProviderContext.Provider value={state}>
        <AouthProviderContextDispatch.Provider value={setState}>
          {children}
        </AouthProviderContextDispatch.Provider>
      </AouthProviderContext.Provider>
    </>
  );
}
export default AouthProvider;
