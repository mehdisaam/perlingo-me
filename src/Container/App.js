import React from "react";
import RouteMain from "../Router/RouteMain";

function App() {
  return (
    <div>
      <RouteMain />
    </div>
  );
}

export default App;
