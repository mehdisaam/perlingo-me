import React, { useState, useEffect } from "react";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import router from "./router";
const RouteMain = () => {
  const [showScrollTop, setShowScrollTop] = useState(false);
  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  window.addEventListener("scroll", function () {
    if (window.pageYOffset >= 700) {
      setShowScrollTop(true);
    } else {
      setShowScrollTop(false);
    }
  });

  return (
    <>
      <Router>
        <Switch>
          {router.map((route) => {
            return (
              <Route
                key={route.path}
                path={route.path}
                component={route.component}
                exact={typeof route.exact === "undefined" ? true : route.exact}
              />
            );
          })}
        </Switch>
        {showScrollTop && (
          <button className="btn btn_scrollTop pointer" onClick={scrollTop}>
            <ArrowUpwardIcon fontSize="large" />
          </button>
        )}
      </Router>
    </>
  );
};

export default RouteMain;
