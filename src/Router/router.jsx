import Header from "../Component/Header/Header";
import MainStationManage from "../Component/ManageStation";
import Flashcart from "../Component/QuestiopnMain";
import Listening from "../Component/QuestiopnMain/Listening/Listening";
import Wordbank from "../Component/WordBank/Wordbank";
import Dashboard from "../Component/Dashboard/Dashboard";
import UserManagement from "../Component/User/User";
import Login from "../Component/Login/Login";
import BadgeManagement from '../Component/Badge/Badge'
import test from '../Component/AddWord/Addword'
export default [
  {
    path: "/",
    component: MainStationManage,
    title: "",
  },
  {
    path: "/question",
    component: Flashcart,
    title: "flashcart",
  },
  {
    path: "/UserManagement",
    component: UserManagement,
    title: "UserManagement",
  },
  {
    path: "/Wordbank",
    component: Wordbank,
    title: "بانک لغات",
  },
  {
    path: "/dashboard",
    component: Dashboard,
    title: "Dashboard",
  },
  {
    path: "/login",
    component: Login,
    title: "Login",
  },
   {
    path: "/BadgeManagement",
    component: BadgeManagement,
    title: "BadgeManagement",
  },
  {
    path: "/test",
    component: test,
    title: "BadgeManagement",
  },
];
