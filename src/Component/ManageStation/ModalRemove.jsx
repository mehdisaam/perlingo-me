import React, { useState } from "react";
import Modal from "react-modal";
import CloseIcon from "@material-ui/icons/Close";
const ModalRemove = (props) => {
  return (
    <div className="testChild">
      <h2>{props.header}</h2>
      <p>{props.content}</p>
      <div className="button-group">
        <button type="button" onClick={props.submitClick}>
          بله
        </button>
        <button onClick={props.closeModal} type="button">
          خیر
        </button>
      </div>
    </div>
  );
};

export default ModalRemove;
