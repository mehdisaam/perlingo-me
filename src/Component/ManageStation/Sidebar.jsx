import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Modal from "react-modal";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

// Icons
import CreateIcon from "@material-ui/icons/Create";
import SettingsIcon from "@material-ui/icons/Settings";
import DeleteIcon from "@material-ui/icons/Delete";
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import AddIcon from "@material-ui/icons/Add";
import DoneIcon from "@material-ui/icons/Done";
// import styled from "styled-components";
import LoadingMain from "../Loading/Loading";
// Images
import add_skill from "../../assets/img/add-skill.png";
import No_Image from "../../assets/img/No_Image.png";
import checkpoint_image from "../../assets/img/battlementC.svg";
import loading from "../../assets/img/loading.gif";
import { setCheckpoint_skill_id } from "../../redux/action/user";
import { useDispatch } from "react-redux";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router";

Modal.setAppElement("#root");

// const Container = styled.div`
//   border: 1px solid lightgrey;
//   border-radius: 2px;
//   padding: 8px;
//   margin-bottom: 8px;
//   transition: background-color 0.2s ease;
//   background-color: ${(props) =>
//     props.isDragDisabled
//       ? "lightgrey"
//       : props.isDragging
//         ? "lightgreen"
//         : "white"};
// `;
export default function Sidebar(props) {
  const history = useHistory();
  //////////////////////////////////////////////////////Modal//////////////////////////////////////////////////////
  const dispatch = useDispatch();
  //open and close add modal
  const [addModalIsOpen, setAddModalIsOpen] = useState(false);
  const [loadingPageBeforeCallApi, setLoadingPageBeforeCallApi] =
    useState(false);
  //open add modal function
  function openAddModal() {
    setAddModalIsOpen(true);
  }

  //close add modal function
  function closeAddModal() {
    setAddModalIsOpen(false);
    setLableIsFull(true);
  }

  ////////////////////////////////////////////////////////////////////////////open and close Edit modal
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);

  //open add modal function
  function openEditModal() {
    setEditModalIsOpen(true);
  }

  //close add modal function
  function closeEditModal() {
    setEditModalIsOpen(false);
    setLableIsFull(true);
    // setlogo(null);
    // setanimation(null);
    // setLable(null);
    // setLogoPreview(null);
    // setAnimationPreview(null);
  }

  ////////////////////////////////////////////////////////////////////////////open and close remove modal
  const [removeModalIsOpen, setRemoveModalIsOpen] = useState(false);

  // open Remove modal function
  function openRemoveModal() {
    setRemoveModalIsOpen(true);
  }

  //close add modal function
  function closeRemoveModal() {
    setRemoveModalIsOpen(false);
  }

  ///////////////////////////////////////////////////////////////////////////////loading

  const [loadingIsOn, setloadingIsOn] = useState(false);

  // open loading function
  function openloading() {
    setloadingIsOn(true);
  }

  //close loading function
  function closeloading() {
    setloadingIsOn(false);
  }

  // /////////////////////////////////////////////////////////////remove checkpoint
  const [removeCheckpoint, setRemoveCheckpoint] = useState(false);

  // open loading function
  function openRemoveCheckpoint() {
    setRemoveCheckpoint(true);
  }

  //close loading function
  function closeRemoveCheckpoint() {
    setRemoveCheckpoint(false);
  }

  const RcheckpontStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "max-content",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  ///////////////////////////////////////////////////////////////////////////////loading

  const [errorModal, setErrorModal] = useState(false);

  // open loading function
  function openErrorModal() {
    setErrorModal(true);
  }

  //close loading function
  function closeErrorModal() {
    setErrorModal(false);
  }

  const ErrorStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "5000",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "max-content",
      height: "max-content",
      zIndex: "5000",
      borderRadius: "30px",
      color: "#721c24",
      backgroundColor: "#f8d7da",
      borderColor: "#f5c6cb",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  //modal styles
  const customStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "max-content",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  const RemoveStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "220px",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  const loadingStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "5000",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "155px",
      backgroundColor: "transparent",
      border: "none",
      zIndex: "5000",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };
  //////////////////////////////////////////////////////Functions//////////////////////////////////////////////////////

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }
    if (destination !== null) {
      testFuncMehdiOne(destination);
    }
  };

  // const onDragOver = (e) => {
  //   e.preventDefault();
  // };

  // let _id = "";

  // const onDragStart = (e, drag_skill_id) => {
  //   _id = drag_skill_id;
  //   e.dataTransfer.setData("drag_skill_id", drag_skill_id);
  // };

  // const onDrop = (e, chpriority, index) => {
  //   let id = e.dataTransfer.getData("drag_skill_id");
  //   if (checkPoint_id === skillData[index]._id) {
  //     setloadingIsOn(true);
  //     axios
  //       .put(`http://46.227.68.244:3050/api/v1/admin/change/skill`, {
  //         priority: chpriority,
  //         checkpoint_id: checkPoint_id,
  //         skill_id: skill_id,
  //       })
  //       .then((res) => {
  //         return setSkillData(res.data.data), setloadingIsOn(false);
  //       })
  //       .catch((err) => (err));

  //     // for(let i=0; i<skillData.length; i++){
  //     //  let index =  skillData[i].skills.findIndex( item => item._id === _id))
  //     //     return index
  //     // }
  //   } else {
  //     return (
  //       setError("شما نمی توانید اسکیل ها را بین چک پوینت ها جا به جا کنید."),
  //       setErrorModal(true)
  //     );
  //   }
  // };

  // const onDrop1 = (e, index1) => {
  //   let id = e.dataTransfer.getData("drag_skill_id");
  //   let chpriority =
  //     skillData[index1].skills.length > 0
  //       ? skillData[index1].skills[skillData[index1].skills.length - 1]
  //         .priority + 1
  //       : 1;

  //   if (checkPoint_id === skillData[index1]._id) {
  //     setloadingIsOn(true);
  //     axios
  //       .put(`http://46.227.68.244:3050/api/v1/admin/change/skill`, {
  //         priority: chpriority,
  //         checkpoint_id: checkPoint_id,
  //         skill_id: skill_id,
  //       })
  //       .then((res) => {
  //         return setSkillData(res.data.data), setloadingIsOn(false);
  //       })
  //       .catch((err) => (err));

  //     // for(let i=0; i<skillData.length; i++){
  //     //  let index =  skillData[i].skills.findIndex( item => item._id === _id))
  //     //     return index
  //     // }
  //   } else {
  //     return (
  //       setError("شما نمی توانید اسکیل ها را بین چک پوینت ها جا به جا کنید."),
  //       setErrorModal(true)
  //     );
  //   }
  // };
  // const onDrop = (e, chpriority) => {
  //     let id = e.dataTransfer.getData("drag_skill_id")
  //     setloadingIsOn(true)
  //     axios.put(`http://46.227.68.244:3050/api/v1/admin/change/skill`,
  //         {
  //             priority: chpriority,
  //             checkpoint_id: checkPoint_id,
  //             skill_id: skill_id

  //         }).then(res => {
  //             return setSkillData(res.data.data),
  //                 setloadingIsOn(false)
  //         })
  //         .catch(err => (err))

  //     // for(let i=0; i<skillData.length; i++){
  //     //  let index =  skillData[i].skills.findIndex( item => item._id === _id))
  //     //     return index
  //     // }
  // }

  // pass chekpoint id and skill id to content station

  // clear state after res
  const clearState = () => {
    setCheckPoint_id(null);
    setlogo(null);
    setanimation(null);
    setLable(null);
    setPriority(null);
    setLogoPreview(null);
    setAnimationPreview(null);
  };

  //setstate input text
  const textInputHandler = (e) => {
    setLable(e.target.value);
  };

  //setstate image
  const changefile1handler = (e) => {
    if (e.target.files[0] !== undefined) {
      return (
        setlogo(e.target.files[0]),
        setLogoPreview(URL.createObjectURL(e.target.files[0]))
      );
    }
  };

  //setstate animation
  const changefile2handler = (e) => {
    if (e.target.files[0] !== undefined) {
      return (
        setanimation(e.target.files[0]),
        setAnimationPreview(URL.createObjectURL(e.target.files[0]))
      );
    }
  };

  //add data to row
  const addDataSkillRow = (index1, indexlevels) => {
    setCheckPoint_id(skillData[index1]._id);
    setPriority(parseInt(indexlevels));
  };

  //submitform
  const handleSubmitAddForm = (e) => {
    if (e.keyCode === 13) {
      addskill();
    }
  };

  const handleSubmitEditForm = (e) => {
    if (e.keyCode === 13) {
      Editskill();
    }
  };

  //add new row
  const setDataSkillRow = (index1) => {
    setCheckPoint_id(skillData[index1]._id);
    let x = skillData[index1].skills.length;
    x > 0
      ? setPriority(parseInt(skillData[index1].skills[x - 1].priority + 1))
      : setPriority(parseInt(1));
  };

  //////////////////////////////////////////////////////Api//////////////////////////////////////////////////////

  // get level of skill for levelcontent component
  const getlevelskill = () => {
    setloadingIsOn(true);
    axios
      .post(
        `${bseurl}/admin/Skill?api_token=${localStorage.getItem("token")}`,
        {
          checkpoint_id: checkPoint_id,
          skill_id: skill_id,
        }
      )
      .then(
        (res) => {
          props.setskill_data(res.data.data);
          setloadingIsOn(false);
          setLoadingPageBeforeCallApi(false);
          props.executeScroll();
        }
      )
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  ////////////////////////////////////////////////////////Add Checkpoint

  const AddCheckpoint = () => {
    setloadingIsOn(true);
    axios
      .post(
        `${bseurl}/admin/checkpoint?api_token=${localStorage.getItem("token")}`,
        {
          priority: skillData[skillData.length - 1].priority + 1,
        }
      )
      .then((res) => {
        return setSkillData(res.data.data), setloadingIsOn(false);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const deleteCheckpointfunc = (chekpointid) => {
    setloadingIsOn(true);
    axios
      .delete(
        `${bseurl}/admin/checkpoint/${chekpointid}?api_token=${localStorage.getItem(
          "token"
        )}`
      )
      .then((res) => {
        return setSkillData(res.data.data), setloadingIsOn(false);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };
  ///////////////////////////////////////////delete skill
  const DeleteskillApi = () => {
    setloadingIsOn(true);
    axios
      .post(
        `${bseurl}/admin/skillDelete?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          id: skill_id,
          checkpoint_id: checkPoint_id,
        }
      )
      .then((res) => {
        return setSkillData(res.data.data), setloadingIsOn(false);
      })
      .catch((err) => {
        return err.response.data.status === 401 ? history.push("/login") : null;
      });
    setCheckPoint_id(null);
    setSkill_id(null);
  };

  //////////////////////////////////////////////add skill
  const addskill = () => {
    if (lable === null || lable === "") {
      setLableIsFull(false);
    }
    if (lable && lable.length > 0) {
      setLableIsFull(true);
      setloadingIsOn(true);
      let data = new FormData();
      data.append("lable", lable);
      data.append("checkpoint_id", checkPoint_id);
      data.append("priority", priority);
      data.append("logo", logo);
      data.append("animation", animation);

      let headers = { "Content-Type": "multipart/form-data" };
      axios
        .post(
          `${bseurl}/admin/checkpoint/Skill?api_token=${localStorage.getItem(
            "token"
          )}`,
          data,
          headers
        )
        .then((res) => {
          return (
            setSkillData(res.data.data),
            setloadingIsOn(false),
            setAddModalIsOpen(false)
          );
          clearState();
        })
        .catch((error) => {
          return (
            setloadingIsOn(false),
            setError(error.response.data.data[0]),
            setErrorModal(true),
            error.response.data.status === 401 ? history.push("/login") : null
          );
        });
    }
  };

  /////////////////////////////////////////////////delete checkpoint

  ///////////////////////////get skill data For preview content

  const getSingleSkill = () => {
    setloadingIsOn(true);
    axios
      .post(
        `${bseurl}/admin/Skill?api_token=${localStorage.getItem("token")}`,
        {
          checkpoint_id: checkPoint_id,
          skill_id: skill_id,
        }
      )
      .then((res) => {
        return (
          setSingleSkill(res.data.data.skills),
          setlogo(res.data.data.skills[0].logo),
          setanimation(res.data.data.skills[0].animation),
          setLable(res.data.data.skills[0].lable),
          setloadingIsOn(false)
        );
      })
      .catch((err) => {
        return err.response.data.status === 401 ? history.push("/login") : null;
      });
  };

  //////////////////////////////////// Send New Data
  const Editskill = () => {
    if (lable === null || lable === "") {
      setLableIsFull(false);
    }
    if (lable && lable.length > 0) {
      setLableIsFull(true);
      setloadingIsOn(true);
      let data = new FormData();
      data.append("lable", lable);
      data.append("checkpoint_id", checkPoint_id);
      data.append("priority", priority);
      data.append("logo", logo);
      data.append("animation", animation);
      data.append("skill_id", skill_id);

      let headers = { "Content-Type": "multipart/form-data" };
      axios
        .put(
          `${bseurl}/admin/checkpoint/Skill?api_token=${localStorage.getItem(
            "token"
          )}`,
          data,
          headers
        )
        .then((res) => {
          return (
            setSkillData(res.data.data),
            setloadingIsOn(false),
            setEditModalIsOpen(false),
            clearState()
          );
        })
        .catch((err) => {
          return (
            setloadingIsOn(false),
            setError(err?.response?.data?.data[0]),
            setErrorModal(true),
            err.response.data.status === 401 ? history.push("/login") : null
          );
        });
    }
  };

  ///////////////////////////////////////////////////////////////////////////////get data before render
  const getCheckpointData = () => {
    setloadingIsOn(true)
    axios
      .get(
        `${bseurl}/admin/checkpoint/list?api_token=${localStorage.getItem(
          "token"
        )}`
      )
      .then((res) => {
        setSkillData(res.data.data); setloadingIsOn(false)
          ; props.setShow(true)
      })
      .catch((err) => {
        return err.response.data.status === 401 ? history.push("/login") : null;
      });
    if (skillData === "") {
      setLoadingPageBeforeCallApi(false);
    }
  };
  // useEffect(() => {}, []);
  const [testMehdi, setTestMehdi] = useState();

  const testFuncMehdiOne = (destination) => {
    setloadingIsOn(true);
    if (parseInt(destination.droppableId.substring(5)) === 0) {
      setloadingIsOn(false);
      setError("شما نمیتوانید بیشتر از سه اسکیل اضافه کنید");
      setErrorModal(true);
    } else {
      axios
        .put(
          `${bseurl}/admin/change/skill?api_token=${localStorage.getItem(
            "token"
          )}`,
          {
            priority: parseInt(destination.droppableId.substring(5)),
            checkpoint_id: checkPoint_id,
            skill_id: skill_id,
          }
        )
        .then((res) => {
          setloadingIsOn(false);
          return setSkillData(res.data.data);
        })
        .catch((err) => {
          if (err.response.status === 400) {
            setloadingIsOn(false);
            setError("شما نمیتوانید بیشتر از سه اسکیل اضافه کنید");
            setErrorModal(true);
          } else if (err.response.status !== 200) {
            setloadingIsOn(false);
            setError("دوباره امتحان کنید");
            setErrorModal(true);
          }
          return err.response.data.status === 401
            ? history.push("/login")
            : null;
        });
    }
  };
  //////////////////////////////////////////////////////HOOK//////////////////////////////////////////////////////

  ///////////////////////////////////data
  let [skillData, setSkillData] = useState("");
  ///////////////////////////////////hover for show skill setting
  let [skillHover, setSkillHover] = useState("");

  let [hoverUpImg, setHoverUpImg] = useState("");

  let [hoverUpAnim, setHoverUpAnim] = useState("");

  let [logo, setlogo] = useState(null);

  let [logoPreview, setLogoPreview] = useState();

  let [animation, setanimation] = useState(null);

  let [animationPreview, setAnimationPreview] = useState();

  let [checkPoint_id, setCheckPoint_id] = useState(null);

  let [lable, setLable] = useState(null);

  let [lableIsFull, setLableIsFull] = useState(true);

  let [priority, setPriority] = useState(null);

  let [skill_id, setSkill_id] = useState(null);

  let [singleSkill, setSingleSkill] = useState();
  ///////////////////////////////////filter priority
  let [sortSkillData, setSortSkillData] = useState([]);

  /////////////////////////////////////checkpoint state/////////////////////////////////////////
  let [lableCheckpoint, setLableCheckpoint] = useState();

  let [skillsOfChekpoint, setSkillsOfChekpoint] = useState();

  let [error, setError] = useState("");

  let [deleteCheckpoint, setDeleteCheckpoint] = useState("");

  const getlevels = (checkPoint, skill) => {
    if (checkPoint_id != null && skill_id != null) {
      return (
        props.setS_id(skill),
        props.setCp_id(checkPoint),
        getlevelskill(),
        sessionStorage.setItem("checkpoint", checkPoint),
        sessionStorage.setItem("skill", skill)
      );
    }
  };

  const textInputHandlerCheckpoint = (e) => {
    setLableCheckpoint(e.target.value);
  };

  useEffect(() => {
    getCheckpointData();
  }, []);
  console.log(skillData);
  return (
    <>
      {/* 

        first map for maping checkpoints
        second map for maping array of filter data
        third map for maping skills in row
        
        */}
      {loadingPageBeforeCallApi ? <LoadingMain /> : null}


      {
        props.show ?

          <div className="col-12 col-lg-4">
            {/* 1.maping check point  */}

            {skillData &&
              skillData.map((item, index1) => {
                // filter priority
                sortSkillData = item.skills;
                let levels = [];
                for (let i = 0; i < sortSkillData.length; i++) {
                  let intern_levels = [];
                  intern_levels.push(sortSkillData[i]);
                  if (sortSkillData[i].priority !== "") {
                    if (i + 1 < sortSkillData.length) {
                      if (
                        sortSkillData[i].priority === sortSkillData[i + 1].priority
                      ) {
                        intern_levels.push(sortSkillData[i + 1]);
                        i++;
                      }
                      if (i + 1 < sortSkillData.length) {
                        if (
                          sortSkillData[i].priority ===
                          sortSkillData[i + 1].priority
                        ) {
                          intern_levels.push(sortSkillData[i + 1]);
                          i++;
                        }
                      }
                    }

                    let obj = {
                      intern_levels,
                      priority: sortSkillData[i].priority,
                    };
                    levels.push(obj);
                  }
                }

                return (
                  //2.map array filter data

                  <div key={index1} className="skill_box mb-5">
                    <DragDropContext onDragEnd={onDragEnd}>
                      <div className="d-flex justify-content-center align-items-center">
                        {index1 === 0 ? (
                          <div className="title_box">
                            <span className="mb-2">شروع</span>
                          </div>
                        ) : (
                          <>
                            <div
                              className="img_box"
                              onMouseLeave={() => {
                                setDeleteCheckpoint("");
                              }}
                            >
                              <img
                                onMouseEnter={() => {
                                  setDeleteCheckpoint(index1);
                                }}
                                className="img_w"
                                src={checkpoint_image}
                                alt=""
                              />
                              {deleteCheckpoint === index1 ? (
                                <div className="index_priority">
                                  <CloseIcon
                                    onClick={() => {
                                      return (
                                        setCheckPoint_id(skillData[index1]._id),
                                        setSkillsOfChekpoint(
                                          skillData[index1].skills.length
                                        ),
                                        setRemoveCheckpoint(true)
                                      );
                                    }}
                                    color="secondary"
                                    className="pointer"
                                  />
                                </div>
                              ) : (
                                <div className="index_priority">{index1}</div>
                              )}
                            </div>
                          </>
                        )}

                        <div className="row d-flex justify-content-center align-items center mt-5 pt-3 pb-5 w-100">
                          {/* {levels[indexlevels].intern_levels.length === 3 ? "index0" : `index${item.priority}`} */}
                          {levels.map((item, indexlevels) => {
                            return (
                              <>
                                <Droppable
                                  droppableId={`index${item.priority}`}
                                  direction="horizontal"
                                >
                                  {(provided, snapshot) => (
                                    <>
                                      <div
                                        ref={provided.innerRef}
                                        {...provided.droppableProps}
                                        key={indexlevels}
                                        className="set_skill_box my-2"
                                      >
                                        {levels[indexlevels].intern_levels.map(
                                          (item, index2) => {
                                            return (
                                              <>
                                                <Draggable
                                                  key={`${item._id}`}
                                                  draggableId={`${item._id}`}
                                                  index={item.priority}
                                                >
                                                  {(provided, snapshot) => (
                                                    <div
                                                      {...provided.draggableProps}
                                                      ref={provided.innerRef}
                                                      key={item._id}
                                                      {...provided.dragHandleProps}
                                                    >
                                                      <div
                                                        className="bg_skill_item mx-2"
                                                        onMouseEnter={() => {
                                                          return (
                                                            setSkillHover(item._id),
                                                            setCheckPoint_id(
                                                              skillData[index1]._id
                                                            ),
                                                            setSkill_id(item._id)
                                                          );
                                                        }}
                                                      >
                                                        {item.logo &&
                                                          item.logo != " " ? (
                                                          <img
                                                            className="skill_item"
                                                            src={`http://46.227.68.244:3050/${item.logo}`}
                                                            alt="skill_item"
                                                          />
                                                        ) : (
                                                          <img
                                                            className="skill_item"
                                                            src={No_Image}
                                                            alt="skill_item"
                                                          />
                                                        )}
                                                        {skillHover === item._id ? (
                                                          <div
                                                            className="skill_setting"
                                                            onMouseLeave={() => {
                                                              return setSkillHover(
                                                                ""
                                                              );
                                                            }}
                                                          >
                                                            <CreateIcon
                                                              onClick={() => {
                                                                return (
                                                                  setPriority(
                                                                    item.priority
                                                                  ),
                                                                  getSingleSkill(),
                                                                  openEditModal()
                                                                  // props.setLoadingPageBeforeCallApi(
                                                                  //   true
                                                                  // )
                                                                );
                                                              }}
                                                              className="pointer green_icon"
                                                            />

                                                            <SettingsIcon
                                                              onClick={() => {
                                                                getlevels(
                                                                  checkPoint_id,
                                                                  skill_id
                                                                );
                                                              }}
                                                              className="pointer"
                                                            />

                                                            <CloseIcon
                                                              onClick={() =>
                                                                openRemoveModal()
                                                              }
                                                              color="secondary"
                                                              className="pointer"
                                                            />
                                                          </div>
                                                        ) : null}
                                                      </div>

                                                      <div className="d-flex justify-content-center mt-1">
                                                        <span className="style_lable">
                                                          {item.lable}
                                                        </span>
                                                      </div>
                                                    </div>
                                                  )}
                                                </Draggable>
                                              </>
                                            );
                                          }
                                        )}
                                        {provided.placeholder}
                                        {/* add button for add data in row */}
                                        {levels[indexlevels].intern_levels
                                          .length === 3 ? null : (
                                          <div
                                            className="pointer"
                                            onClick={() => {
                                              return (
                                                openAddModal(),
                                                clearState(),
                                                addDataSkillRow(
                                                  index1,
                                                  levels[indexlevels].priority
                                                )
                                              );
                                            }}
                                          >
                                            {/* style should be remove */}
                                            <img
                                              style={{
                                                width: "110px",
                                                height: "110px",
                                              }}
                                              src={add_skill}
                                              alt=""
                                            />
                                            <div className="p-07"></div>
                                          </div>
                                        )}
                                      </div>
                                    </>
                                  )}
                                </Droppable>
                                {levels.length === indexlevels + 1 && (
                                  <Droppable
                                    droppableId={`index${levels[levels.length - 1].priority + 1
                                      }`}
                                    direction="horizontal"
                                  >
                                    {(provided, snapshot) => (
                                      <>
                                        <div
                                          ref={provided.innerRef}
                                          {...provided.droppableProps}
                                          key={indexlevels}
                                          data-rbd-droppable-id="index7"
                                          className="set_skill_box my-2"
                                        >
                                          <div
                                            className="pointer"
                                            onClick={() => {
                                              return (
                                                openAddModal(),
                                                clearState(),
                                                setDataSkillRow(index1)
                                              );
                                            }}
                                          >
                                            {/* style should be remove */}
                                            <img
                                              style={{
                                                width: "110px",
                                                height: "110px",
                                              }}
                                              src={add_skill}
                                              alt=""
                                            />
                                          </div>
                                          {provided.placeholder}
                                        </div>
                                      </>
                                    )}
                                  </Droppable>
                                )}
                              </>
                            );
                          })}
                          {/* add button for add new row */}
                          {skillData[index1].skills.length === 0 && (
                            <>
                              <div className="set_skill_box my-2">
                                <div
                                  className="pointer"
                                  onClick={() => {
                                    return (
                                      openAddModal(),
                                      clearState(),
                                      setDataSkillRow(index1)
                                    );
                                  }}
                                >
                                  {/* style should be remove */}
                                  <img
                                    style={{
                                      width: "110px",
                                      height: "110px",
                                    }}
                                    src={add_skill}
                                    alt=""
                                  />
                                </div>
                              </div>
                            </>
                          )}
                        </div>
                      </div>
                    </DragDropContext>
                  </div>
                );
              })}
            <div className="skill_box mb-5">
              <div className="d-flex justify-content-center align-items-center">
                <div
                  className="title_box pointer"
                  onClick={() => {
                    return AddCheckpoint();
                  }}
                >
                  <AddIcon fontSize="large" className="text_gray" />
                </div>
                <div className="py-5"></div>
              </div>
            </div>

            {/* //////////////////////////////////////////////////////Add modal////////////////////////////////////////////////////// */}

            <Modal
              isOpen={addModalIsOpen}
              onRequestClose={closeAddModal}
              style={customStyles}
              closeTimeoutMS={200}
            >
              {/* content */}
              <div
                className="outline-none"
                onKeyDown={(e) => handleSubmitAddForm(e)}
                tabIndex="0"
              >
                <div className="row d-flex justify-content-between align-items center px-4">
                  <div>
                    <CloseIcon
                      fontSize="large"
                      className="pointer"
                      onClick={closeAddModal}
                    />
                    <span className="mr-2 text_bold fs_xs_14px">
                      ثبـت مهـارت جدید
                </span>
                  </div>
                  <div onClick={addskill} className="pointer">
                    <SaveOutlinedIcon fontSize="large" />
                    <span className="text_bold d-none d-md-inline">ذخیره</span>
                  </div>
                </div>
                <div class="form-group mt-4">
                  <input
                    type="text"
                    onChange={(e) => textInputHandler(e)}
                    className="form-control input_modal_border"
                    placeholder="عنوان مهارت"
                    autoFocus={true}
                  />
                </div>
                <div className="row d-flex justify-content-around align-items-center">
                  <div className="text-center">
                    <span className="text_bold d-block">تصویر ثابت</span>
                    <label
                      className="d-flex justify-content-center align-items-center modal_add_img pointer"
                      for="file1"
                      onMouseEnter={() =>
                        logoPreview != null ? setHoverUpImg(logoPreview) : null
                      }
                    >
                      {logoPreview != null ? (
                        logo?.name?.split(".").pop() === "jpg" ||
                          logo?.name?.split(".").pop() === "png" ? (
                          <img
                            className="pv_img_modal pointer"
                            src={logoPreview}
                            alt="skill_item"
                          />
                        ) : (
                          <>
                            <img
                              className="pv_img_modal"
                              src={No_Image}
                              alt="skill_item"
                            />
                          </>
                        )
                      ) : (
                        <AddIcon fontSize="large" />
                      )}

                      {hoverUpImg === logoPreview ? (
                        <div
                          className="hover_add_img"
                          onMouseLeave={() => setHoverUpImg("")}
                        >
                          <CloseIcon />
                        </div>
                      ) : null}
                    </label>

                    <input
                      id="file1"
                      type="file"
                      onChange={(e) => changefile1handler(e)}
                      accept=".jpg , .png"
                      hidden
                    />
                  </div>
                  <div className="text-center">
                    <span className="text_bold d-block">تصویر متحرک </span>
                    <label
                      className="d-flex justify-content-center align-items-center modal_add_img pointer"
                      for="file2"
                      onMouseEnter={() =>
                        animationPreview != null
                          ? setHoverUpAnim(animationPreview)
                          : null
                      }
                    >
                      {animationPreview != null ? (
                        animation?.name?.split(".").pop() === "gif" ? (
                          <img
                            className="pv_img_modal pointer"
                            src={animationPreview}
                            alt="skill_item"
                          />
                        ) : (
                          <>
                            <img
                              className="pv_img_modal"
                              src={No_Image}
                              alt="skill_item"
                            />
                          </>
                        )
                      ) : (
                        <AddIcon fontSize="large" />
                      )}

                      {hoverUpAnim === animationPreview ? (
                        <div
                          className="hover_add_img"
                          onMouseLeave={() => setHoverUpAnim("")}
                        >
                          <CloseIcon />
                        </div>
                      ) : null}
                    </label>

                    <input
                      id="file2"
                      type="file"
                      onChange={(e) => changefile2handler(e)}
                      accept=".gif"
                      hidden
                    />
                  </div>
                </div>
                {/* error input */}
                {lableIsFull ? null : (
                  <div className="row justify-content-center">
                    <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                      <span>عنوان مهـارت را وارد کنید</span>
                    </div>
                  </div>
                )}

                {/* erorr files */}
                {logo && logo != " " ? (
                  logo.name ? (
                    logo.name.split(".").pop() === "jpg" ||
                      logo.name.split(".").pop() === "png" ? null : (
                      <div className="row justify-content-center">
                        <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                          <span>پسوند تصویر ثابت نادرست است</span>
                        </div>
                      </div>
                    )
                  ) : logo.split(".").pop() === "jpg" ||
                    logo.split(".").pop() === "png" ? null : (
                    <div className="row justify-content-center">
                      <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                        <span>پسوند تصویر ثابت نادرست است</span>
                      </div>
                    </div>
                  )
                ) : null}
                {animation && animation != " " ? (
                  animation.name ? (
                    animation.name.split(".").pop() === "gif" ? null : (
                      <div className="row justify-content-center">
                        <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                          <span>پسوند تصویر متحرک نادرست است</span>
                        </div>
                      </div>
                    )
                  ) : animation.split(".").pop() === "gif" ? null : (
                    <div className="row justify-content-center">
                      <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                        <span>پسوند تصویر متحرک نادرست است</span>
                      </div>
                    </div>
                  )
                ) : null}
              </div>
            </Modal>

            {/* //////////////////////////////////////////////////////Edit modal////////////////////////////////////////////////////// */}

            {singleSkill && loadingIsOn === false && (
              <Modal
                isOpen={editModalIsOpen}
                onRequestClose={closeEditModal}
                style={customStyles}
                closeTimeoutMS={200}
              >
                {/* content */}
                <div
                  className="outline-none"
                  onKeyDown={(e) => handleSubmitEditForm(e)}
                  tabIndex="0"
                >
                  <div className="row d-flex justify-content-between align-items center px-4">
                    <div>
                      <CloseIcon
                        fontSize="large"
                        className="pointer"
                        onClick={closeEditModal}
                      />
                      <span className="mr-2 text_bold fs_xs_14px">
                        ویرایش مهـارت
                  </span>
                    </div>
                    <div onClick={Editskill} className="pointer">
                      <SaveOutlinedIcon fontSize="large" />
                      <span className="text_bold d-none d-md-inline">ذخیره</span>
                    </div>
                  </div>
                  <div class="form-group mt-4">
                    <input
                      autoFocus={true}
                      type="text"
                      onChange={(e) => textInputHandler(e)}
                      className="form-control input_modal_border"
                      value={lable}
                    />
                  </div>

                  <div className="row d-flex justify-content-around align-items-center">
                    <div className="text-center">
                      <span className="text_bold d-block">تصویر ثابت</span>
                      <label
                        className="d-flex justify-content-center align-items-center modal_add_img pointer"
                        for="file1"
                        onMouseEnter={() =>
                          logoPreview != null
                            ? setHoverUpImg(logoPreview)
                            : logo && logo != " " && logo != undefined
                              ? setHoverUpImg(logo)
                              : setHoverUpImg(" ")
                        }
                      >
                        {logoPreview != null ? (
                          logo?.name?.split(".").pop() === "jpg" ||
                            logo?.name?.split(".").pop() === "png" ? (
                            <img
                              className="pv_img_modal"
                              src={logoPreview}
                              alt="skill_item"
                            />
                          ) : (
                            <img
                              className="pv_img_modal"
                              src={No_Image}
                              alt="skill_item"
                            />
                          )
                        ) : logo && logo != " " ? (
                          <img
                            className="pv_img_modal"
                            src={`http://46.227.68.244:3050/${logo}`}
                            alt="skill_item"
                          />
                        ) : (
                          <img
                            className="pv_img_modal"
                            src={No_Image}
                            alt="skill_item"
                          />
                        )}

                        {hoverUpImg === logoPreview ||
                          hoverUpImg === logo ||
                          hoverUpImg === " " ? (
                          <div
                            className="hover_add_img"
                            onMouseLeave={() => setHoverUpImg("")}
                          >
                            <CloseIcon />
                          </div>
                        ) : null}
                      </label>

                      <input
                        id="file1"
                        type="file"
                        onChange={(e) => changefile1handler(e)}
                        hidden
                        accept=".jpg , .png"
                      />
                    </div>
                    <div className="text-center">
                      <span className="text_bold d-block">تصویر متحرک </span>
                      <label
                        className="d-flex justify-content-center align-items-center modal_add_img pointer"
                        for="file2"
                        onMouseEnter={() =>
                          animationPreview != null
                            ? setHoverUpAnim(animationPreview)
                            : animation &&
                              animation != " " &&
                              animation != undefined
                              ? setHoverUpAnim(animation)
                              : setHoverUpAnim(" ")
                        }
                      >
                        {animationPreview != null ? (
                          animation?.name?.split(".").pop() === "gif" ? (
                            <img
                              className="pv_img_modal"
                              src={animationPreview}
                              alt="skill_item"
                            />
                          ) : (
                            <img
                              className="pv_img_modal"
                              src={No_Image}
                              alt="skill_item"
                            />
                          )
                        ) : animation && animation != " " ? (
                          <img
                            className="pv_img_modal"
                            src={`http://46.227.68.244:3050/${animation}`}
                            alt="skill_item"
                          />
                        ) : (
                          <img
                            className="pv_img_modal"
                            src={No_Image}
                            alt="skill_item"
                          />
                        )}

                        {hoverUpAnim === animationPreview ||
                          hoverUpAnim === animation ||
                          hoverUpAnim === " " ? (
                          <div
                            className="hover_add_img"
                            onMouseLeave={() => setHoverUpAnim("")}
                          >
                            <CloseIcon />
                          </div>
                        ) : null}
                      </label>

                      <input
                        id="file2"
                        type="file"
                        onChange={(e) => changefile2handler(e)}
                        hidden
                        accept=".gif"
                      />
                    </div>
                  </div>

                  {/* error input */}
                  {lableIsFull ? null : (
                    <div className="row justify-content-center">
                      <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                        <span>عنوان مهـارت را وارد کنید</span>
                      </div>
                    </div>
                  )}

                  {/* erorr files */}
                  {logo && logo != " " ? (
                    logo.name ? (
                      logo.name.split(".").pop() === "jpg" ||
                        logo.name.split(".").pop() === "png" ? null : (
                        <div className="row justify-content-center">
                          <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                            <span>پسوند تصویر ثابت نادرست است</span>
                          </div>
                        </div>
                      )
                    ) : logo.split(".").pop() === "jpg" ||
                      logo.split(".").pop() === "png" ? null : (
                      <div className="row justify-content-center">
                        <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                          <span>پسوند تصویر ثابت نادرست است</span>
                        </div>
                      </div>
                    )
                  ) : null}
                  {animation && animation != " " ? (
                    animation.name ? (
                      animation.name.split(".").pop() === "gif" ? null : (
                        <div className="row justify-content-center">
                          <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                            <span>پسوند تصویر متحرک نادرست است</span>
                          </div>
                        </div>
                      )
                    ) : animation.split(".").pop() === "gif" ? null : (
                      <div className="row justify-content-center">
                        <div class="alert alert-danger py-1 mb-0 mt-1 alert_skills_modal">
                          <span>پسوند تصویر متحرک نادرست است</span>
                        </div>
                      </div>
                    )
                  ) : null}
                </div>
              </Modal>
            )}

            {/* //////////////////////////////////////////////////////remove modal////////////////////////////////////////////////////// */}

            <Modal
              isOpen={removeModalIsOpen}
              onRequestClose={closeRemoveModal}
              style={RemoveStyles}
              closeTimeoutMS={200}
            >
              <div className="row d-flex align-items-center center px-4">
                <div>
                  <CloseIcon
                    fontSize="large"
                    className="pointer"
                    onClick={closeRemoveModal}
                  />
                  <span className="mr-2 text_bold">حذف مهـارت</span>
                </div>
              </div>
              <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
                آیا شما قصد حذف کردن این مهارت را دارید؟
          </div>
              <div className="row d-flex justify-content-center align-items-center mt-5">
                <button
                  onClick={() => {
                    return DeleteskillApi(), closeRemoveModal();
                  }}
                  className="btn btn-outline-success px-5 mx-3"
                >
                  بلــه
            </button>
                <button
                  onClick={closeRemoveModal}
                  className="btn btn-danger px-5 mx-3"
                >
                  خیـر
            </button>
              </div>
            </Modal>

            {/* //////////////////////////////////////////////////////loading modal////////////////////////////////////////////////////// */}

            <Modal isOpen={loadingIsOn} style={loadingStyle}>
              <div class="d-flex justify-content-center">
                <img className="w-25 h-25" src={loading} alt="" />
              </div>
            </Modal>

            {/* //////////////////////////////////////////////////////erro modal////////////////////////////////////////////////////// */}

            <Modal
              isOpen={errorModal}
              onRequestClose={closeErrorModal}
              style={ErrorStyle}
              closeTimeoutMS={200}
            >
              <div className="row d-flex align-items-center center px-4">
                <div>
                  <CloseIcon
                    fontSize="large"
                    className="pointer"
                    onClick={closeErrorModal}
                  />
                  <span className="mr-2 text_bold">{error}</span>
                </div>
              </div>
            </Modal>

            {/* //////////////////////////////////////////////////////erro modal////////////////////////////////////////////////////// */}

            <Modal
              isOpen={removeCheckpoint}
              onRequestClose={closeRemoveCheckpoint}
              style={RcheckpontStyle}
              closeTimeoutMS={200}
            >
              <div className="row d-flex align-items-center center px-4">
                <div>
                  <CloseIcon
                    fontSize="large"
                    className="pointer"
                    onClick={closeRemoveCheckpoint}
                  />
                  <span className="mr-2 text_bold">حذف چک پوینت</span>
                </div>
              </div>
              <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
                آیا شما قصد حذف کردن این چک پوینت را دارید؟
          </div>
              <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
                این چک پوینت حاوی {skillsOfChekpoint} اسکیل می باشد
          </div>
              <div className="row d-flex justify-content-center align-items-center mt-5">
                <button
                  onClick={() => {
                    return (
                      deleteCheckpointfunc(checkPoint_id), closeRemoveCheckpoint()
                    );
                  }}
                  className="btn btn-outline-success px-5 mx-3"
                >
                  بلــه
            </button>
                <button
                  onClick={closeRemoveCheckpoint}
                  className="btn btn-danger px-5 mx-3"
                >
                  خیـر
            </button>
              </div>
            </Modal>
          </div>
          :
          <Modal isOpen={loadingIsOn} style={loadingStyle}>
            <div class="d-flex justify-content-center">
              <img className="w-25 h-25" src={loading} alt="" />
            </div>
          </Modal>
      }

    </>
  );
}
