import React, { useState, useEffect, useRef } from "react";

import Sidebar from "./Sidebar";
import Header from "../Header/Header";
import Drag from "./b";
import ContentStation from "./ContentStation";
import { isMobile } from "react-device-detect";
import LoadingMain from "../Error Modal/Loading";
const MainStationManage = (props) => {
  let [cp_id, setCp_id] = useState();
  let [s_id, setS_id] = useState();
  let [skill_data, setskill_data] = useState();
  const [callbackRef, setCallbackRf] = useState();
  const [loadingPageBeforeCallApi, setLoadingPageBeforeCallApi] = useState();
  const [show, setShow] = useState(false);

  const moveScrollOfset = useRef();
  const executeScroll = () => scrollToRef(moveScrollOfset);
  const scrollToRef = (ref) =>
    window.scrollTo({
      top: ref?.current?.offsetTop,
      behavior: "smooth",
    });
  useEffect(() => {
    executeScroll();
  }, []);
  useEffect(() => {
    if (skill_data) {
      setLoadingPageBeforeCallApi(false);
    }
  }, [skill_data]);

  return (
    <>
      {loadingPageBeforeCallApi ? <LoadingMain /> : null}
      <Header />
      <div className=" container main-parent-statios p-0 p-md-3">
        <div className="row">
          <Sidebar
            setShow={setShow}
            show={show}
            setCp_id={(data) => {
              setCp_id(data);
            }}
            setS_id={(data) => {
              setS_id(data);
            }}
            setskill_data={(data) => {
              setskill_data(data);
            }}
            executeScroll={executeScroll}
            setLoadingPageBeforeCallApi={setLoadingPageBeforeCallApi}
          />
          <div
            className="col-12 col-lg-8   col-md-12"
            ref={isMobile ? moveScrollOfset : null}
          >
            <ContentStation
              show={show}
              ref={moveScrollOfset}
              checkpoint={cp_id}
              skill={s_id}
              skill_data={skill_data}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default MainStationManage;
