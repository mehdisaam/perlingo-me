import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

import PhonelinkSetupIcon from "@material-ui/icons/PhonelinkSetup";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import Answers from "./LessoneMain";
import classnames from "classnames";
import ModalRemove from "./ModalRemove";
import Modal from "react-modal";
import LoadingMain from "../Loading/Loading";
import noImg from "../../assets/img/No_Image.png";
import { bseurl } from "../../api-config/base-url-main";
import { api_token } from "../../api-config/tiken-main";
import { useHistory } from "react-router";

const ContentStation = (props) => {
  const history = useHistory;
  const [skilLevelId] = useState();
  const [checkpointId, setCheckpointId] = useState("");
  const [lengthLevels] = useState();
  const [, updateCharacters] = useState();
  const [levelChange, setLevelChange] = useState();
  const [lessoneItem, setLessoneItem] = useState();
  const [lessonePriority, setLessonePriority] = useState();
  const [lessonePer, setLessonePr] = useState();
  const [, setStateResultLevelId] = useState();
  const [loading, setLoiading] = useState(false);
  var subtitle;
  const [modalIsOpen, setIsOpen] = useState(false);

  function openModal(per, id) {
    setLessonePr(id);
    setLessonePriority(per);
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  const getlevelskill = () => {
    axios
      .post(
        `${bseurl}/admin/Skill?api_token=${localStorage.getItem("token")}`,
        {
          checkpoint_id: props.checkpoint,
          skill_id: props.skill,
        }
      )
      .then((res) => {
        setCheckpointId(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const addLessones = (length, levelId) => {
    axios
      .post(
        `${bseurl}/admin/checkpoint/lesson?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          checkpoint_id: props.checkpoint,
          skill_id: props.skill,
          level_id: levelId,
          priority: length + 1,
        }
      )
      .then((res) => {
        setCheckpointId(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const addLevel = () => {
    axios
      .post(
        `${bseurl}/admin/checkpoint/level?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          skill_id: props.skill,
          priority: lengthLevels + 1 || 1,
          checkpoint_id: props.checkpoint,
        }
      )
      .then((res) => {
        setCheckpointId(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };
  const putChangeApi = (arrayTest) => {
    axios
      .put(
        `${bseurl}/admin/change/level?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          skill_id: props.skill,
          checkpoint_id: props.checkpoint,
          levels: JSON.stringify(arrayTest),
        }
      )
      .then((res) => {
        setCheckpointId(res.data.data);
        setLoiading(false);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const changePutLessoneApi = (arrayLessoneList, levelId) => {
    axios
      .put(
        `${bseurl}/admin/change/lesson?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          skill_id: props.skill,
          checkpoint_id: props.checkpoint,
          level_id: levelId,
          lessons: JSON.stringify(arrayLessoneList),
        }
      )
      .then((res) => {
        setCheckpointId(res.data.data);
        setLoiading(false);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };
  const deleteLevel = (item) => {
    axios
      .post(
        `${bseurl}/admin/levelDelete?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          checkpoint_id: props.checkpoint,
          skill_id: props.skill,
          level_id: item,
        }
      )
      .then((res) => {
        setCheckpointId(res.data.data);
        setLoiading(false);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const addLevelFunction = () => {
    addLevel();
  };
  const handleCallbnackProps = (length, levelId) => { };
  const callLevelIdFromChild = (levelId) => { };
  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  function handleOnDragEnd(result) {
    if (result.type !== "QUESTIONS") {
      setStateResultLevelId(result.destination.droppableId.substring(9, 50));
    }

    const start = result?.source?.index;
    const end = result?.destination?.index;

    var j = end + 1;
    var d = end + 1;
    let arrayTest = [];
    var arrayLessoneList = [];
    var i;

    const apitestmap = checkpointId.skills.map((item) => {
      if (result.type !== "QUESTIONS") {
        const testSplite = result.destination.droppableId.substring(9, 50);
        item.leveles.map((level) => {
          if (String(level._id) === String(testSplite)) {
            if (start > end) {
              for (i = end; i <= start; i++) {
                if (i === start) {
                  arrayLessoneList.push({
                    id: level.lessones[i]._id,
                    priority: j,
                    question: level.lessones[i].questions,
                  });
                } else {
                  arrayLessoneList.push({
                    id: level.lessones[i]._id,
                    priority: ++d,
                    question: level.lessones[i]?.questions,
                  });
                }
              }
            } else {
              for (i = start; i <= end; i++) {
                if (i === start) {
                  arrayLessoneList.push({
                    id: level?.lessones[i]?._id,
                    priority: j--,
                    question: level.lessones[i]?.questions,
                  });
                } else {
                  arrayLessoneList.push({
                    id: level?.lessones[j]?._id,
                    priority: j--,
                    question: level.lessones[i]?.questions,
                  });
                }
              }
            }
          }
        });
      } else {
        if (start > end) {
          for (i = end; i <= start; i++) {
            if (i === start) {
              arrayTest.push({
                id: item.leveles[i]._id,
                priority: j,
                lessones: item.leveles[i].lessones,
              });
            } else {
              arrayTest.push({
                id: item.leveles[i]._id,
                priority: ++d,
                lessones: item.leveles[i]?.lessones,
              });
            }
          }
        } else {
          for (i = start; i <= end; i++) {
            if (i === start) {
              arrayTest.push({
                id: item?.leveles[i]?._id,
                priority: j--,
                lessones: item.leveles[i].lessones,
              });
            } else {
              arrayTest.push({
                id: item?.leveles[j]?._id,
                priority: j--,
                lessones: item.leveles[i].lessones,
              });
            }
          }
        }
      }
    });
    if (result.destination.index !== result.source.index) {
      if (result.type === "QUESTIONS") {
        putChangeApi(arrayTest);
        setLoiading(true);
      } else if (result.type !== "QUESTIONS") {
        setLessoneItem(arrayLessoneList);
        changePutLessoneApi(
          arrayLessoneList,
          result.destination.droppableId.substring(9, 50)
        );

        setLoiading(true);
      }
    }
  }
  useEffect(() => {
    setLevelChange();
  }, []);

  const handleClickDeleteLevel = (id) => {
    closeModal();
    deleteLevel(id);
    setLoiading(true);
  };
  var resultTest;
  useEffect(() => {
    setCheckpointId(props.skill_data);
  }, [props.skill_data]);
  const testArray = [
    { id: 1, content: "اول" },
    { id: 2, content: "دوم" },
    { id: 3, content: "سوم" },
    { id: 4, content: "چهارم" },
    { id: 5, content: "پنجم" },
    { id: 6, content: "ششم" },
    { id: 7, content: "هفتم" },
    { id: 8, content: "هشتم" },
    { id: 9, content: "نهم" },
    { id: 10, content: "دهم" },
  ];
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  return (
    <>

      {
        props.show &&
        (
          <DragDropContext onDragEnd={handleOnDragEnd}>
            {loading ? <LoadingMain /> : null}
            <Droppable droppableId="droppable" type="QUESTIONS">
              {(provided, snapshot) => {
                return (
                  <div
                    ref={provided.innerRef}
                    className={"main-content-setion w-100 p-3   sticymenu"}
                  >
                    <div className="title_box bg_skill_item">
                      <span className="skill_item">
                        <img
                          src={checkpointId ? (checkpointId?.skills[0]?.logo !== undefined ? "http://46.227.68.244:3050" + checkpointId.skills[0].logo : noImg) : noImg}
                          alt={checkpointId?.skills?.ffff}
                        />
                      </span>
                    </div>
                    {checkpointId !== undefined && checkpointId ? (
                      checkpointId?.skills[0]?.leveles?.map((question, index) => {
                        return (
                          <Draggable
                            key={question._id}
                            draggableId={question._id}
                            index={index}
                          >
                            {(provided, snapshot) => {
                              return (
                                <div
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  className={classnames(
                                    snapshot.isDragging
                                      ? "bg-draggingMain main-skil-level "
                                      : "main-skil-level"
                                  )}
                                >
                                  {question.content}
                                  <div
                                    className={classnames(
                                      snapshot.isDragging
                                        ? "bg-dragging priority-level"
                                        : "priority-level"
                                    )}
                                    {...provided.dragHandleProps}
                                  >
                                    <span> {"مرحله"}</span>
                                    {
                                      ((resultTest = testArray.findIndex(
                                        ({ id }) => id === index + 1
                                      )),
                                        resultTest >= 0
                                          ? testArray[resultTest].content
                                          : index)
                                    }
                                    { }
                                    <div className="level-list">
                                      <CloseIcon
                                        onClick={() => {
                                          openModal(index, question._id);
                                        }}
                                        id="close-icon"
                                      />
                                    </div>
                                  </div>
                                  <Answers
                                    questionNum={index}
                                    question={question}
                                    parentCallback={handleCallbnackProps}
                                    levelIdCallBackFunction={callLevelIdFromChild}
                                    levelIndex={index}
                                    levelId={question._id}
                                    skilLevelId={skilLevelId}
                                    checkpoint={props.checkpoint}
                                    skill={props.skill}
                                    arrSkills={checkpointId?.skills[0]}
                                    lessoneUpdate={lessoneItem}
                                    checkpointId={checkpointId}
                                    draging={snapshot.isDragging}
                                  />
                                  <div
                                    onClick={() => {
                                      handleCallbnackProps(
                                        question.lessones.length,
                                        question._id
                                      );
                                      addLessones(
                                        question.lessones.length,
                                        question._id
                                      );
                                    }}
                                    className="add-lesson"
                                  >
                                    + افزودن درس
                              </div>
                                </div>
                              );
                            }}
                          </Draggable>
                        );
                      })
                    ) : (
                      <div
                        className="add-class-style"
                        style={{ cursor: "default" }}
                      >
                        اسکیلی انتخاب نشده
                      </div>
                    )}
                    {provided.placeholder}
                    {checkpointId !== undefined ? (
                      <div className="add-class-style " onClick={addLevelFunction}>
                       + افزودن مرحله
                      </div>
                    ) : null}
                  </div>
                );
              }}
            </Droppable>
          </DragDropContext>


        )
      }
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
        style={customStyles}
        className="modal-react"
      >
        {" "}
        <CloseIcon
          onClick={() => {
            closeModal();
          }}
        >
          close
        </CloseIcon>
        <ModalRemove
          header={` حذف مرحله ${((resultTest = testArray.findIndex(
            ({ id }) => id === lessonePriority + 1
          )),
            resultTest >= 0 ? testArray[resultTest].content : lessonePriority)
            }`}
          submitClick={() => handleClickDeleteLevel(lessonePer)}
          content={"آیا مایل به حذف مرحله مورد نظر هستید؟"}
          closeModal={() => closeModal()}
        />
      </Modal>
    </>
  );
};

export default ContentStation;
