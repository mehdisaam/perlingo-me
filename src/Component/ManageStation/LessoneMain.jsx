import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { Droppable, Draggable } from "react-beautiful-dnd";
import ModalRemove from "./ModalRemove";
import Modal from "react-modal";
import PhonelinkSetupIcon from "@material-ui/icons/PhonelinkSetup";
import CloseIcon from "@material-ui/icons/Close";
import classnames from "classnames";
import LoadingMain from "../Loading/Loading";
import { useHistory } from "react-router-dom";
import { setLevel_lesson_id } from "../../redux/action/user";
import { useDispatch } from "react-redux";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
const LessoneMain = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoiading] = useState(false);
  const { question, questionNum } = props;

  const [dataProps, setDataProps] = useState();
  const [idLevelTest, setLevelIdtest] = useState(question._id);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [lessonePer, setLessonePr] = useState();

  const [lessonePriority, setLessonePriority] = useState();
  var subtitle;
  const handleClickEditQuestion = (answer, periority, priorityLevel) => {
    sessionStorage.setItem("periority", periority);

    history.push({
      pathname: "/question",
      state: {
        checkpoint: props.checkpoint,
        level_id: idLevelTest,
        skilId: props.skill,
        lesson_id: answer,
        priority: periority,
        priorityLevel: question.priority,
      },
    });
  };

  const deleteLessones = (item, answer, question) => {
    axios
      .post(
        `${bseurl}/admin/lessonDelete?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          checkpoint_id: props.checkpoint,
          skill_id: props.skill,
          level_id: item,
          lesson_id: answer,
          levelPeriority: question.priority,
        }
      )
      .then((res) => {
        return (
          setDataProps(res.data.data), setIsOpen(false), setLoiading(false)
        );
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const handleClickDeleteQuestion = (item, answer, question) => {
    closeModal();
    setLoiading(true);

    deleteLessones(item, answer, question);
  };

  const onTrigger = (event) => {
    props.parentCallback(dataProps.length);
  };

  const onDeleteControll = (lessone) => {};
  const testChange = (event) => {
    props.levelIdCallBackFunction(event);
  };

  function openModal(id, pr) {
    setLessonePriority(pr);
    setLessonePr(id);
    setIsOpen(true);
  }
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };

  localStorage.setItem("question", question.priority);
  // const loadingStyle = {
  //   overlay: {
  //     position: "fixed",
  //     top: 0,
  //     left: 0,
  //     right: 0,
  //     bottom: 0,
  //     backgroundColor: "rgba(0 0 0 / 40%)",
  //   },
  //   content: {
  //     margin: "auto",
  //     width: "400px",
  //     height: "155px",
  //     backgroundColor: "transparent",
  //     border: "none",
  //     zIndex: "1",
  //     borderRadius: "30px",
  //     // animationName: 'Modal_animation',
  //     // animationDuration: '.6s'
  //   },
  // };

  function closeModal() {
    setIsOpen(false);
  }

  useEffect(() => {
    setDataProps(question.lessones);
  }, [question.lessones]);

  return (
    <>
      {loading ? "" : null}
      <Droppable
        droppableId={`droppable${question._id}`}
        type={`${questionNum}`}
      >
        {(provided, snapshot) => {
          return (
            <div
              ref={provided.innerRef}
              className={classnames(
                props.draging
                  ? "bg-draggingMain  main-skil-lessen"
                  : "main-skil-lessen"
              )}
            >
              {dataProps?.map((answer, index) => {
                return (
                  <Draggable
                    key={`${questionNum}${answer._id}`}
                    draggableId={`${questionNum}${answer._id}`}
                    index={index}
                    isDragging={
                      (onTrigger(dataProps.length), testChange(props.levelId))
                    }
                  >
                    {(provided, snapshot) => {
                      return (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          className={classnames(
                            snapshot.isDragging
                              ? "bg-draggingMain  priority-lesson "
                              : " priority-lesson"
                          )}
                          {...provided.dragHandleProps}
                        >
                          <span>
                            درس {+" "}
                            {answer.priority}
                          </span>
                          <div className="hover-list">
                            <PhonelinkSetupIcon
                              onClick={() => {
                                handleClickEditQuestion(
                                  answer._id,
                                  answer.priority,
                                  dataProps
                                );
                                sessionStorage.setItem("level", idLevelTest);
                                sessionStorage.setItem("lesson", answer._id);
                                sessionStorage.setItem(
                                  "priority_level",
                                  question.priority
                                );
                              }}
                              id="edite-icon"
                            />
                            <CloseIcon
                              onClick={() =>
                                openModal(answer._id, answer.priority)
                              }
                              id="close-icon"
                            />
                          </div>
                        </div>
                      );
                    }}
                  </Draggable>
                );
              })}
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
        style={customStyles}
        className="modal-react"
      >
        <CloseIcon
          onClick={() => {
            closeModal();
          }}
        >
          close
        </CloseIcon>
        <ModalRemove
          header={` حذف درس ${lessonePriority} `}
          submitClick={() =>
            handleClickDeleteQuestion(idLevelTest, lessonePer, question)
          }
          content={"آیا مایل به حذف درس مورد نظر هستید؟"}
          closeModal={() => closeModal()}
        />
      </Modal>
    </>
  );
};

export default LessoneMain;

// <Droppable droppableId={`droppable${question._id}`} type={`${questionNum}`}>
//   {(provided, snapshot) => (
//     <div ref={provided.innerRef} className="main-skil-lessen">
//       {dataProps?.map((answer, index) => {
//         return (
//           <Draggable
//             key={`${questionNum}${answer._id}`}
//             draggableId={`${questionNum}${answer._id}`}
//             index={index}
//             isDragging={
//               ((
//                 props.arrSkills.leveles[index]._id,
//                 index,
//                 "eventeventevenasdasdasdasdasdtevent"
//               ),
//               onTrigger(dataProps.length),
//               testChange(props.arrSkills.leveles[index]._id))
//             }
//           >
//             {(provided, snapshot) => (
//               <div
//                 ref={provided.innerRef}
//                 {...provided.draggableProps}
//                 className="priority-lesson "
//                 {...provided.dragHandleProps}
//               >
//                 <span>
//                   font assssem
//                   {answer._id}
//                 </span>
//                 <div className="hover-list">
//                   <PhonelinkSetupIcon
//                     onClick={handleClickEditQuestion}
//                     id="edite-icon"
//                   />
//                   <CloseIcon
//                     onClick={() => {
//                       handleClickDeleteQuestion(idLevelTest, answer._id);
//                     }}
//                     id="close-icon"
//                   />
//                 </div>
//               </div>
//             )}
//           </Draggable>
//         );
//       })}
//       {provided.placeholder}
//     </div>
//   )}
// </Droppable>
