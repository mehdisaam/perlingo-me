import React, { useState } from "react";
import Modal from "react-modal";
import CloseIcon from "@material-ui/icons/Close";

export default function ErrorModal(props) {
  const ErrorStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: "rgba(0 0 0 / 40%)",
      zIndex: "5000",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: "auto",
      width: "max-content",
      height: "max-content",
      zIndex: "5000",
      borderRadius: "30px",
      color: "#721c24",
      backgroundColor: "#f8d7da",
      borderColor: "#f5c6cb",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  return (
    <>
      <Modal
        isOpen={props.isOpen}
        onRequestClose={() => props.close(false)}
        style={ErrorStyle}
        closeTimeoutMS={200}
      >
        <div className="row d-flex align-items-center center px-4">
          <div>
            <CloseIcon
              fontSize="large"
              className="pointer"
              onClick={() => props.close(false)}
            />
            <span className="mr-2 text_bold">{props.error}</span>
          </div>
        </div>
      </Modal>
    </>
  );
}
