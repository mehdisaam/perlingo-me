import React from 'react'
import Modal from 'react-modal'
import loading from '../../assets/img/loading.gif'

export default function Loading(props) {


    //close loading function

    const loadingStyle = {
        overlay: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: '5000',
            backgroundColor: 'rgba(0 0 0 / 40%)'
        },
        content: {
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            margin: 'auto',
            width: '400px',
            height: '155px',
            backgroundColor: 'transparent',
            border: 'none',
            zIndex: '5000',
            borderRadius: '30px',
            // animationName: 'Modal_animation',
            // animationDuration: '.6s'
        }
    };

    return (
        <div>
            <Modal
                isOpen={props.isOpen}
                style={loadingStyle}
            >
                <div class="d-flex justify-content-center">
                    <img className="w-25 h-25" src={loading} alt="" />
                </div>
            </Modal>

        </div>
    )
}
