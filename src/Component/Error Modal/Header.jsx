import React from 'react'
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";

export default function Header(props) {
    return (
        <>
            <div className="row d-flex justify-content-between align-items-center center px-4">
                <div>
                    <CloseIcon
                        fontSize="large"
                        className="pointer"
                        onClick={props.Close}
                    />
                    <span className="mr-2 text_bold fs_xs_14px">{props.label}</span>
                </div>
                <div className="d-flex align-items-center">
                    {props.newbutton && <div className="pointer ml-4 fs_xs_14px">
                        {props.newbutton}
                    </div>}

                    <div
                        onClick={props.Api}
                        className="pointer">
                        <SaveOutlinedIcon fontSize="large" />
                        <span className="text_bold d-none d-md-inline">ذخیره</span>

                    </div>
                </div>
            </div>
            <hr className=""/>
        </>
    )
}
