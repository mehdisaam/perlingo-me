import React, { useEffect, useState } from "react";
import axios from "axios";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router-dom";

const HeaderDashboard = () => {
  const [chartState, setChartState] = useState();
  const history = useHistory();
  const apiCharts = () => {
    axios
      .get(`${bseurl}/dashboard?api_token=${localStorage.getItem("token")}`)
      .then(
        (res) => (
          setChartState(res.data.data.checkpointInfo),
          (res.data.data, "sadasdasdadsfsdfsdfsdfsdf")
        )
      )
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };
  useEffect(() => {
    apiCharts();
  }, []);

  return (
    <div className="heade-dash-main ">
      <div className="col-12 text-right">
        <h3 className="col-12">ایستگاه ها</h3>
      </div>
      {/* {Object.keys(chartState.checkpointInfo).map((keyName, i) => {
        (keyName, "keyName");
      })} */}
      <div className="col-12 content-header">

        <div className="col-12 header-dash-content">
          <p>تعداد چکپوینت های ثبت شده</p>
          <span>{chartState?.checkpointCount}</span>
        </div>
        <div className="col-12 header-dash-content">
          <p>تعداد مهارت های ثبت شده</p>
          <span>{chartState?.skillCount}</span>
        </div>
        <div className="col-12 w-100 header-dash-content">
          <p>تعداد سطوح ثبت شده</p>
          <span>{chartState?.levelCount}</span>
        </div>
        <div className="col-12 header-dash-content">
          <p>تعداد دروس ثبت شده</p>
          <span>{chartState?.lessonCount}</span>
        </div>
        <div className="col-12 header-dash-content">
          <p>تعداد سوالات ثبت شده</p>
          <span>{chartState?.questionCount}</span>
        </div>
      </div>
    </div>
  );
};

export default HeaderDashboard;
