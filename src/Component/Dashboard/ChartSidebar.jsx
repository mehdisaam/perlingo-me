import React, { useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import imgMain from "../../assets/img/test_skill.jpg";

import axios from "axios";
import { bseurl } from "../../api-config/base-url-main";
import { api_token } from "../../api-config/tiken-main";
import { useHistory } from "react-router";
import noImg from "../../assets/img/No_Image.png";

const ChartSidebar = () => {
  const history = useHistory();
  const [chartState, setChartState] = useState();
  const [arrayChart, setArrayChart] = useState([]);
  const [topUser, setTopUser] = useState();

  const apiCharts = () => {
    axios
      .get(`${bseurl}/dashboard?api_token=${localStorage.getItem("token")}`)
      .then(
        (res) => (
          setChartState(res.data.data),
          setTopUser(res.data.data.topUser)
          // console.log( res.data.data, "sadasdasdadsfsdfsdfsdfsdf")
        )
      )
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };
  useEffect(() => {
    apiCharts();
  }, []);
  var labelArray = [];
  chartState?.topUser?.map(
    (chart) =>
      chart?.skillChart?.map((userChart) => labelArray?.push(userChart.month))
    // (chart.month, "sdadasdfsdfsdfsd")
  );
  var testArray = [];

  var lastArrayInApi;
  lastArrayInApi = chartState?.topUser.slice(0, 5);
  if (chartState?.topUser.length < 4) {
    lastArrayInApi.shift();
  }
  // var barGraph = new Chart()

  var state;
  //   lastArrayInApi?.skillChart?.map((chart) => (chart, "adasdasdasd"));
  //   lastArrayInApi?.map((item, index) => {
  //     var labelArrayY = [];
  //     item?.skillChart?.map((chart) => labelArrayY.push(chart.month));
  //     setArrayChart.push(labelArrayY);
  //   });
  //   labelArrayY.push(chart.month)
  //   const state = {
  //     labels: testArray,
  //     datasets: [
  //       {
  //         label: "",
  //         fill: false,
  //         lineTension: 0.5,
  //         backgroundColor: "transparent",
  //         borderColor: "#f4b8b8",
  //         borderWidth: 2,
  //         data: [65, 59, 80, 81, 56],
  //       },
  //     ],
  //   };

  const Options =
  {
    plugins: {
      title: {
        display: false,
        fontSize: 20,
      },
      legend: {
        display: false,
      }
    }
  }

  return (
    <>
      {topUser && topUser.slice(0, 5).map((item, index) => (
        <div className="chart-sidebar col-12 pl-0  ">
          <div className="col-md-5 col-12  pr-0 content-sidebar">
            <div className="d-flex ">
              <div className="img-user">
                <img
                  src={
                    item.picture
                      ? `http://46.227.68.244:3050/${item.picture}`
                      : noImg
                  }
                  alt="imgMain"
                />
              </div>
              <div className="mr-2">
                <p className="m-3">{item.name}</p>

              </div>
            </div>
          </div>

          <div className="col-md-7 col-12 my-4 my-md-0 pr-0">
            <Line
              data={{
                labels: item?.perChart?.map((chart, index) => {
                  var timestamp = chart.date;
                  var a = new Date(timestamp * 1000);
                  var days = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'S'];
                  var dayOfWeek = days[a.getDay()]
                  return (dayOfWeek)
                }),

                datasets: [
                  {
                    label: '',
                    fill: true,
                    lineTension: 1,
                    backgroundColor: "transparent",
                    borderColor: "#f4b8b8",
                    borderWidth: 2,
                    data: item?.perChart?.map((chart) => chart.xp),

                  },
                ],
              }}
              options={Options}
            />
          </div>
        </div>
      ))}
    </>
  );
};

export default ChartSidebar;
