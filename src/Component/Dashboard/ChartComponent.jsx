import React, { useState } from "react";
import { Line } from "react-chartjs-2";
const ChartComponent = () => {
  const data = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
    datasets: [
      {
        label: "First dataset",
        data: [33, 53, 85, 41, 44, 65],
        fill: true,
      borderRarius:'10px',
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
    ],

      },
  
    ],
  };

  return (
    <div style={{ width: "100%", height: "200px" }}>
      <Line data={data} />
    </div>
  );
};

export default ChartComponent;
