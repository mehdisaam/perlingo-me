import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import axios from "axios";
import { bseurl } from "../../api-config/base-url-main";
import { api_token } from "../../api-config/tiken-main";
import { useHistory } from "react-router";

const ChartContent = () => {
  const [chartState, setChartState] = useState();
  const history = useHistory();
  const apiCharts = () => {
    axios
      .get(`${bseurl}/dashboard?api_token=${localStorage.getItem("token")}`)
      .then((res) => setChartState(res.data.data))
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login") : null;
      });
  };
  var labelArray = [];
  chartState?.allUserInfo?.map((chart) => labelArray.push(chart.day));
  var labelArrayY = [];
  chartState?.allUserInfo?.map((chart) =>
    labelArrayY.push(Math.round(chart.count))
  );

  useEffect(() => {
    apiCharts();
  }, []);

  const state = {
    labels: labelArray,
    datasets: [
      {
        label: "kkkkkk",
        fill: false,

        backgroundColor: "transparent",
        borderColor: "#f4b8b8",
        borderWidth: 2,
        data: labelArrayY,
      },
    ],
  };

  const Options =
  {
    plugins: {
      title: {
        display: false,
        fontSize: 20,
      },
      legend: {
        display: false,
      }
    }
  }
  
  return (
    <div className="chart-content col-12">
      <div className="col-12 text-right">
        <p className="text_dashboard_style">ثبت نام کاربران 30 روز اخیر</p>
      </div>
      <Line
        data={state}
        options={Options}
      />
    </div>
  );
};

export default ChartContent;
