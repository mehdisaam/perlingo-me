import React from "react";
import HeaderDashboard from "./HeaderDashboard";
import SidebarDashboard from "./SidebarDashboard";
import ContentDashboard from "./ContentDashboard";
import Header from "../Header/Header";
const Dashboard = () => {
  return (
    <div className="dashboard-main">
      <Header />

      <div className="col-12 container">
        <div className="col-md-5 col-12 pr-0 pl-0 float-right side-bar-dashbord">
          <SidebarDashboard />
        </div>
        <div className="col-md-7 col-12 float-right sticymenu p-0 mt-5 ">
          <div className="col-12 p-0 mr-4">
            <HeaderDashboard />
          </div>
          <div className="col-12 p-0 mr-4">
            <ContentDashboard />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
