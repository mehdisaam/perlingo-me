import React from "react";
import ChartSidebar from "./ChartSidebar";
const SidebarDashboard = () => {
  return (
    <div>
      <div className="col-12 side-bar-parent">
        <h3>فعال ترین کاربران 5 روز اخیر</h3>
      </div>

      <ChartSidebar />
    </div>
  );
};

export default SidebarDashboard;
