import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { BaseUrl } from "../../Constant/Config";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";
import { MDBDataTable } from "mdbreact";
import No_Image from "../../assets/img/No_Image.png";
import VolumeUpOutlinedIcon from "@material-ui/icons/VolumeUpOutlined";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import CloseIcon from "@material-ui/icons/Close";
import Modal from "react-modal";
import Editword from "./Editword";

export default function Wordlist(props) {
  // State
  let ref = useRef();

  const [searchWord, setSearchWord] = useState();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [audio, setAudio] = useState();
  const [sound, setSound] = useState(false);
  const [editModal, setEditModal] = useState(false); // modal Edit kalame

  // modal state
  const [image, setImage] = useState();
  const [name, setName] = useState();
  const [lang, setLang] = useState();
  const [meaning, setMeaning] = useState();
  const [voiceMan, setVoiceMan] = useState();
  const [voiceWoman, setVoiceWoman] = useState();
  const [word_id, setWord_id] = useState();
  const [removeModalIsOpen, setRemoveModalIsOpen] = useState(false);

  // open Remove modal function
  function openRemoveModal() {
    setRemoveModalIsOpen(true);
  }

  //close add modal function
  function closeRemoveModal() {
    setRemoveModalIsOpen(false);
  }
  const RemoveStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "220px",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  function openEditModal() {
    setEditModal(true);
  }
  function closeEditModal() {
    setEditModal(false);
  }
  const EditModalStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: "auto",
      width: "70%",
      height: "max-content",
      zIndex: "10",
      borderRadius: "30px",
    },
  };

  const deleteWord = () => {
    setLoading(true);
    axios
      .delete(`${BaseUrl}/api/v1/admin/word/${word_id}?api_token=${localStorage.getItem(
        "token"
      )}`)
      .then((res) => {
        props.setWordList(res.data.data.reverse());

        setLoading(false);
      })
      .catch((err) => err);
  };

  const setEditData = (
    picture,
    word,
    language,
    meanWord,
    voiceman,
    voiceWoman,
    id
  ) => {
    setImage(picture);
    setName(word);
    setLang(language);
    setMeaning(meanWord);
    setVoiceMan(voiceman);
    setVoiceWoman(voiceWoman);
    setWord_id(id);
  };

  const setref = (e) => {
    ref = new Audio(`${BaseUrl}${e}`);
  };

  const playaudio = () => {
    ref.play();
  };

  // data table
  const userAttributes = [];
  props.wordList &&
    props.wordList.forEach((el) => {
      userAttributes.push({
        image: el.picture ? (
          <img
            className="img_style"
            src={`${BaseUrl}/${el.picture}`}
            alt="sdasdasdsa"
          />
        ) : (
          <img className="img_style" src={No_Image} alt="No_image" />
        ),
        word: el.word ? el.word : "empty",
        meaning: el.meanings
          ? el.meanings.map(
            (item, index) =>
              " " + item + (el.meanings.length - 1 === index ? "" : " ،")
          )
          : "empty",
        voices: (
          <div className="d-flex justify-content-center align-items-center">
            {el.male && (
              <div
                className="mx-3 pointer"
                onMouseEnter={() => setref(el.male)}
              >
                <div onClick={() => playaudio()}>
                  <VolumeUpOutlinedIcon />
                  <span className="d-block">اقا</span>
                </div>
              </div>
            )}
            {el.female && (
              <div
                className="mx-3 pointer"
                onMouseEnter={() => setref(el.female)}
              >
                <div onClick={() => playaudio()}>
                  <VolumeUpOutlinedIcon />
                  <span className="d-block">خانم</span>
                </div>
              </div>
            )}
          </div>
        ),
        operation: (
          <>
            <button
              className="btn text-success"
              onClick={() => {
                openEditModal();
                setEditData(
                  el.picture,
                  el.word,
                  el.language,
                  el.meanings,
                  el.male,
                  el.female,
                  el._id
                );
              }}
            >
              ویرایش
              <CreateOutlinedIcon />
            </button>
            <button
              className="btn text-danger"
              onClick={() => {
                setRemoveModalIsOpen(true);
                setWord_id(el._id);
              }}
            >
              حذف
              <CloseIcon />
            </button>
          </>
        ),
      });
    });

  const data = {
    columns: [
      {
        label: "تصویر",
        field: "image",
        sort: "asc",
        width: 50,
      },
      {
        label: "عنوان کلمه",
        field: "word",
        sort: "asc",
        width: 20,
      },
      {
        label: "معنی کلمه",
        field: "meaning",
        sort: "asc",
        width: 20,
      },
      {
        label: "صداهای کلمه",
        field: "voices",
        sort: "asc",
        width: 25,
      },
      {
        label: "عملیات",
        field: "operation",
        sort: "asc",
        width: 150,
      },
    ],
    rows: userAttributes,
  };

  const searchHandler = (e) => {
    setSearchWord(e.target.value);
  };

  return (
    <>
      <div className="word_list_Box px-5">
        <div className="col-12 mt-5 ">
          <MDBDataTable responsive striped bordered data={data} />
        </div>
      </div>
      {editModal && (
        <Modal
          isOpen={editModal}
          onRequestClose={closeEditModal}
          style={EditModalStyle}
          closeTimeoutMS={200}
        >
          <div className="pic_edit_word">
            <Editword
              editpicture={image}
              name={name}
              language={lang}
              meaning={meaning}
              man={voiceMan}
              woman={voiceWoman}
              setWordList={props.setWordList}
              closeEditModal={closeEditModal}
              word_id={word_id}
            />
          </div>
        </Modal>
      )}
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
      {removeModalIsOpen && (
        <Modal
          isOpen={removeModalIsOpen}
          onRequestClose={closeRemoveModal}
          style={RemoveStyles}
          closeTimeoutMS={200}
        >
          <div className="row d-flex align-items-center center px-4">
            <div>
              <CloseIcon
                fontSize="large"
                className="pointer"
                onClick={closeRemoveModal}
              />
              <span className="mr-2 text_bold">حذف مهـارت</span>
            </div>
          </div>
          <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
            آیا شما قصد حذف کردن این کلمه را دارید؟
          </div>
          <div className="row d-flex justify-content-center align-items-center mt-5">
            <button
              onClick={() => {
                return deleteWord(), closeRemoveModal();
              }}
              className="btn btn-outline-success px-5 mx-3"
            >
              بلــه
            </button>
            <button
              onClick={closeRemoveModal}
              className="btn btn-danger px-5 mx-3"
            >
              خیـر
            </button>
          </div>
        </Modal>
      )}
    </>
  );
}
