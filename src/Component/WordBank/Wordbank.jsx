import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import SelectLanguage from "../AddWord/SelectLanguage";
import { BaseUrl } from "../../Constant/Config";
import axios from "axios";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";
import Header from "../Header/Header";

import TextField from "@material-ui/core/TextField";
import Autocomplete from "../AddWord/Autocomplete_Addword";
import Voice from "../AddWord/Voices";

import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import PrePicture from "../AddWord/PrePicture";
import Wordlist from "./Wordlist";
import AddWord from "../AddWord/Addword";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router";
export default function Wordbank() {
  const history = useHistory();
  // State
  const [wordList, setWordList] = useState();
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode
  const [wordName, setWordName] = useState(""); // sabt nam kalame
  const [selectedWord, setSelectedWord] = useState([]);
  const [voiceMan, setVoiceMan] = useState(null); //voice agha
  const [voiceWoman, setVoiceWoman] = useState(null); // voice khanom
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [lang, setLang] = useState(""); // sabt language
  const [addWord, setAddWord] = useState(false); // modal afzodan kalame
  const [emptyvc, setEmptyvc] = useState(false);

  // Modal
  function openAddWord() {
    setAddWord(true);
  }
  function closeAddWord() {
    setAddWord(false);
  }
  const AddWordStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      margin: "auto",
      width: "70%",
      height: "400px",
      borderRadius: "30px",
    },
  };

  // Functions
  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  const handleLanguage = (event) => {
    setLang(event.target.value);
  };

  const ClearState = () => {
    setPicture(null);
    setWordName("");
    setSelectedWord([]);
    setVoiceMan(null);
    setVoiceWoman(null);
    setLang("");
    setEmptyvc(true);
  };

  // Api
  const addWordApi = () => {
    setLoading(true);
    let data = new FormData();
    data.append("picture", picture);
    data.append("word", wordName);
    data.append("meanings", JSON.stringify(selectedWord));
    data.append("female", voiceWoman);
    data.append("male", voiceMan);
    data.append("language", lang);
    let headers = { "Content-Type": "multipart/form-data" };
    axios
      .post(
        `${bseurl}/admin/word?api_token=${localStorage.getItem("token")}`,
        data,
        headers
      )
      .then((res) => {
        setWordList(res.data.data.reverse());
        ClearState();
        setLoading(false);
        closeAddWord();
      })
      .catch((err) => {
        return (
          setError(err.response.data.data),
          setLoading(false),
          setErrorModal(true),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };

  const getWordList = () => {
    setLoading(true);
    axios
      .get(`${BaseUrl}/api/v1/admin/word/list?api_token=${localStorage.getItem(
        "token"
      )}`)
      .then((res) => {
        setWordList(res.data.data.reverse());

        setLoading(false);
      })
      .catch((error) =>
        error.response.data.status === 401 ? history.push("/login") : null
      );
  };

  useEffect(() => {
    getWordList();
  }, []);

  return (
    <>
      <Header />
      <div className="container Wordbankpage">
        <div className="row mt-5">
          <div className="col-12">
            <button className="btn btn_AddWord">
              <AddWord setWord={setWordList} />
            </button>
            <Wordlist setWordList={setWordList} wordList={wordList} />
          </div>
        </div>
      </div>
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </>
  );
}
