import React, { useEffect, useState } from "react";
import SelectLanguage from "../AddWord/SelectLanguage";
import { BaseUrl } from "../../Constant/Config";
import axios from "axios";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";

import TextField from "@material-ui/core/TextField";
import Autocomplete from "../AddWord/Autocomplete_Addword";
import Voice from "../AddWord/Voices";

import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import PrePicture from "./PrePictureEdit";
import Wordlist from "./Wordlist";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router";
export default function Editword(props) {
  const history = useHistory();
  // State
  const [prePicture1, setPrePicture1] = useState(); //preview aks entekhab shode
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [addWord, setAddWord] = useState(false); // modal afzodan kalame

  // api property
  const [picture1, setPicture1] = useState(); //save picture for API
  const [lang, setLang] = useState(""); // sabt language
  const [wordName, setWordName] = useState(""); // sabt nam kalame
  const [selectedWord, setSelectedWord] = useState([]);
  const [voiceMan, setVoiceMan] = useState(null); //voice agha
  const [voiceWoman, setVoiceWoman] = useState(null); // voice khanom

  // condition for send files when dont be modify
  const [vcm, setVcm] = useState(false);
  const [vcvm, setVcvm] = useState(false);
  const [img, setImg] = useState(false);

  const ClearState = () => {
    setPicture1(null);
    setWordName("");
    setSelectedWord([]);
    setVoiceMan(null);
    setVoiceWoman(null);
    setLang("");
  };

  const EditWord = () => {
    setLoading(true);
    let headers = { "Content-Type": "multipart/form-data" };
    let data = new FormData();
    data.append("word", wordName);
    data.append("meanings", JSON.stringify(selectedWord));
    data.append("language", lang);
    data.append("id", props.word_id);
    img && data.append("picture", picture1);
    vcvm && data.append("female", voiceWoman);
    vcm && data.append("male", voiceMan);
    axios
      .put(
        `${bseurl}/admin/word?api_token=${localStorage.getItem("token")}`,
        data,
        headers
      )
      .then((res) => {
        props.setWordList(res.data.data.reverse());
        ClearState();
        setLoading(false);
        props.closeEditModal();
      })
      .catch((err) => {
        return (
          setError(err.response.data.data),
          setLoading(false),
          setErrorModal(true),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };

  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture1(e.target.files[0]);
      setPrePicture1(URL.createObjectURL(e.target.files[0]));
      setImg(true);
    }
  };

  const handleLanguage = (event) => {
    setLang(event.target.value);
  };

  useEffect(() => {
    setPicture1(props.editpicture);
    setWordName(props.name);
    setLang(props.language);
    setSelectedWord(props.meaning);
    setVoiceMan(props.man);
    setVoiceWoman(props.woman);
  }, []);

  return (
    <>
      <div className="row">
        <div className="col-12">
          <div className="edit_word_box">
            <div className="row d-flex justify-content-between align-items-center center px-4 text_gray">
              <div>
                <span className="mr-2 text_bold fs_xs_14px">ثبت کلمه جدید</span>
              </div>
              <div className="d-flex align-items-center">
                <div onClick={EditWord} className="pointer">
                  <SaveOutlinedIcon fontSize="large" />
                  <span className="text_bold d-none d-md-inline">ذخیره</span>
                </div>
              </div>
            </div>
            <hr className="hr" />
            <div className="row px_wordbank pt-3 pb-4 d-flex align-items-center">
              <div className="col-12 col-lg-2">
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center picture   ">
                  <PrePicture
                    id={"edit"}
                    Picturehandler={Picturehandler}
                    prePicture={prePicture1}
                    editpicture={props.editpicture}
                  />
                </div>
              </div>
              <div className="col-12 col-lg-5">
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center focus_lable">
                  <div>
                    <TextField
                      defaultValue={props.name}
                      onChange={(e) => setWordName(e.target.value)}
                      label="نام کلمه"
                      color="success"
                      autoComplete="off"
                    />
                  </div>
                  <div className="mr-5 focus_lable_lan">
                    <SelectLanguage
                      language={props.language}
                      handleLanguage={handleLanguage}
                      lang={lang}
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center">
                  <Autocomplete
                    setSelectedWord={setSelectedWord}
                    meaning={props.meaning}
                  />
                </div>
              </div>
              <div className="col-12 col-lg-5">
                <div className="d-flex justify-content-center justify-content-lg-start align-items- mt-3">
                  <Voice
                    defaultValue={props.man}
                    id={"manedit"}
                    onChangeFunc={setVoiceMan}
                    label={"صدای کلمه را وارد کنید (آقا)"}
                    setVc={setVcm}
                  />
                </div>
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center mt-2 pt-1">
                  <Voice
                    defaultValue={props.woman}
                    id={"womanedit"}
                    onChangeFunc={setVoiceWoman}
                    label={"صدای کلمه را وارد کنید (خانم)"}
                    setVc={setVcvm}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </>
  );
}
