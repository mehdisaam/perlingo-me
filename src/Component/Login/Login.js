import React, { useState } from "react";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Modal from "react-modal";
import CloseIcon from "@material-ui/icons/Close";
import { useHistory } from "react-router";
import ErrorModal from "../Error Modal/ErrorModal";
import Loading from "../Loading/Loading";
import Ellipsis from "@bit/joshk.react-spinners-css.ellipsis";
import {
  fade,
  ThemeProvider,
  withStyles,
  makeStyles,
  createMuiTheme,
} from "@material-ui/core/styles";
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "green",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00ccff",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderWidth: "2px",
        borderRadius: "10px",
      },
      "&:hover fieldset": {
        borderColor: "#777777",
        borderRadius: "10px",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00ccff",
        borderRadius: "10px",
      },
    },
    "& MuiAutocomplete-option": {
      direction: "rtl",
    },
  },
})(TextField);
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

const Login = () => {
  const [loading, setLoading] = useState(false);
  const [errorHandler, setErrorHandler] = useState(false);
  const [textError, setTextModal] = useState("");
  const history = useHistory();
  const classes = useStyles();
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [addModalIsOpen, setaddModalIsOpen] = useState();
  const [closeAddModal, setcloseAddModal] = useState();
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const dataLoginFromApi = () => {
    axios
      .post(`http://46.227.68.244:3050/api/v1/admin/login`, {
        username: username,
        password: password,
      })
      .then((res) =>
        res.data.status === 200
          ? ((res.data.data.token, "res.data.data"),
            setLoading(false),
            localStorage.setItem("token", res.data.data.token),
            setError(res.data.data),
            history.push("/dashboard"))
          : (addModalIsOpen(true), (res.data.data, "res.data.data"))
      )
      .catch((error) => {
        return (
          error.response?.data?.status === 404
            ? (setErrorHandler(true),
              setTextModal(error.response.data.data),
              setLoading(false))
            : null,
          error.response?.data?.status === 403
            ? (setErrorHandler(true),
              setTextModal(error.response.data.data),
              setLoading(false))
            : null,
          error.response.data.status === 401 ? history.push("/login") : null,
          setError(error?.response?.data?.data, "aaaaaaaa")
        );
      });
  };
  const handleChangeUsername = (event) => {
    setUserName(event.target.value);
  };
  const handleChangePaswword = (event) => {
    setPassword(event.target.value);
  };
  const onSubmitForm = (event) => {
    setLoading(true);
    event.preventDefault();
    dataLoginFromApi();
  };

  const ErrorStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: "rgba(0 0 0 / 40%)",
      zIndex: "5000",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: "auto",
      width: "max-content",
      height: "max-content",
      zIndex: "5000",
      borderRadius: "30px",
      color: "#721c24",
      backgroundColor: "#f8d7da",
      borderColor: "#f5c6cb",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  function closeErrorModal() {
    setErrorModal(false);
  }

  return (
    <>
      <div className="login-main">
        <div className="header-login">
          <div className="img-login"></div>
        </div>
        <div className="content-api container">
          <form onSubmit={onSubmitForm}>
            <label>
              <CssTextField
                id={"outlined-basic"}
                label={<p className="text_gray_login">نام کاربری</p>}
                variant="outlined"
                onChange={handleChangeUsername}
                className={`${classes.margin},test-class`}
                style={{ marginBottom: "20px" }}
                type="text"
                autoComplete="of"
              />
            </label>
            <label>
              <CssTextField
                id={"outlined-basic"}
                label={<p className="text_gray_login">پسورد</p>}
                variant="outlined"
                onChange={handleChangePaswword}
                className={`${classes.margin}"test-class"`}
                style={{ marginBottom: "20px" }}
                type="password"
              />
            </label>
            <div>
              <button type="submit" className="btn btn-outline-info col-12">
                {loading ? <Ellipsis color="#00ccff " /> : <p>ورود به پنل</p>}
              </button>
            </div>
          </form>
        </div>
      </div>
      {errorHandler ? (
        <ErrorModal
          isOpen={errorHandler}
          error={textError}
          close={setErrorHandler}
        />
      ) : null}
    </>
  );
};

export default Login;
