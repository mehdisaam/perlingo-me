import React, { useState, useEffect } from "react";
import { BaseUrl } from "../../Constant/Config";
import axios from "axios";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";
import { MDBDataTable } from "mdbreact";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import CloseIcon from "@material-ui/icons/Close";
import No_Image from "../../assets/img/No_Image.png";
import Modal from "react-modal";
import Header from "../Header/Header";
import { useHistory } from "react-router";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import ErrorModal from "../Error Modal/ErrorModal";

export default function User() {
  const history = useHistory();
  // state
  const [word, setWord] = useState();
  const [userList, setUserList] = useState();
  const [loading, setLoading] = useState(false);
  const [infoModal, setInfoModal] = useState(false); // user info  modal
  const [userData, setUserData] = useState();
  const [showWord, setShowWord] = useState(false);
  const [type, setType] = useState("");
  const [badge, setBadge] = useState();
  const [badgeData, setBadgeData] = useState();
  const [textError, setTextModal] = useState("");
  const [errorHandler, setErrorHandler] = useState(false);

  function openBadge() {
    setBadge(true);
  }

  //close add modal function
  function closeBadge() {
    setBadge(false);
  }

  const BadgeStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "50%",
      maxHeight: "75%",
      overflow: "hidden",
      zIndex: "10",
      borderRadius: "30px",

      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  function openShowWord() {
    setShowWord(true);
  }

  //close add modal function
  function closeShowWord() {
    setShowWord(false);
  }

  const ShowStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "50%",
      height: "max-content",
      zIndex: "10",
      borderRadius: "30px",
    },
  };

  const [removeModalIsOpen, setRemoveModalIsOpen] = useState(false);
  const [id, set_id] = useState();
  // open Remove modal function
  function openRemoveModal() {
    setRemoveModalIsOpen(true);
  }

  //close add modal function
  function closeRemoveModal() {
    setRemoveModalIsOpen(false);
  }
  const RemoveStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "220px",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };
  const userAttributes = [];
  userList &&
    userList.forEach((el) => {
      userAttributes.push({
        picture: el.picture ? (
          <img className="img_style" src={`${BaseUrl}${el.picture}`} alt="" />
        ) : (
          <img className="img_style" src={No_Image} alt="" />
        ),
        name: el.name ? el.name : el.user_name,
        phoneNumber: el.phone_number ? el.phone_number : "تکمیل نشده",
        email: el.email ? el.email : "تکمیل نشده",
        information: (
          <>
            <button
              className="btn text_bold"
              onClick={() => {
                openShowWord();
                setWord(el.word_report.new);
                setType("N");
              }}
            >
              کلمات جدید
            </button>
            <button
              className="btn text_bold"
              onClick={() => {
                openShowWord();
                setWord(el.word_report.reviewed_words);
                setType("D");
              }}
            >
              کلمات مستقیم
            </button>
            <button
              className="btn text_bold"
              onClick={() => {
                openShowWord();
                setWord(el.word_report.reviewed_indirectly);
                setType("ID");
              }}
            >
              کلمات غیر مستقیم
            </button>
            <button
              className="btn text_bold"
              onClick={() => {
                openBadge();
                setBadgeData(el.badge);
                setType("B");
              }}
            >
              نشان ها ({el.badge.length})
            </button>
          </>
        ),
        operation: (
          <>
            {/* <button
                        className="btn text-success"
                    onClick={() => {
                        setInfoModal();
                        setUserData(el.picture, el.name, el.phone_number, el.meanings)
                    }}
                    >
                        ویرایش<CreateOutlinedIcon />
                    </button> */}
            <button
              className="btn text-danger"
              onClick={() => {
                setRemoveModalIsOpen(true);
                set_id(el._id);
              }}
            >
              حذف
              <CloseIcon />
            </button>
          </>
        ),
      });
    });

  const data = {
    columns: [
      {
        label: "عکس",
        field: "picture",
        sort: "asc",
        width: 50,
      },
      {
        label: "نـام",
        field: "name",
        sort: "asc",
        width: 50,
      },
      {
        label: "شماره تلفن همراه",
        field: "phoneNumber",
        sort: "asc",
        width: 20,
      },
      {
        label: "ایمیل",
        field: "email",
        sort: "asc",
        width: 20,
      },
      {
        label: "سایر اطلاعات",
        field: "information",
        sort: "asc",
        width: 20,
      },
      {
        label: "عملیات",
        field: "operation",
        sort: "asc",
        width: 50,
      },
    ],
    rows: userAttributes,
  };

  const deleteWord = () => {
    setLoading(true);
    axios
      .delete(
        `${bseurl}/admin/user/${id}?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setUserList(res.data.data.reverse());

        setLoading(false);
      })
      .catch((err) => {
        setErrorHandler(true);
        setTextModal(err.response.data.data);
        setLoading(false);
      });
  };

  const getuserList = () => {
    setLoading(true);
    axios
      .get(
        `${bseurl}/admin/userList?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setUserList(res.data.data.reverse());

        setLoading(false);
      })
      .catch((error) =>
        error.response.data.status === 401 ? history.push("/login") : null
      );
  };

  useEffect(() => {
    getuserList();
  }, []);

  return (
    <>
      <Header />
      <div className="container mt-5">
        <div className="User">
          <div className="col-12 mt-5 ">
            <MDBDataTable responsive striped bordered data={data} />
          </div>
        </div>
      </div>
      {loading && <Loading isOpen={loading} />}
      {removeModalIsOpen && (
        <Modal
          isOpen={removeModalIsOpen}
          onRequestClose={closeRemoveModal}
          style={RemoveStyles}
          closeTimeoutMS={200}
        >
          <div className="row d-flex align-items-center center px-4">
            <div>
              <CloseIcon
                fontSize="large"
                className="pointer"
                onClick={closeRemoveModal}
              />
              <span className="mr-2 text_bold">حذف مهـارت</span>
            </div>
          </div>
          <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
            آیا شما قصد حذف کردن این کاربر را دارید؟
          </div>
          <div className="row d-flex justify-content-center align-items-center mt-5">
            <button
              onClick={() => {
                return deleteWord(), closeRemoveModal();
              }}
              className="btn btn-outline-success px-5 mx-3"
            >
              بلــه
            </button>
            <button
              onClick={closeRemoveModal}
              className="btn btn-danger px-5 mx-3"
            >
              خیـر
            </button>
          </div>
        </Modal>
      )}
      {showWord && (
        <Modal
          isOpen={showWord}
          onRequestClose={closeShowWord}
          style={ShowStyles}
          closeTimeoutMS={200}
        >
          <div className="row d-flex align-items-center center px-4">
            <div>
              <CloseIcon
                fontSize="large"
                className="pointer"
                onClick={closeShowWord}
              />
              <span className="mr-2 text_bold">لیست کلمات</span>
            </div>
          </div>
          <div className="row justify-content-center align-items-center text_remove_Modal my-4 ">
            {word === null || word.length === 0 ? (
              <span className="">این یوزر هیچ کلمه جدیدی نیاموخته است</span>
            ) : (
              word.map((item, index) => {
                return type === "N" ? (
                  <span className="text_box_user">{item}</span>
                ) : (
                  <div className="text_box_user">
                    <div class="badge_style">
                      <span className="mt-1">{item.count}</span>
                    </div>
                    <span>{item.word}</span>
                  </div>
                );
              })
            )}
          </div>
        </Modal>
      )}
      {badge && (
        <Modal
          isOpen={badge}
          onRequestClose={closeBadge}
          style={BadgeStyles}
          closeTimeoutMS={200}
        >
          <div className="row d-flex align-items-center  px-4">
            <div>
              <CloseIcon
                fontSize="large"
                className="pointer"
                onClick={closeBadge}
              />
              <span className="mr-2 text_bold">نشان ها</span>
            </div>
          </div>
          <div className="row align-items-center align-content-start text_remove_Modal my-4 neshan_user scroll_addpicture_neshan">
            {badgeData.length === 0 ? (
              <span className="">این یوزر فاقد نشان است. </span>
            ) : (
              badgeData.map((item, index) => {
                let Lastlevel = null;
                let checkLastLevel = item.leveles.findIndex(
                  (e) => e.status === false
                );
                const lastlevel = () => {
                  if (checkLastLevel < 0) {
                    Lastlevel = item.leveles.length - 1;
                  } else {
                    return (Lastlevel = checkLastLevel);
                  }
                };
                lastlevel();

                return (
                  <>
                    <div className="col-12 col-xl-4 my-4">
                      <div className="border_neshan">
                        <div className="position-relative">
                          {item?.id?.picture !== null ? (
                            <img
                              src={`${BaseUrl}${item?.id?.picture}`}
                              className="img_style"
                            />
                          ) : (
                            "فاقد عکس"
                          )}
                          {/* <img
                            src={`${BaseUrl}${item.picture}`}
                            className="img_style"
                          /> */}
                          <span className="level_style text_bold">
                            سطح
                            {item?.id?.leveles?.length > 0
                              ? item?.id?.leveles[Lastlevel]?.priority
                              : 1}
                          </span>
                        </div>
                        <div className="d-flex flex-column mr-3">
                          <span className="text_bold my-1">
                            {item?.id?.title}
                          </span>
                          <span className="text_bold my-1">
                            {(item?.process === null ? 0 : item?.process) +
                              "/" +
                              (item?.id?.leveles?.length > 0
                                ? item?.id?.leveles[Lastlevel]?.item === null
                                  ? 0
                                  : item?.id?.leveles[Lastlevel]?.item
                                : 0)}
                          </span>
                        </div>
                      </div>
                    </div>
                  </>
                );
              })
            )}
          </div>
        </Modal>
      )}
      {errorHandler ? (
        <ErrorModal
          isOpen={errorHandler}
          error={textError}
          close={setErrorHandler}
        />
      ) : null}
    </>
  );
}
