import React from "react";
import Ellipsis from "@bit/joshk.react-spinners-css.ellipsis";
const Loading = () => {
  const loadingStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "5000",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "155px",
      backgroundColor: "transparent",
      border: "none",
      zIndex: "5000",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };
  return (
    <div className="loading-main">
      <div className="loading-img">
        <Ellipsis color="#fff" />
      </div>
    </div>
  );
};

export default Loading;
