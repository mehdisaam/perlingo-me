import React, { useEffect, useState } from "react";
import axios from "axios";
import Modal from "react-modal";
import { BaseUrl } from "../../Constant/Config";
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import CreateIcon from "@material-ui/icons/Create";
import PrePicture from "./PrePicture";
import TextField from "@material-ui/core/TextField";
import Select from "./SelectType";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";
import No_Image from "../../assets/img/No_Image.png";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router-dom";
import { BlockPicker } from "react-color";
import DoneIcon from "@material-ui/icons/Done";
import LevelCart from "./LevelCart";
import DeleteIcon from "@material-ui/icons/Delete";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

export default function Carts(props) {
  const history = useHistory();
  // State
  const [edit, setEdit] = useState(false);
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode
  const [wordName, setWordName] = useState(""); // sabt nam kalame
  const [type, setType] = useState(""); // sabt language
  const [id, setId] = useState();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [array, setArray] = useState([]);
  const [arrayList, setArrayList] = useState([]);
  const [removeModalIsOpen, setRemoveModalIsOpen] = useState(false);
  const [badgeColor, setBadgecolor] = useState("#00ccff"); // save color for api
  const [backgroundColor, setBackgroundColor] = useState("#00ccff"); // for react-color package to handle pick color
  const [colors, setColors] = useState([
    "#fc4848",
    "#7ac70c",
    "#00ccff",
    "#1cb0f6",
    "#ce82ff",
    "#ffc800",
    "#ffd900",
  ]); // show default color to user
  const [openColor, setOpenColor] = useState(false); // to open react-color component
  const [levels, setLevels] = useState([]); // level array in create badge
  const [editIndex, setEditIndex] = useState();
  const [mergeArray, setMergeArray] = useState([]);
  // open Remove modal function
  function openRemoveModal() {
    setRemoveModalIsOpen(true);
  }

  //close add modal function
  function closeRemoveModal() {
    setRemoveModalIsOpen(false);
  }
  const RemoveStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "220px",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };

  const handleLanguage = (event) => {
    setType(event.target.value);
  };
  const Picturehandler = (e) => {
    console.log(e.target.files[0])
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  const clearState = () => {
    setEdit(false);
    setPicture("");
    setPrePicture("");
    setWordName("");
    setType("");
    setId("");
    setArray([]);
    setMergeArray([]);
  };
  const editBadge = () => {
    setLoading(true);
    let headers = { "Content-Type": "multipart/form-data" };
    let data = new FormData();
    data.append("picture", picture);
    data.append("title", wordName);
    data.append("type", type);
    data.append("leveles", JSON.stringify(mergeArray));
    data.append("color", badgeColor);
    data.append("description", "");
    data.append("id", id);

    axios
      .put(
        `${bseurl}/admin/Badge?api_token=${localStorage.getItem("token")}`,
        data,
        headers
      )
      .then((res) => {
        props.setBadgeList(res.data.data.reverse());
        setLoading(false);
        clearState();
        // props.setCreateBadge(false);
        // props.setScrollTop(true);
      })
      .catch((err) => {
        if (err.status === "500") {
          setError("مشکلی پیش آمده است");
          setErrorModal(true);
        }
        setError(err.response.data);
        setErrorModal(true);
        setLoading(false);
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const deleteBadge = (_id) => {
    setLoading(true);
    axios
      .delete(
        `${bseurl}/admin/Badge/${_id}?api_token=${localStorage.getItem(
          "token"
        )}`
      )
      .then((res) => {
        props.setBadgeList(res.data.data.reverse());
        setLoading(false);
        setEdit(false);
        props.setCreateBadge(false);
        props.setScrollTop(true);
      })
      .catch((err) => {
        if (err.status === "500") {
          setError("مشکلی پیش آمده است");
          setErrorModal(true);
        }
        setError(err.response.data);
        setErrorModal(true);
        setLoading(false);
      });
  };

  useEffect(() => {
    setArrayList(props.badgeList);
  }, [props.badgeList]);

  useEffect(() => {
    mergeArray.length > 0 && editBadge();
  }, [mergeArray]);

  useEffect(() => {

  }, [picture, PrePicture]);

  return (
    <>
      {arrayList &&
        arrayList.map((item, index) => {
          return (
            <div
              key={index}
              className={`${edit && id === item._id ? "cart_Edit" : "cart_box"
                } mb-5`}
            >
              <div className="row d-flex justify-content-between align-items-center text_gray px-4 mt-2">
                <div>
                  <span className="mr-2 text_bold fs_xs_14px">مدیریت نشان</span>
                </div>
                <div className="d-flex align-items-center">
                  <div
                    className="pointer"
                    onClick={() => {
                      setEdit(true);
                      setPicture(item.picture);
                      setWordName(item.title);
                      setType(item.type);
                      setId(item._id);
                      setArray(item.leveles);
                      setBadgecolor(item.color);
                      setEditIndex(item.leveles.length);
                      setLevels([]);
                    }}
                  >
                    <CreateOutlinedIcon color="action" fontSize="large" />
                  </div>
                  {edit && id === item._id ? (
                    <div
                      className="pointer"
                      onClick={() => {
                        setEdit(false);
                      }}
                    >
                      <CloseIcon color="action" fontSize="large" />
                    </div>
                  ) : (
                    <div
                      className="pointer"
                      onClick={() => {
                        setId(item._id);
                        openRemoveModal();
                      }}
                    >
                      <DeleteOutlinedIcon color="action" fontSize="large" />
                    </div>
                  )}
                  {edit && id === item._id && (
                    <div
                      onClick={() => {
                        setMergeArray([...item.leveles, ...levels]);
                      }}
                      className="pointer"
                    >
                      <SaveOutlinedIcon color="action" fontSize="large" />
                    </div>
                  )}
                </div>
              </div>
              <hr className="hr" />
              <div className="row my-2 d-flex align-items-center pb-5">
                <div className="col-12 col-lg-2 d-flex justify-content-center justify-content-lg-start align-items-center">
                  {edit && id === item._id ? (
                    <PrePicture
                      id={"AddWord"}
                      Picturehandler={Picturehandler}
                      prePicture={prePicture}
                      editpicture={picture}
                    />
                  ) : item.picture ? (
                    <div className="image_box">
                      <img
                        className="image_style"
                        src={`${BaseUrl}${item.picture}`}
                        alt=""
                      />
                    </div>
                  ) : (
                    <div className="image_box">
                      <img className="image_style" src={No_Image} alt="" />ّ
                    </div>
                  )}
                </div>
                <div className="col-12 col-lg-4 mt-4">
                  {edit && id === item._id ? (
                    <div className="d-flex justify-content-center justify-content-lg-start align-items-center mb-5">
                      <div className="w-100 neshan_name">
                        <TextField
                          onChange={(e) => setWordName(e.target.value)}
                          label="نشان سلطنتی"
                          value={wordName}
                          color="success"
                          id="word"
                        />
                      </div>
                    </div>
                  ) : (
                    <div className="neshan_name text-center">
                      <span className="text_bold">عنوان نشان : </span>
                      <span className="text_bold">{item.title}</span>
                    </div>
                  )}
                </div>
                <div className="col-12 col-lg-3 mt-4">
                  {edit && id === item._id ? (
                    <div className="d-flex align-items-center w-100 neshan_name mb-5">
                      <span className="w-100 mt-4 ml-3">
                        جهت دریافت نیاز به :
                      </span>
                      <Select
                        typedefault={type}
                        handleLanguage={(e) => handleLanguage(e)}
                        type={type}
                      />
                    </div>
                  ) : (
                    <div className="neshan_name text-center">
                      <span className="w-100 text_bold mt-4 ml-3">
                        جهت دریافت نیاز به :
                      </span>
                      <span className="text_bold">{item.type}</span>
                    </div>
                  )}
                </div>
                <div className="col-12 col-lg-3 text-center mt-4">
                  {edit && id === item._id ? (
                    <div className="col-12 text-center">
                      <span
                        onClick={() => setOpenColor(true)}
                        className="d-flex justify-content-center mb-4 pointer"
                      >
                        رنگ : {badgeColor}
                        <div
                          className="showcolor mr-3"
                          style={{ backgroundColor: badgeColor }}
                        ></div>
                      </span>
                      {openColor && (
                        <div className="col-12 d-flex justify-content-center mt-1 position-relative">
                          <BlockPicker
                            className="w-100"
                            color={backgroundColor}
                            onChangeComplete={setBackgroundColor}
                            colors={colors}
                          />
                          <div
                            className="Color_registration pointer"
                            onClick={() => {
                              setOpenColor(false);
                              setBadgecolor(backgroundColor.hex);
                            }}
                          >
                            <DoneIcon className="text-white" />
                          </div>
                        </div>
                      )}
                    </div>
                  ) : (
                    <span
                      // onClick={() => setOpenColor(true)}
                      className="d-flex justify-content-center"
                    >
                      رنگ : {item.color}
                      <div
                        className="showcolor mr-3"
                        style={{ backgroundColor: item.color }}
                      ></div>
                    </span>
                  )}
                </div>
              </div>
              {/* <div className={`${edit && id == item._id ? "openbox_badge" : "closebox_badge"}`}> */}
              {edit && id === item._id && (
                <>
                  <hr className="hr" />
                  <div className="row px-3">
                    {item.leveles.map((level, index2) => {
                      return (
                        <div className="col-12 my-2">
                          <div className="row">
                            <div className="col-12 col-lg-6 d-flex align-items-center justify-content-between   ">
                              <span className="text_bold">
                                مرحله : {index2 + 1}
                              </span>
                              {edit && id === item._id ? (
                                <div className="w-75 neshan_name">
                                  <TextField
                                    onChange={(e) => {
                                      array[index2].item = e.target.value;
                                    }}
                                    label="مقدار"
                                    defaultValue={level.item}
                                    color="success"
                                  />
                                </div>
                              ) : (
                                <span className="ltr text_bold">
                                  {level.item + " " + item.type} : مقدار
                                </span>
                              )}
                              {/* {prePicture ? (
                                                        <div className="image_box_level">
                                                            <img
                                                                className="image_style"
                                                                src={`${prePicture}`}
                                                                alt=""
                                                            />
                                                        </div>
                                                    ) : (
                                                        <div className="image_box_level">
                                                            <img
                                                                className="image_style"
                                                                src={`${BaseUrl}${item.picture}`}
                                                                alt=""
                                                            />
                                                        </div>
                                                    )} */}
                              {/* {item.picture ?
                                                        <div className="image_box_level">
                                                            <img
                                                                className="image_style"
                                                                src={`${BaseUrl}${item.picture}`}
                                                                alt=""
                                                            />
                                                        </div>
                                                        :
                                                        <div className="image_box_level">
                                                            <img
                                                                className="image_style"
                                                                src={No_Image}
                                                                alt=""
                                                            />
                                                        </div>
                                                    } */}
                            </div>
                            <div className="col-12 col-lg-6 d-flex align-items-center justify-content-center my-3">
                              {edit && id === item._id ? (
                                <TextField
                                  type="text"
                                  className="w-100"
                                  label="توضیحات"
                                  defaultValue={level.description}
                                  disabled={
                                    edit && id === item._id ? false : true
                                  }
                                  onChange={(e) => {
                                    array[index2].description = e.target.value;
                                  }}
                                />
                              ) : (
                                <span className="text_bold">
                                  توضیحات : {level.description}
                                </span>
                              )}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </>
              )}
              {edit && id === item._id && (
                <>
                  <div>
                    <LevelCart
                      data={levels}
                      setData={setLevels}
                      editIndex={editIndex}
                      arrayL={levels.length}
                    />
                  </div>
                  <div
                    className="row d-flex justify-content-center align-items-center text_gray pointer"
                    onClick={() => {
                      setLevels([...levels, { priority: editIndex + 1 }]);
                      setEditIndex(editIndex + 1);
                    }}
                  >
                    <span class="text_gray mt-2 my-4 w-75 add-class-style">
                      + مرحله جدید
                    </span>
                  </div>
                </>
              )}
            </div>
            // arrayList[index].leveles.length
            // </div>
          );
        })}
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
      {removeModalIsOpen && (
        <Modal
          isOpen={removeModalIsOpen}
          onRequestClose={closeRemoveModal}
          style={RemoveStyles}
          closeTimeoutMS={200}
        >
          <div className="row d-flex align-items-center center px-4">
            <div>
              <CloseIcon
                fontSize="large"
                className="pointer"
                onClick={closeRemoveModal}
              />
              <span className="mr-2 text_bold">حذف مهـارت</span>
            </div>
          </div>
          <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
            آیا شما قصد حذف کردن این نشان را دارید؟
          </div>
          <div className="row d-flex justify-content-center align-items-center mt-5">
            <button
              onClick={() => {
                deleteBadge(id);
                closeRemoveModal();
              }}
              className="btn btn-outline-success px-5 mx-3"
            >
              بلــه
            </button>
            <button
              onClick={closeRemoveModal}
              className="btn btn-danger px-5 mx-3"
            >
              خیـر
            </button>
          </div>
        </Modal>
      )}
    </>
  );
}
