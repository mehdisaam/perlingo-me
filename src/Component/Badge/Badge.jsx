import React, { useEffect, useRef, useState } from "react";
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import Header from "../Header/Header";
import PrePicture from "../AddWord/PrePicture";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import { BaseUrl } from "../../Constant/Config";
import Select from "./SelectType";
import Carts from "./Carts";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";
import { BlockPicker } from "react-color";
import DoneIcon from "@material-ui/icons/Done";
import LevelCart from "./LevelCart";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router-dom";
export default function Badge() {
  const history = useHistory();
  // state
  const ref = useRef();
  const executeScroll = () => scrollToRef(ref);
  const scrollToRef = (ref) =>
    window.scrollTo({
      top: ref.current.offsetTop,
      behavior: "smooth",
    });
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode
  const [addWord, setAddWord] = useState(false); // modal afzodan kalame
  const [wordName, setWordName] = useState(""); // sabt nam kalame
  const [xp, setXp] = useState(""); // sabt nam kalame
  const [type, setType] = useState(""); // sabt language
  const [badgeList, setBadgeList] = useState();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [CreateBadge, setCreateBadge] = useState(false);
  const [backgroundColor, setBackgroundColor] = useState("#00ccff"); // for react-color package to handle pick color
  const [colors, setColors] = useState([
    "#fc4848",
    "#7ac70c",
    "#00ccff",
    "#1cb0f6",
    "#ce82ff",
    "#ffc800",
    "#ffd900",
  ]); // show default color to user
  const [openColor, setOpenColor] = useState(false); // to open react-color component
  const [badgeColor, setBadgecolor] = useState("#00ccff"); // save color for api
  const [levels, setLevels] = useState([]); // level array in create badge
  const [scrollTop, setScrollTop] = useState(false);
  const scrolltop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
    setScrollTop(false);
  };
  const ClearState = () => {
    setPicture(undefined);
    setWordName("");
    setType("");
    setLevels([]);
    setBadgecolor("#00ccff");
  };
  const createBadge = () => {
    setLoading(true);
    let headers = { "Content-Type": "multipart/form-data" };
    let data = new FormData();
    data.append("picture", picture);
    data.append("title", wordName);
    data.append("type", type);
    data.append("leveles", JSON.stringify(levels));
    data.append("color", badgeColor);
    data.append("description", "");

    axios
      .post(
        `${BaseUrl}/api/v1/admin/Badge?api_token=${localStorage.getItem(
          "token"
        )}`,
        data,
        headers
      )
      .then((res) => {
        setBadgeList(res.data.data.reverse());
        setLoading(false);
        setCreateBadge(false);
        setScrollTop(true);
        ClearState();
      })
      .catch((err) => {
        if (err.status === "500") {
          setError("مشکلی پیش آمده است");
          setErrorModal(true);
        }
        setError(err.response.data);
        setErrorModal(true);
        setLoading(false);
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const GetBadge = () => {
    setLoading(true);
    axios
      .get(
        `${BaseUrl}/api/v1/admin/badgelist?api_token=${localStorage.getItem(
          "token"
        )}`
      )
      .then((res) => {
        setBadgeList(res.data.data.reverse());
        setLoading(false);
      })
      .catch((err) => {
        setError(err.response.data.data);
        setLoading(false);
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const handleLanguage = (event) => {
    setType(event.target.value);
  };

  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  useEffect(() => {
    CreateBadge && executeScroll();
    scrollTop && scrolltop();
  }, [CreateBadge, scrollTop]);

  useEffect(() => {
    GetBadge();
  }, []);

  return (
    <>
      <Header />
      <div className="container my-5">
        <div className="Badge">
          <div
            class="d-flex justify-content-start align-items-center pointer mt-5 mb-4"
            onClick={() => {
              setCreateBadge(true);
              CreateBadge && executeScroll();
            }}
          >
            <span class="text_gray add-class-style text_bold">
              + ثبت نشان جدید
            </span>
          </div>
          <div className="carts">
            <Carts
              badgeList={badgeList}
              setBadgeList={setBadgeList}
              badgeColor={badgeColor}
              setScrollTop={setScrollTop}
              setCreateBadge={setCreateBadge}
            />{" "}
          </div>
          <div className="create_badge" ref={ref}>
            <div className="row d-flex justify-content-between align-items-center text_gray px-4 mt-2">
              <div>
                <span className="mr-2 text_bold fs_xs_14px">ثبت نشان جدید</span>
              </div>
              <div className="d-flex align-items-center">
                <div
                  onClick={() => {
                    createBadge();
                  }}
                  className="pointer"
                >
                  <SaveOutlinedIcon color="action" fontSize="large" />
                </div>
              </div>
            </div>
            <hr className="hr" />
            <div className="row my-2">
              <div className="col-12 col-lg-2">
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center">
                  <PrePicture
                    id={"AddWord"}
                    Picturehandler={Picturehandler}
                    prePicture={prePicture}
                    picture={picture}
                  />
                </div>
              </div>
              <div className="col-12 col-lg-4 mt-4">
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center focus_lable">
                  <div className="w-100 neshan_name">
                    <TextField
                      onChange={(e) => setWordName(e.target.value)}
                      label="نشان سلطنتی"
                      value={wordName}
                      color="success"
                      id="word"
                    />
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-3 mt-4">
                <div className="d-flex justify-content-center justify-content-lg-start align-items-center focus_lable">
                  <div className="d-flex align-items-center w-auto neshan_name">
                    <span className="w-100 mt-4 ml-3">
                      جهت دریافت نیاز به :
                    </span>
                    <Select handleLanguage={handleLanguage} type={type} />
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-3 mt-4">
                <div className="col-12 text-center mt-4">
                  <span
                    onClick={() => setOpenColor(true)}
                    className="d-flex justify-content-center mt-4 pointer"
                  >
                    رنگ : {badgeColor}
                    <div
                      className="showcolor mr-3"
                      style={{ backgroundColor: badgeColor }}
                    ></div>
                  </span>
                </div>
                {openColor && (
                  <div className="col-12 d-flex justify-content-center mt-1 position-relative">
                    <BlockPicker
                      className="w-100"
                      color={backgroundColor}
                      onChangeComplete={setBackgroundColor}
                      colors={colors}
                    />
                    <div
                      className="Color_registration pointer"
                      onClick={() => {
                        setOpenColor(false);
                        setBadgecolor(backgroundColor.hex);
                      }}
                    >
                      <DoneIcon className="text-white" />
                    </div>
                  </div>
                )}
              </div>
            </div>

            <div>
              <LevelCart data={levels} setData={setLevels} />
            </div>
            <div
              className="row d-flex justify-content-center align-items-center text_gray pointer px-2"
              onClick={() =>
                setLevels([...levels, { priority: levels.length + 1 }])
              }
            >
              <span class="text_gray mt-2 my-4 add-class-style">
                + مرحله جدید
              </span>
            </div>
          </div>
        </div>
      </div>
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </>
  );
}
