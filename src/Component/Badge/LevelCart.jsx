import React, { useState, useEffect, useRef } from "react";
import TextField from "@material-ui/core/TextField";

export default function LevelCart(props) {
  const array = props.data;

  useEffect(() => {}, [props.editIndex, props.arrayL])

    return (
        <>
            {props.data &&
                props.data.map((level, index) => {
                    console.log(index, " index")
                    return (
                        <div className="col-12 my-2">
                            <div className="row">
                                <div className="col-12 col-lg-6 d-flex align-items-center justify-content-between   ">
                                    <span className="text_bold">مرحله : {props.editIndex ? props.editIndex - ((props.arrayL - index) - 1)   : index + 1}</span>
                                    <div className="w-75 neshan_name p_level_input">
                                        <TextField
                                            onChange={(e) => {
                                                array[index].item = e.target.value;
                                                props.setData(array);
                                                console.log(array);
                                            }}
                                            label="مقدار"
                                            color="success"
                                            id={`w${index}`}
                                        />
                                    </div>
                                </div>
                                <div className="col-12 col-lg-6 d-flex align-items-center justify-content-center my-3">
                                    <TextField
                                        type="text"
                                        label="توضیحات"
                                        className="mb-4 w-100"
                                        onChange={(e) => {
                                            array[index].description = e.target.value;
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    );
                })}
        </>
    );
}
