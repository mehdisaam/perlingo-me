import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    button: {
        display: 'block',
        marginTop: theme.spacing(2),
    },
    formControl: {
        margin: "-13px",
        minWidth: 120,
    },

}));



export default function SelectType(props) {
    const classes = useStyles();
    const [age, setAge] = React.useState('');
    const [open, setOpen] = React.useState(false);

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    return (
        <div className="selectLanguage">
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-controlled-open-select-label">نوع</InputLabel>
                <Select
                    labelId="demo-controlled-open-select-label"
                    id="demo-controlled-open-select"
                    open={open}
                    onClose={handleClose}
                    onOpen={handleOpen}
                    defaultValue={props.typedefault && props.typedefault}
                    value={props.type}
                    onChange={props.handleLanguage}
                >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    <MenuItem value={'xp'}>XP</MenuItem>
                    <MenuItem value={"crown"}>Crown</MenuItem>
                    <MenuItem value={"daily"}>Daily</MenuItem>
                    <MenuItem value={"new"}>new</MenuItem>
                    <MenuItem value={"weekend"}>Weekend</MenuItem>


                </Select>
            </FormControl>
        </div>
    );
}