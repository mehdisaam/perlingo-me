import React, { useState, useEffect } from "react";
import axios from "axios";
import { BaseUrl } from "../../../Constant/Config";
import Loading from "../../Error Modal/Loading";
import Error from "../../Error Modal/ErrorModal";
import DeleteIcon from "@material-ui/icons/Delete";
import CloseIcon from "@material-ui/icons/Close";

// icons
import DoneIcon from "@material-ui/icons/Done";
import { useDispatch } from "react-redux";
import { setSpeakingPic } from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

export default function WCarts(props) {
  const history = useHistory();
  // State
  const dispatch = useDispatch();
  const [checkBox, setCheckBox] = useState("");
  const [error, setError] = useState();
  const [errorModal, setErrorModal] = useState();
  const [loading, setLoading] = useState();
  const [pic_id, setPic_id] = useState("");

  useEffect(() => {
    dispatch(setSpeakingPic(checkBox));
  }, [dispatch, checkBox]);

  // delete picture
  const DeletePicture = () => {
    setLoading(true);
    axios
      .delete(
        `${bseurl}/admin/picture/${pic_id}?api_token=${localStorage.getItem(
          "token"
        )}`
      )
      .then((res) => {
        props.setData(res.data.data.reverse());
        setLoading(false);
      })
      .catch((err) => {
        return (
          setError(err.response.data.data),
          setLoading(false),
          setErrorModal(true),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };

  return (
    <div className="Wordbank_Modal">
      <div className="row pb-5 mb-5">
        {props.data.map((item, index) => {
          return (
            <>
              <div
                className="col-6 col-lg-2 col-xl-2 pointer "
                key={item?._id}
                onClick={() => {
                  setCheckBox(item?._id);
                  props?.setPic_id(item?._id);
                  props?.setSelPic(item?.picture);
                }}
                onMouseEnter={() => setPic_id(item._id)}
                onMouseLeave={() => setPic_id("")}
              >
                <div className="d-flex justify-content-center justify-content-md-center ">
                  {pic_id === item._id && (
                    <div
                      className="delete_circle"
                      onClick={() => {
                        DeletePicture(item._id);
                      }}
                    >
                      <CloseIcon fontsize="large" className="text-white" />
                    </div>
                  )}
                  <div className="img_box mx-4">
                    <img
                      className="images"
                      src={`${BaseUrl}${item.picture}`}
                      alt="flashcart_image"
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-center align-items-center text_gray p-3 rtl">
                  <div className="checkbox">
                    {checkBox === item._id && (
                      <DoneIcon fontsize="large" className="text-success" />
                    )}
                  </div>
                </div>
              </div>
            </>
          );
        })}
      </div>
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </div>
  );
}
