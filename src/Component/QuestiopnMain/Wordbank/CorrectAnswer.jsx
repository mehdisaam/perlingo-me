import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { useDispatch } from "react-redux";
import {
  setAllAnswer,
  setCorrectAnswerNew,
  setArrayNewWorldFooter,
} from "../../../redux/action/user";
export default function CorrectAnswer(props) {
  // State
  const dispatch = useDispatch();
  const [chosenWord, setChosenWord] = useState([]);
  const [chipValue, setChipValue] = useState(
    allanswerFromApi > 0 ? allanswerFromApi : ""
  );
  const [style, setStyle] = useState(
    allanswerFromApi > 0 ? allanswerFromApi : ""
  );
  // functions
  const onChangeHandler = (e, value) => {
    setChosenWord(value);
    props.setCorrectAnswer(value);
    dispatch(setCorrectAnswerNew(value));
    if (props.type) {
    }
  };

  var allanswerFromApi;
  if (props?.answerinapi) {
    allanswerFromApi = props?.answerinapi?.answer[0].correct_answer;
  }

  const handlechange = (e) => {
    setStyle(e.target.value);
    document
      .querySelector(".placeholder_style_correct")
      .classList.add("beforeEnter");
    if (style === " ") {
      document
        .querySelector(".placeholder_style_correct")
        .classList.add("beforeEnter");
    }
  };

  const onKeyHandler = (event) => {
    if (event.key === "Enter") {
      document
        .querySelector(".placeholder_style_correct")
        .classList.add("afterEnter");
    }
  };

  return (
    <>
      <div className="correct">
        <Autocomplete
          onChange={(e, v) => onChangeHandler(e, v)}
          className="rtl correct w-100"
          multiple
          id="tags-filled"
          defaultValue={allanswerFromApi && allanswerFromApi}
          options={chosenWord && chosenWord.map((option) => option)}
          freeSolo
          renderTags={(chipValue, getTagProps) =>
            chipValue.map((option, index) => (
              <Chip
                variant="outlined"
                label={option}
                {...getTagProps({ index })}
              />
            ))
          }
          autoComplete
          filterSelectedOptions
          renderInput={(params) => (
            <TextField
              className="placeholder_style_correct"
              {...params}
              variant="filled"
              placeholder={"  جواب صحیح را وارد کنید و اینتر را بزنید "}
              filterSelectedOptions
              autoComplete="off"
              onKeyDown={(e) => onKeyHandler(e)}
              onChange={handlechange}
              value={style}
            />
          )}
        />
      </div>
    </>
  );
}
