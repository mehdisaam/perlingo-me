import React, { useState, useEffect } from "react";
import PrePicture from "./PrePicture";
import axios from "axios";
import Question from "./AutocompleteWordbank";
import Correct from "./CorrectAnswer";
import Wordsuggest from "./WordsforSelect";
import BoxAddword from "../../QuestiopnMain/ChoseWordLang";
import TextField from "@material-ui/core/TextField";

import { useDispatch, useSelector } from "react-redux";
import { setTitle, setValidation } from "../../../redux/action/user";
// url
import { BaseUrl } from "../../../Constant/Config";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

export default function Wordbank(props) {
  const titleApi = props?.item?.title;
  // const pictureApi = props.item.picture.picture;
  // const answerApi = props.item.answer[0].correct_answer;
  // State
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode baraye neshan dadan
  const [wordList, setWordList] = useState([]);
  const [filterWord, setFilterWord] = useState([]);
  const [filterWordEn, setFilterEn] = useState([]);
  const [filterChangeLangFa, setLangFa] = useState([]);
  const [langBlankChoise, setLangBlankChoise] = useState("fa");
  // api content
  const [pic_id, setPic_id] = useState(); //sabt id braye ersal be server property picture
  const [questionWord, setQuestionWord] = useState();
  const [correctAnswer, setCorrectAnswer] = useState([]);
  const [answerForUser, setAnswerForUser] = useState([]);
  const [titlef, setTitlef] = useState(titleApi ? titleApi : "");
  // words
  //  correct_awnser
  // const handleFilterWord = (words, language) => {
  //   setFilterWord(words?.filter((word) => word.language === language));
  //   setLang(language);
  // };

  //functions
  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  const handletitle = (e) => {
    dispatch(setTitle(e.target.value));
    setTitlef(e.target.value);
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) =>
        error?.response?.data.status === 401 ? history.push("/login") : null
      );
  };
  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    setLangBlankChoise(language);
  };
  // const handleFilterFa = (words, language) => {
  //   setLangFa(words?.filter((word) => word.language === "fa"));
  // };
  // const handleChangeEn = (words, language) => {
  //   setFilterEn(words?.filter((word) => word.language === "en"));
  // };
  useEffect(() => {
    getWordList();
    // handleFilterWord();
  }, []);
  useEffect(() => {
    handleFilterWord(wordList);
    // handleChangeEn(wordList);
    // handleFilterFa(wordList);
  }, []);

  return (
    <div className="Wordbank">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 title_of_questions">
            <TextField
              variant="filled"
              id="standard-basic "
              // value={title}
              className="text_bold"
              label={<p>عنوان سوال</p>}
              onChange={handletitle}
              autoComplete="off"
              value={titlef}
            />
          </div>
          <div className="col-12 d-block d-lg-flex align-items-center ">
            <div className="d-flex align-items-center justify-content-center">
              <PrePicture
                id="Wordbank"
                pic_id={pic_id}
                setPic_id={setPic_id}
                Picturehandler={Picturehandler}
                prePicture={prePicture}
                picture={picture}
                answerinapi={props.item}
              />
            </div>
            {/* <div className="d-flex align-items-center justify-content-center "> */}
            <div>
              <dic className="d-flex justify-content-center justify-content-lg-end mt-4 mt-lg-0">
                <BoxAddword setWordList={setWordList} />
              </dic>
              <div className="d-flex justify-content-center align-items-center mt-3">
                <div className="mosalas d-none d-lg-block"></div>
                <div className="title_word">
                  <Question
                    setQuestionWord={setQuestionWord}
                    Words={wordList}
                    type="word-bank"
                    answerinapi={props.item}
                  />
                </div>
              </div>
            </div>

            {/* </div> */}
          </div>
        </div>
        <div className="row py-5">
          <div className="col-12 correct_awnser">
            <div className="px-3">
              <Correct
                setCorrectAnswer={setCorrectAnswer}
                answerinapi={props.item}
              />
            </div>
          </div>

          <div className="col-12 Wordsuggest d-flex justify-content-between align-items-center">
            <Wordsuggest
              setAnswerForUser={setAnswerForUser}
              words={wordList}
              type="word-bank"
              filterChangeLangFa={filterChangeLangFa}
              answerinapi={props.item}
            />
          </div>
          {/* <div className="col-12 correct_awnser mt-3">
            <hr className="hr" />
          </div> */}
        </div>
      </div>
    </div>
  );
}
