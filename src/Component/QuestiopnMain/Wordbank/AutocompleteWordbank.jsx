import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";

import {
  setWordsMain,
  setWordInAnswer,
  setArrayNewWorldFooter,
  setLanguageType,
  setNewArrayFooterUi,
} from "../../../redux/action/user";

export default function AutocompleteWordbank(props) {
  const user = useSelector((state) => state.user);

  var wordbankwords = props.wordbankanswerinapi?.words?.map(
    (item) => item.word
  );

  const dispatch = useDispatch();
  const [filterWord, setFilterWord] = useState([]);
  const [chosenWord, setChosenWord] = useState([]);

  const [lang, setLang] = useState("fa");
  var wordTextArray = [];
  var newArrayFooter = [];
  var newArrayFooterUi = [];
  // functions

  const onChangeHandler = (e, value) => {
    value.forEach((item) => {
      newArrayFooter.push(item.word);
    });
    user.conditionalType !== "T" &&
      value.forEach((item) => {
        newArrayFooterUi.push({ word: item.word });
        dispatch(setNewArrayFooterUi(newArrayFooterUi));
      });

    value.forEach((item) =>
      wordTextArray.push({
        word_id: item._id,
        word: item.word,
      })
    );
    // dispatch(setArrayNewWorldFooter(newArrayFooter));
    // dispatch(setNewArrayFooterUi(newArrayFooterUi));

    if (props.type === "blank-type" || props.type === "listening") {
      dispatch(setWordsMain(wordTextArray));
    }

    if (props.type === "word-bank") {
      // dispatch(setArrayNewWorldFooter(newArrayFooter));
      dispatch(setWordsMain(wordTextArray));
    }

    if (props.type === "listening") {
      dispatch(setWordInAnswer(wordTextArray));
    }
    setChosenWord(value);
    props.setQuestionWord(value);
  };
  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    setLang(language);
    dispatch(setLanguageType(language));
  };
  var allanswerFromApi = props?.answerinapi?.words?.map((item) => item);

  const handleFilterWord1 = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    // setLangBlankChoise(language);
  };

  useEffect(() => {
    setLang(user.typeLanguage);
    handleFilterWord(props?.Words, user.typeLanguage === "fa" ? "en" : "fa");
  }, [props.Words]);

  return (
    <>
      <div className="container wordbank_taginput">
        <div className="row">
          <div class="d-flex w-100 position-relative">
            <Autocomplete
              onChange={(e, v) => onChangeHandler(e, v)}
              className={`rtl wordbank_taginput_${lang} w-100`}
              multiple
              defaultValue={allanswerFromApi?.map((word, index) => word)}
              filterSelectedOptions
              id="tags-standard"
              options={filterWord && filterWord.map((option) => option)}
              getOptionLabel={(option) => option.word}
              // getOptionDisabled={(options) => (chosenWord.length > 3 ? true : false)}
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip
                    variant="outlined"
                    label={option.word}
                    {...getTagProps({ index })}
                  />
                ))
              }
              renderInput={(params) => (
                <TextField
                  className="placeholder_style"
                  {...params}
                  variant="filled"
                  placeholder={lang === "fa" ? "انتخاب کلمه" : "Chose word"}
                />
              )}
            />

            <div className={`d-flex justify-content-center align-items-center`}>
              <button
                className={`btn ${lang === "fa" ? "text_bold" : ""}`}
                onClick={() => handleFilterWord(props.Words, "fa")}
              >
                Fa
              </button>
              <span>|</span>
              <button
                onClick={() => handleFilterWord(props.Words, "en")}
                className={`btn ${lang === "en" ? "text_bold" : ""}`}
              >
                En
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
