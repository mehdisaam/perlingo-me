import React, { useState } from "react";
import axios from "axios";
import Loading from "../../Error Modal/Loading";
import Error from "../../Error Modal/ErrorModal";
import { BaseUrl } from "../../../Constant/Config";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

export default function Addpicture(props) {
  //State
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [picture, setPicture] = useState();

  const changefilehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      ADDPicture(e.target.files[0]);
    }
  };

  const ADDPicture = (picture) => {
    setLoading(true);
    let data = new FormData();
    data.append("picture", picture);

    let headers = { "Content-Type": "multipart/form-data" };
    axios
      .post(
        `${bseurl}/admin/picture?api_token=${localStorage.getItem("token")}`,
        data,
        headers
      )
      .then((res) => {
        props.setData(res.data.data.reverse());
        setLoading(false);
      })
      .catch((err) => {
        return (
          setError(err.response.data.data),
          setLoading(false),
          setErrorModal(true),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };

  return (
    <>
      <label
        className="d-flex justify-content-center align-items-center pointer"
        for={props.id}
      >
        <span className="text_bold mt-1">افزودن تصویر</span>
      </label>

      <input
        id={props.id}
        type="file"
        onChange={(e) => changefilehandler(e)}
        accept=".png"
        hidden
      />
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </>
  );
}
