import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";
import { setWordInAnswer } from "../../../redux/action/user";
export default function WordsforSelect(props) {
  // State
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [chosenWord, setChosenWord] = useState([]);
  const [filterWord, setFilterWord] = useState([]);

  const [lang, setLang] = useState();
  //?.words?.map((item) => item.word)
  // functions
  var arrayWoerdAnswer = [];
  var wordbankchip =
    props?.answerinapi?.answer[0] &&
    props?.answerinapi?.answer[0]?.word.map((item) => item);

  const onChangeHandler = (e, value) => {
    value.forEach((item) =>
      arrayWoerdAnswer.push({ word: item.word, word_id: item._id })
    );
    setChosenWord(value);
    props.setAnswerForUser(value);
    dispatch(setWordInAnswer(arrayWoerdAnswer));
  };

  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    // setLangBlankChoise(language);
  };

  useEffect(() => {
    setLang(user.typeLanguage);
    handleFilterWord(props.words, user.typeLanguage === "fa" ? "en" : "fa")
  }, [user.typeLanguage]);

  return (
    <>
      <Autocomplete
        onChange={(e, v) => onChangeHandler(e, v)}
        className={`rtl  w-100`}
        multiple
        filterSelectedOptions
        id="tags-standard"
        defaultValue={wordbankchip && wordbankchip.map((item) => item)}
        options={
          filterWord && filterWord.map((option) => option)
          // props.words
          //   ? props.words.map((option) => option)
          //   : lang === "en"
          //   ? props.filterChangeLangFa &&
          //     props.filterChangeLangFa.map((option) => option)
          //   : lang === "fa"
          //   ? props.filterWordEn && props.filterWordEn.map((option) => option)
          //   : props.filterChangeLangFa
        }
        getOptionLabel={(option) => option.word}
        autoComplete
        // getOptionDisabled={(options) => (chosenWord.length > 3 ? true : false)}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant="outlined"
              label={option.word}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            className="placeholder_style"
            {...params}
            variant="filled"
            placeholder=" لیست کلمات قابل انتخاب توسط کاربر را از بانک کلمات انتخاب کنید "
            autoComplete="off"
          />
        )}
      />
    </>
  );
}
