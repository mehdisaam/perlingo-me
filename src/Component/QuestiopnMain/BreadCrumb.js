import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";
const BreadCrumb = (props) => {
  return (
    <Breadcrumb className="container p-0">
      <Breadcrumb.Item active href={"#"}>
        {props.firstParent}
      </Breadcrumb.Item>

      <Breadcrumb.Item href={props.secondlink}>
        {props.secondParent}
      </Breadcrumb.Item>
      {props.thirdParent ? (
        <Breadcrumb.Item href={props.thirdLink}>
          {props.thirdParent}
        </Breadcrumb.Item>
      ) : null}
    </Breadcrumb>
  );
};

export default BreadCrumb;
