import React, { useState, useEffect } from "react";
import Question from "./AutocompleteSpeaking";
import BoxAddword from "../../QuestiopnMain/ChoseWordLang";
import PrePicture from "./Pictures";
import axios from "axios";
import { BaseUrl } from "../../../Constant/Config";
import { useDispatch, useSelector } from "react-redux";
import { setSpeakingData } from "../../../redux/action/user";
import TextField from "@material-ui/core/TextField";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";

import { setTitle, setValidation } from "../../../redux/action/user";
import { useHistory } from "react-router";
export default function Speaking(props) {
  const history = useHistory();
  // State
  const dispatch = useDispatch();
  const [titlef, setTitlef] = useState(
    props?.item?.title ? props?.item?.title : ""
  );
  const user = useSelector((state) => state.user);
  const [pic_id, setPic_id] = useState(); //sabt id braye ersal be server property picture
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode baraye neshan dadan
  const [wordList, setWordList] = useState([]); // hameye kalame ha
  const [questionWord, setQuestionWord] = useState(); // sabt klmat soal

  //functions
  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  const handletitle = (e) => {
    dispatch(setTitle(e.target.value));
    setTitlef(e.target.value);
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) =>
        error.response.data.status === 401 ? history.push("/login") : null
      );
  };

  useEffect(() => {
    getWordList();
  }, []);

  return (
    <div className="Speaking">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 title_of_questions">
            <TextField
              variant="filled"
              id="standard-basic "
              // value={title}
              className="text_bold"
              label={<p>عنوان سوال</p>}
              onChange={handletitle}
              autoComplete="off"
              value={titlef}
            />
          </div>
          <div className="col-12 mb-5">
            <div className="text-right">
              <span className="text_bold">
                کلمات مورد نظر را به ترتیب برای عنوان سوال انتخاب کنید و جای
                خالی را مشخص کنید.
              </span>
            </div>
          </div>
          <div className="col-12 d-block d-lg-flex align-items-center ">
            <div className="d-flex align-items-center justify-content-center">
              <PrePicture
                id="Speaking"
                pic_id={pic_id}
                setPic_id={setPic_id}
                Picturehandler={Picturehandler}
                prePicture={prePicture}
                picture={picture}
                answerinapi={props.item}
              />
            </div>
            <div>
              <dic className="d-flex justify-content-center justify-content-lg-end mt-4 mt-lg-0">
                <BoxAddword setWordList={setWordList} />
              </dic>
              <div className="d-flex justify-content-center align-items-center mt-3">
                <div className="mosalas d-none d-lg-block"></div>
                <div className="title_word">
                  <Question
                    setQuestionWord={setQuestionWord}
                    Words={wordList}
                    answerinapi={props.item}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
