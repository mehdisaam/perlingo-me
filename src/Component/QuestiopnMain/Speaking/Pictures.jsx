import React, { useState } from "react";
import Modal from "react-modal";
import HeaderModal from "../../Error Modal/Header";
import axios from "axios";
import { BaseUrl } from "../../../Constant/Config";
import Loading from "../../Error Modal/Loading";
import Error from "../../Error Modal/ErrorModal";
import AddIcon from "@material-ui/icons/Add";
import Carts from "./PicturesCart";
import Addpicture from "../Wordbank/Addpicture";
import { useDispatch } from "react-redux";
import { setTitle, setSpeakingPic } from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

export default function Pictures(props) {
  // State
  const history = useHistory();
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [data, setData] = useState([]);
  const [selPic, setSelPic] = useState(); //fot get picture before save in modal
  const [picture, setPicture] = useState(props?.answerinapi?.picture?.picture);
  // props?.answerinapi?.picture.picture
  //   ? props?.answerinapi?.picture?.picture
  //   : "" //show image after modal be closed
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const PictureStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "70%",
      height: "80%    ",
      zIndex: "10",
      borderRadius: "30px",
      overflow: "hidden",
    },
  };

  const handleAddWord = (e) => {
    if (e.keyCode === 13 && props.pic_id) {
      setPicture(selPic);
      closeModal();
      // props.setPic_id('')
    }
  };

  const getPicture = () => {
    setLoading(true);
    axios
      .get(`${bseurl}/admin/picture?api_token=${localStorage.getItem("token")}`)
      .then((res) => {
        setData(res.data.data.reverse());
        setLoading(false);
      })
      .catch((err) => {
        return (
          setError(err.response.data.data),
          setLoading(false),
          setErrorModal(true),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };

  return (
    <>
      <div
        className="add_img_box pointer"
        onClick={() => {
          openModal();
          getPicture();
        }}
      >
        {picture ? (
          <img
            className="images"
            src={`${BaseUrl}${picture}`}
            alt="flashcart_image"
          />
        ) : (
          <AddIcon className="text_gray" fontSize="large" />
        )}
      </div>
      {isOpen && (
        <Modal
          isOpen={isOpen}
          onRequestClose={closeModal}
          style={PictureStyle}
          closeTimeoutMS={200}
          shouldCloseOnOverlayClick={false}
        >
          <div
            className="outline-none"
            onKeyDown={(e) => handleAddWord(e)}
            tabIndex="0"
          >
            <HeaderModal
              newbutton={<Addpicture setData={setData} id={"add picture"} />}
              label="تصاویر"
              Close={closeModal}
              Api={() => {
                setPicture(selPic);
                closeModal();
              }}
            />
            <div className="row scroll_addpicture">
              <Carts
                setSelPic={setSelPic}
                setPic_id={props.setPic_id}
                data={data}
                setData={setData}
              />
            </div>
          </div>
        </Modal>
      )}
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </>
  );
}
