import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";
import {
  setWordsMain,
  setCorrectAnswerNew,
  setArrayNewWorldFooter,
  setlangFor_T_Wb,
  setNewArrayFooterUi,
  setLanguageType,
} from "../../../redux/action/user";
export default function AutocompleteSpeaking(props) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  // State
  const [filterWord, setFilterWord] = useState([]);
  const [chosenWord, setChosenWord] = useState([]);
  const [lang, setLang] = useState("fa");
  const [correctAwnser, setCorrectAnswer] = useState([]);
  var newArrayFooter = [];
  var newArrayFooterUi = [];
  // functions
  const onChangeHandler = (e, value) => {
    setChosenWord(value);
    props.setQuestionWord(value);
    let word = [];
    value.map((item) => {
      word.push({ word_id: item._id, word: item.word });
    });
    value.forEach((item) => {
      newArrayFooter.push(item.word);
    });
    value.forEach((item) => {
      newArrayFooterUi.push({ word: item.word });
    });
    dispatch(setArrayNewWorldFooter(newArrayFooter));
    dispatch(setNewArrayFooterUi(newArrayFooterUi));
    dispatch(setWordsMain(word));
    let correct = [];
    correct.push(word.map((item) => item.word).join(" "));

    dispatch(setCorrectAnswerNew(correct));
  };

  const handleFilterWord = (words, language) => {
    setFilterWord(words.filter((word) => word.language === language));
    setLang(language);
    dispatch(setLanguageType(language));
  };

  useEffect(() => {
    handleFilterWord(props.Words, "fa");
  }, [props.Words]);

  const arrayTest = props?.answerinapi?.words?.map((item) => item.word);
  var allanswerFromApi = props?.answerinapi?.words?.map((item) => item);
  return (
    <>
      <div className="container wordbank_taginput">
        <div className="row">
          <div class="d-flex w-100 position-relative">
            <Autocomplete
              onChange={(e, v) => onChangeHandler(e, v)}
              className={`rtl wordbank_taginput_${lang} w-100`}
              multiple
              defaultValue={allanswerFromApi?.map((word, index) => word)}
              filterSelectedOptions
              id="tags-standard"
              options={filterWord && filterWord.map((option) => option)}
              getOptionLabel={(option) => option.word}
              // getOptionDisabled={(options) => (chosenWord.length > 3 ? true : false)}
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip
                    variant="outlined"
                    label={!allanswerFromApi ? option.word : option.word}
                    {...getTagProps({ index })}
                  />
                ))
              }
              renderInput={(params) => (
                <TextField
                  className="placeholder_style"
                  {...params}
                  variant="filled"
                  placeholder={lang === "fa" ? "انتخاب کلمه " : "Chose word"}
                  autoComplete="off"
                />
              )}
            />
            <div className={`d-flex justify-content-center align-items-center`}>
              <button
                className={`btn ${lang === "fa" ? "text_bold" : ""}`}
                onClick={() => handleFilterWord(props.Words, "fa")}
              >
                Fa
              </button>
              <span>|</span>
              <button
                onClick={() => handleFilterWord(props.Words, "en")}
                className={`btn ${lang === "en" ? "text_bold" : ""}`}
              >
                En
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
