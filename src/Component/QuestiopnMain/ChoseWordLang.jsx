import React, { useState } from "react";
import Addword from "../AddWord/Addword";
import Modal from "react-modal";

function ChoseWordLang(props) {
  const [changeSelectOption, setSelectOption] = useState();
  const [flashCartModal, setFlashCartModal] = useState(false);
  const [wordList, setWordList] = useState([]);

  const onChangeSelect = (e) => {
    setSelectOption(e.target.value);
  };
  function openflashCartModal() {
    setFlashCartModal(true);
  }
  function closeflashCartModal() {
    setFlashCartModal(false);
  }
  const flashCartStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: "auto",
      width: "70%",
      height: "340px",
      zIndex: "10",
      borderRadius: "30px",
    },
  };

  return (
    <div className="mr-md-2 chose-main">
      <Addword setWordList={props.setWordList} />
    </div>
  );
}

export default ChoseWordLang;
