import React, { useState, useEffect } from "react";
import LoadingMain from "../../Loading/Loading";
import TextField from "@material-ui/core/TextField";
import PrePicture from "../Wordbank/PrePicture";
import axios from "axios";
import Correct from "../Wordbank/CorrectAnswer";
import Aotucomplte from "../Aotucomplte";
import ChoseWordLang from "../ChoseWordLang";
import AutocompleteWordbank from "../Speaking/AutocompleteSpeaking";
import { useDispatch, useSelector } from "react-redux";

import Question from "../Wordbank/AutocompleteWordbank";
import {
  setCorrectAnswer,
  setTitle,
  setAllAnswer,
  setCorrectAnswerNew,
} from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";

import Checkbox from "@material-ui/core/Checkbox";
import { useHistory } from "react-router";
const TypeMain = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [onChangeHeader, setonChangeHeader] = useState(
    props.item ? props.item.title : ""
  );
  const [loadingModal, setLoadingModal] = useState(true);
  const [blankFromApi, setBlankFromApi] = useState();
  const [picture, setPicture] = useState();
  const [prePicture, setPrePicture] = useState();
  const [pic_id, setPic_id] = useState();
  const [questionWord, setQuestionWord] = useState();
  const [correctAnswer, setCorrectAnswerDefault] = useState([]);
  const [openbox, setOpenbox] = useState(false);
  const [checkbox, setCheckbox] = useState(
    props?.item?.answer[0]?.word?.length > 0 ? true : false
  );
  const [wordList, setWordList] = useState([]);
  const handleChangeHeader = (e, value) => {
    setonChangeHeader(e.target.value);
    dispatch(setTitle(e.target.value));
  };

  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  useEffect(() => {
    dispatch(setCorrectAnswerNew(correctAnswer));
  }, [correctAnswer, dispatch]);

  useEffect(() => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setBlankFromApi(res.data.data);
        setWordList(res.data.data);
        setLoadingModal(false);
      })
      .catch((error) =>
        error.response.data.status === 401 ? history.push("/login") : null
      );
  }, []);

  const handleChange = (event) => {
    setCheckbox(event.target.checked);
  };

  return (
    <>
      {loadingModal ? <LoadingMain /> : null}

      <div className="   blank-main type-blank-type col-12">
        <div className="col-12 text-right header-type">
          <TextField
            variant="filled"
            id="standard-basic "
            label={<p>عنوان سوال</p>}
            onChange={handleChangeHeader}
            autoComplete="off"
            value={onChangeHeader}
          />
        </div>
        <div className="Wordbank type-main py-2">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12  d-block d-lg-flex align-items-center ">
                <div className="d-flex align-items-center justify-content-center mt-3">
                  <PrePicture
                    Picturehandler={Picturehandler}
                    prePicture={prePicture}
                    picture={picture}
                    setPic_id={setPic_id}
                    answerinapi={props.item}
                  />
                </div>
                <div>
                  <div className="d-none d-lg-flex justify-content-end mb-2 mr-4">
                    <ChoseWordLang />
                  </div>
                  {blankFromApi ? (
                    <>
                      <div className="d-flex justify-content-center align-items-center ">
                        <div className="mosalas d-none d-lg-block"></div>
                        <div className="title_word">
                          <AutocompleteWordbank
                            setQuestionWord={setQuestionWord}
                            Words={blankFromApi}
                            type="blank-type"
                            answerinapi={props.item}
                          />
                        </div>
                      </div>
                    </>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row py-4">
          <div className="col-12 correct_awnser">
            <Correct
              setCorrectAnswer={setCorrectAnswerDefault}
              answerinapi={props.item}
              type="typeing"
            />
          </div>
        </div>
        <div
          className="col-12 d-flex align-items-center mt-5 p-0 p-md-2 pointer"
          onClick={() => {
            setOpenbox(!checkbox);
            setCheckbox(!checkbox);
          }}
        >
          <Checkbox
            checked={checkbox}
            onChange={handleChange}
            color="primary"
          />
          <span className="text_bold">استفاده از بانک کلمات برای پاسخ دهی</span>
        </div>

        <div className={`col-12 mt-4 ${checkbox ? "openbox" : "closebox"}`}>
          <div className="d-flex justify-content-center justify-content-lg-start align-items-center text-center">
            <span className="text_bold">
              {" کلمـات مورد نظر را جهت نمـایش در بانک کلمـات انتخـاب کنید. "}
            </span>
          </div>
          <div className="mr-auto w-100 w-lg-50 mt-5">
            <Question
              setQuestionWord={setQuestionWord}
              Words={wordList}
              type="listening"
              mirorlang={true}
              answerinapi={props.item}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default TypeMain;
