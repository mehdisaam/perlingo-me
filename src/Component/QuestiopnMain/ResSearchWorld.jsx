import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";
import {
  setArrayNewBlankType,
  setWordsMain,
  setArrayNewWorldFooter,
  setLoadingLang,
} from "../../redux/action/user";
import LoadingMain from "../Loading/Loading";
// setArrayNewBlankType
const ResSearchWorld = (props) => {
  const dispatch = useDispatch();
  const [lang, setLang] = useState("fa");
  const [chosenWord, setChosenWord] = useState(props.words);
  const [loadingModal, setLoadingModal] = useState(false);
  const [filterWord, setFilterWord] = useState([]);
  const onChangeHandler = (e, value) => {
    const newArrayTest = [];
    value.forEach((item) => {
      newArrayTest.push({
        word_id: item._id,
        word: item.word,
      });
    });

    if (props.type === "blank-type" || props.type === "Multi") {
      dispatch(setWordsMain(newArrayTest));
      dispatch(setArrayNewWorldFooter(newArrayTest));
    } else {
      dispatch(setArrayNewBlankType(value));
      dispatch(setArrayNewWorldFooter(value));
    }
  };

  const handleFilterWord = (words, language) => {
    setFilterWord(
      words && words?.filter((word) => word?.language === language)
    );
    setLang(language);
  };
  // useEffect(() => {
  //   if (filterWord !== undefined) {
  //     handleFilterWord(props.Words && props.Words, "fa");
  //   }
  // }, []);

  return (
    <>
      {loadingModal ? <LoadingMain /> : null}
      <div className="container p-0 reuseble-add-world">
        <div
          className={`d-flex justify-content-center align-items-center languages_${lang}`}
          style={{ position: "absolute", left: "0", zIndex: "2" }}
        >
          <button
            className={`btn ${lang === "fa" ? "text_bold" : ""}`}
            onClick={() => {
              handleFilterWord(props.Words, "fa");
              dispatch(setLoadingLang(lang));
            }}
          >
            Fa
          </button>
          <span>|</span>
          <button
            onClick={() => {
              handleFilterWord(props.Words, "en");
              dispatch(setLoadingLang(lang));
            }}
            className={`btn ${lang === "en" ? "text_bold" : ""}`}
          >
            En
          </button>
        </div>
        <Autocomplete
          onChange={(e, v) => onChangeHandler(e, v)}
          multiple
          id="tags-standard"
          options={filterWord && filterWord}
          getOptionLabel={(option) => option?.word}
          className={`rtl flashcart_taginput_${lang} w-100`}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              placeholder="سوال خود را انتخاب کنید"
              autoComplete="off"
            />
          )}
        />
      </div>
    </>
  );
};

export default ResSearchWorld;
