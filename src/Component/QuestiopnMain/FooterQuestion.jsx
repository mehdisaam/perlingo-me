import React, { useState, useEffect } from "react";
import Aotucomplte from "./Aotucomplte";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import axios from "axios";
import classnames from "classnames";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import {setWORDLIST} from "../../redux/action/user"
import { ButtonGroup, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { setTypeFooter, setBooleanViceFooter } from "../../redux/action/user";
import { useHistory } from "react-router";
const FooterQuestion = (props) => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const history = useHistory();
  const [rightWorldGetFromApi, setRightWorldGetFromApi] = useState();
  const [indirectlyWorldGetFromApi, setIndirectlyWorldGetFromApi] = useState();
  const [value, setValue] = React.useState("other");
  const [chooseLang, setChooseLang] = useState("fa");
  const [changeVice, setVioce] = useState();
  const handleChange = (event) => {
    setValue(event.target.value);
    dispatch(setTypeFooter(event.target.value));
  };

  const changeVoice = (e) => {
    setVioce(e.target.checked);
    dispatch(setBooleanViceFooter(e.target.checked));
  };

  const handleChooseLang = (e) => {
    setChooseLang(e);
  };
  // if (chooseLang === "fa") {
  //   props?.takeTypePage("fa");
  // } else {
  //   props?.takeTypePage("en");
  // }

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setRightWorldGetFromApi(res.data.data);
        setIndirectlyWorldGetFromApi(res.data.data);
        dispatch(setWORDLIST(res.data.data))
      })
      .catch((err) => {
        return err.response.data.status === 401 ? history.push("/login") : null;
      });
  };
  useEffect(() => {
    getWordList();
  }, []);

  return (
    <div className="footer-qiuestion-main">
      <div className="header-question">
        <h3>تنظیمات پیشرفته</h3>
      </div>
      <div className=" autocomplete-footer  mb-3">
        {/* {user.conditionalType === "F" ||
        user.conditionalType === "L" ||
        user.conditionalType === "M" ? ( */}
        <div
          className={classnames(
            user.conditionalType === "F" ||
              user.conditionalType === "L" ||
              user.conditionalType === "M"
              ? "disabled-style col-12 col-md-3 complete-text p-0 mb-2 mb-md-auto"
              : " col-12 col-md-3 complete-text p-0 mb-2 mb-md-auto"
          )}
        >
          <Aotucomplte label={<p>کلمه جدید</p>} type="new-Array" />
        </div>

        <div className=" col-12 col-md-3 complete-text  p-0 mr-md-4 mb-2 mb-md-auto">
          <Aotucomplte
            rightWorldGetFromApi={user.WordList}
            type="reviewed_words"
            label={<p>کلمه مستقیم</p>}
          />
        </div>
        <div className=" col-12 col-md-3  complete-text p-0  mr-md-4 mb-2 mb-md-auto">
          <Aotucomplte
            indirectlyWorldGetFromApi={user.WordList}
            type="reviewed_indirectly"
            label={<p>کلمه غیر مستقیم</p>}
          />
        </div>
      </div>

      <div className=" checked-footer">
        <Checkbox
          color="primary"
          inputProps={{ "aria-label": "secondary checkbox" }}
          onChange={changeVoice}
        />

        <label className="m-0"> صدا دارد ؟ </label>
      </div>

      <div className="radio-box">
        <FormControl
          component="fieldset"
          className="col-12  w-100 float-right text-right"
        >
          <RadioGroup
            aria-label="gender"
            name="gender1"
            value={value}
            onChange={handleChange}
            className="d-flex align-items-center"
          >
            <p> گوینده :</p>
            <FormControlLabel value="man" control={<Radio />} label="مرد" />
            <FormControlLabel value="female" control={<Radio />} label="زن" />
            <FormControlLabel
              value="other"
              control={<Radio />}
              label="تصادفی"
            />
          </RadioGroup>
        </FormControl>
      </div>
    </div>
  );
};

export default FooterQuestion;
