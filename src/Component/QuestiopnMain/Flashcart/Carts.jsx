import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import { useDispatch } from "react-redux";
import {
  setCorrectAnswerNew,
  setWordInAnswer,
} from "../../../redux/action/user";
//Url
import { BaseUrl } from "../../../Constant/Config";
// Images
import No_Image from "../../../assets/img/No_Image.png";

// Icons
import DoneIcon from "@material-ui/icons/Done";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import { Checkbox } from "@material-ui/core";
import ControlledOpenSelect from "../../AddWord/SelectLanguage";







export default function Carts(props) {
  //State
  const dispatch = useDispatch();

  let test = []
  test = props?.imginApiQuestion?.filter(item => { return item.word === props?.item?.answer[0]?.correct_answer[0] })

  const [checkBox, setCheckBox] = useState(test?.length > 0 ? String(test[0]?.word) : props?.selectedWord[0]?.word);
  const [deleteBox, setDeleteBox] = useState("false");
  // const [deleteWord, setDeleteWord] = useState('false')

  const deleteWord = (index) => {
    let array = props.selectedWord;
    let indexOfCorrectAnswer = array.findIndex((el) => el.word === checkBox);
    if (index === indexOfCorrectAnswer) {
      setCheckBox(null);
    }
    array.splice(index, 1);
    props.setSelectedWord(array);
    dispatch(setWordInAnswer(array));
  };

  useEffect(() => {
    test?.length > 0 ? String(test[0]?.word) : setCheckBox(props?.selectedWord[0]?.word);
    props.selectedWord.length > 0 && dispatch(setCorrectAnswerNew([checkBox]));
  }, [props.selectedWord]);

  useEffect(() => {
    dispatch(setCorrectAnswerNew([checkBox]));
  }, [checkBox]);

  var indexWord;
  var currentWord;


  return (
    <>
      {props.selectedWord.map((item, index) => {
        indexWord =
          props.Words === []
            ? item.word_id.findIndex((word) => word.word === item.word)
            : props.Words.findIndex((word) => word.word === item.word);

        currentWord =
          props.Words.length === 0 ? item.word_id : props.Words[indexWord];
        console.log(item, "1345 item")
        return (
          <>
            <div className="col-12 col-lg-6 col-xl-3 my-5">
              <div
                className="position-relative pointer"
                onMouseEnter={() => setDeleteBox(item.word)}
                onMouseLeave={() => setDeleteBox("")}
              >
                <div className="d-flex justify-content-center justify-content-md-center">
                  <div className="flashcart_box mx-4">
                    <div
                      className="delete_circle"
                      onClick={() => {
                        deleteWord(index);
                        setDeleteBox("");
                      }}
                    >
                      <CloseIcon fontsize="large" className="text-white" />
                    </div>
                    {deleteBox === item?.word && (
                      <div
                        className="delete_circle"
                        onClick={() => {
                          deleteWord(index);
                          setDeleteBox("");
                        }}
                      >
                        <CloseIcon fontsize="large" className="text-white" />
                      </div>
                    )}
                    {currentWord?.picture === undefined ||
                      currentWord?.picture === "" ? (
                      <img
                        className="flashcart_img"
                        src={No_Image}
                        alt="flashcart_image"
                      />
                    ) : (
                      <img
                        className="flashcart_img"
                        src={`${BaseUrl}${currentWord?.picture}`}
                        alt="flashcart_image"
                      />
                    )}
                  </div>
                </div>
                <div
                  className="d-flex justify-content-center align-items-center text_gray p-3 rtl"
                  onClick={() =>
                    setCheckBox(
                      item.word,
                      dispatch(setCorrectAnswerNew([item.word]))
                    )
                  }
                >
                  <div className="checkbox">
                    {checkBox === item.word && (
                      <DoneIcon fontsize="large" className="text-success" />
                    )}
                  </div>
                  <span className="mr-2">پاسخ صحیح: {item.word}</span>
                </div>
              </div>
            </div>
          </>
        );
      })}
      {props.selectedWord.length < 4 && (
        <div className="col-12 col-lg-6 col-xl-3 d-flex justify-content-center mt-5 my-4">
          <div
            className="add_flashcart mx-4"
            onClick={props.openflashCartModal}
          >
            <AddIcon className="text_gray" fontSize="large" />
          </div>
        </div>
      )}
      {/* {props.imginApiQuestion
        ? props.imginApiQuestion.map((item) => (
            <>
              <div className="flashcart_box mx-4">
                <img
                  className="flashcart_img"
                  src={`${BaseUrl}${item.word_id.picture}`}
                />
              </div>
            </>
          ))
        : null} */}
    </>
  );
}
