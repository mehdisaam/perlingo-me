import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import Carts from "./Carts";
import SelectWord from "./SelectWord";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import { useDispatch } from "react-redux";
import { setTitle } from "../../../redux/action/user";

// url
import { BaseUrl } from "../../../Constant/Config";

// Image
import loading from "../../../assets/img/loading.gif";
import { useHistory } from "react-router";
// Icons
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";

export default function Flashcart(props) {
  const history = useHistory();
  // State
  const dispatch = useDispatch();
  const [wordList, setWordList] = useState([]);
  const [flashCartModal, setFlashCartModal] = useState(false);
  const [loadingIsOn, setloadingIsOn] = useState(false);
  const [selectedWord, setSelectedWord] = useState(
    props.imginApiQuestion ? props.imginApiQuestion : []
  );
  const [titlef, setTitlef] = useState(
    props.titleApiQuestion ? props.titleApiQuestion : ""
  );

  //////////////////////// add flashcart Modal
  function openflashCartModal() {
    setFlashCartModal(true);
  }
  function closeflashCartModal() {
    setFlashCartModal(false);
  }
  const flashCartStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "70%",
      height: "340px",
      zIndex: "10",
      borderRadius: "30px",
    },
  };

  ///////////////////////// loading Modal
  // open loading function
  function openloading() {
    setloadingIsOn(true);
  }
  //close loading function
  function closeloading() {
    setloadingIsOn(false);
  }
  const loadingStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "5000",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: "400px",
      height: "155px",
      backgroundColor: "transparent",
      border: "none",
      zIndex: "5000",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };
  console.log(props.imginApiQuestion, "propspropsprops");
  // Api
  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  // function

  const handletitle = (e) => {
    dispatch(setTitle(e.target.value));
    setTitlef(e.target.value);
  };

  useEffect(() => {
    getWordList();
  }, []);

  return (
    <>
      <div className="container-fluid pt-3">
        <div className="row">
          <div className="col-12 title_of_questions">
            <TextField
              variant="filled"
              id="standard-basic "
              className="text_bold"
              label={<p>عنوان سوال</p>}
              onChange={handletitle}
              autoComplete="off"
              value={titlef}
            />
          </div>
          <div className="col-12">
            <div className="text-right">
              <span className="text_bold">
                فلـش کارت‌هـا را از بانک کلمات انتخاب کنید و پاسخ صحیح را علامت
                بزنید.
              </span>
            </div>
          </div>
          <div className="col-12 pt-4 ltr">
            <div className="row">
              <Carts
                openflashCartModal={openflashCartModal}
                setSelectedWord={setSelectedWord}
                selectedWord={selectedWord}
                Words={wordList}
                imginApiQuestion={props.imginApiQuestion}
                item={props.item}
              />
            </div>
          </div>
        </div>
      </div>
      {flashCartModal && (
        <Modal
          isOpen={flashCartModal}
          onRequestClose={closeflashCartModal}
          style={flashCartStyle}
          closeTimeoutMS={200}
        >
          <SelectWord
            closeflashCartModal={closeflashCartModal}
            setWordList={setWordList}
            Words={wordList}
            selectedWord={selectedWord}
            setSelectedWord={setSelectedWord}
            imginApiQuestion={props.imginApiQuestion}
          />
        </Modal>
      )}
      {loadingIsOn && (
        <Modal isOpen={loadingIsOn} style={loadingStyle}>
          <div class="d-flex justify-content-center">
            <img className="w-25 h-25" src={loading} alt="" />
          </div>
        </Modal>
      )}
    </>
  );
}
