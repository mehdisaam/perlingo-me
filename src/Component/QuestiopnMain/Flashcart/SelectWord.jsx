import React, { useState, useEffect } from "react";
import Addword from "../../AddWord/Addword";
// Icons
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";

// material-ui
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { SignalCellularNoSimOutlined } from "@material-ui/icons";
import { useDispatch } from "react-redux";
import {
  setLanguageType,
  setFlashcardAnswer,
  setAllAnswer,
  setWordInAnswer,
} from "../../../redux/action/user";
export default function SelectWord(props) {
  // State
  const dispatch = useDispatch();
  const [filterWord, setFilterWord] = useState([]);
  const [chosenWord, setChosenWord] = useState([]);
  const [lang, setLang] = useState("fa");

  // Functions

  const onChangeHandler = (e, value) => {
    setChosenWord(value);
  };
  // Functions

  const SaveSelectedWords = () => {
    props.setSelectedWord(chosenWord);

    let answer = [];
    chosenWord.forEach((item) =>
      answer.push({ word: item.word, word_id: item._id })
    );
    dispatch(setWordInAnswer(answer));
    props.closeflashCartModal();
  };

  const handleAddflashcart = (e) => {
    if (e.keyCode === 13) {
      SaveSelectedWords();
    }
  };

  useEffect(() => {
    handleFilterWord(props.Words, "fa");
    setChosenWord(props.selectedWord);
    dispatch(setLanguageType("fa"));
  }, [props.Words]);

  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    dispatch(setLanguageType(language));
    setLang(language);
  };

  const handleFilterWordwithdefaultvalue = async (words, language) => {
    console.log("Fl dare run mishe");
    await setFilterWord(words?.filter((word) => word.language === language));

    let arrayAsli = filterWord;
    let arrayfilter = [];

    filterWord.forEach((el, index1379) => {
      let indexes = null;
      props.selectedWord.forEach((item) => {
        if (item.word === el.word) {
          indexes = index1379;
        } else {
          indexes = null;
        }
      });

      if (indexes == !null) {
        arrayAsli = arrayAsli.splice(indexes, 1);
        setFilterWord(arrayAsli);
      }
    });
    console.log(arrayAsli, "fl array");

    dispatch(setLanguageType(language));
    setLang(language);
  };

  useEffect(() => {
    props.selectedWord.length === 0
      ? handleFilterWord(props.Words, "en")
      : handleFilterWordwithdefaultvalue(props.Words, "en");
    setChosenWord(props.selectedWord);
    dispatch(setLanguageType("en"));
  }, [props.Words]);

  console.log(filterWord, "props.imginApiQuestion");
  return (
    <>
      <div
        className="outline-none h-fill "
        // onKeyDown={(e) => handleAddflashcart(e)}
        // tabIndex="0"
      >
        <div className="row d-flex justify-content-between align-items center px-4">
          <div>
            <CloseIcon
              fontSize="large"
              className="pointer"
              onClick={props.closeflashCartModal}
            />
            <span className="mr-2 text_bold fs_xs_14px">انتخاب کلمه</span>
          </div>
          <div className="d-flex align-items-center">
            <div className="pointer ml-4">
              <Addword setWordList={props.setWordList} />
            </div>
            <div onClick={SaveSelectedWords} className="pointer">
              <SaveOutlinedIcon fontSize="large" />
              <span className="text_bold d-none d-md-inline">ذخیره</span>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <span className="mr-4 mt-5 text_bold">
              در لیست کلمات جستجو کنید.
            </span>
            <div class="d-flex w-100 mt-3 mt-lg-5 style-changelang-flash">
              <div
                className={`d-flex justify-content-center align-items-center languages_${lang}`}
              >
                <button
                  className={`btn ${lang === "fa" ? "text_bold" : ""}`}
                  onClick={() => handleFilterWord(props.Words, "fa")}
                >
                  Fa
                </button>
                <span>|</span>
                <button
                  onClick={() => handleFilterWord(props.Words, "en")}
                  className={`btn ${lang === "en" ? "text_bold" : ""}`}
                >
                  En
                </button>
              </div>
              <Autocomplete
                defaultValue={
                  props.selectedWord &&
                  props.selectedWord.map((option) => option)
                }
                autoComplete
                onChange={(e, v) => onChangeHandler(e, v)}
                className={` flashcart_taginput_${lang} w-100`}
                multiple
                filterSelectedOptions
                id="tags-standard"
                options={filterWord && filterWord.map((option) => option)}
                getOptionLabel={(option) => option.word}
                getOptionDisabled={(options) =>
                  chosenWord?.length > 3 ? true : false
                }
                renderTags={(value, getTagProps) =>
                  value.map((option, index) => (
                    <Chip
                      variant="outlined"
                      label={option.word}
                      {...getTagProps({ index })}
                    />
                  ))
                }
                renderInput={(params) => (
                  <TextField
                    className="placeholder_style"
                    {...params}
                    variant="filled"
                    placeholder={
                      chosenWord?.length >= 4
                        ? " "
                        : lang === "fa"
                        ? "انتخاب کلمه فارسی"
                        : "Chose word"
                    }
                    autoComplete="off"
                  />
                )}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
