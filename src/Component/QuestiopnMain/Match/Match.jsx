import React, { useState, useEffect } from "react";
import AddIcon from "@material-ui/icons/Add";
import axios from "axios";
import { BaseUrl } from "../../../Constant/Config";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { useDispatch } from "react-redux";
import {
  setWordsMain,
  setCorrectAnswerNew,
  setWordInAnswer,
  setTitle,
} from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

export default function Match(props) {
  const history = useHistory();
  // State
  const dispatch = useDispatch();
  const [wordList, setWordList] = useState([]);
  const [filterWordQuestion, setFilterWordQuestion] = useState([]);
  const [filterWordAnswer, setFilterWordAnswer] = useState([]);
  const [questions, setQuestions] = useState([]); //Save Words for post to server
  const [questionsWord, setQuestionsWord] = useState([]); // save Words for PreView
  const [answers, setAnswers] = useState([]);
  const [InputisOpen, setInputIsOpen] = useState(false);
  const [langAnswer, setLangAnswer] = useState("fa"); // true = en false= fa
  const [langQuestion, setLangQuestion] = useState("en");

  // const handleFilterWord = (words, language) => {
  //     setFilterWord(words.filter((word) => word.language === language));
  //     setLang(language);
  // };

  const AnswerHandler = (e, value) => {
    setAnswers(value);
    let answer = [];
    value.map((item, index) =>
      answer.push({ word: item.word, word_id: item._id, priority: index + 1 })
    );
    // setQuestionsWord(setQuestions.word)
    // props.setQuestionWord(value)
    dispatch(setWordInAnswer(answer));
  };
  const QuestionsHandler = (e, value) => {
    setQuestions(value);
    let question = [];
    value.map((item, index) =>
      question.push({ word: item.word, word_id: item._id, priority: index + 1 })
    );
    dispatch(setWordsMain(question));
    // setQuestionsWord(setQuestions.word)
    // props.setQuestionWord(value)
  };

  const handletitle = (e) => {
    dispatch(setTitle(e.target.value));
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
        setFilterWordQuestion(
          res.data.data.filter((word) => word.language === langQuestion)
        );
        setFilterWordAnswer(
          res.data.data.filter((word) => word.language === langAnswer)
        );
      })
      .catch((error) => {
        return error?.response?.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  const changeWords = (A, Q) => {
    setLangAnswer(A);
    setLangQuestion(Q);
    handleFilterWord(A, Q);
  };

  const handleFilterWord = (A, Q) => {
    setFilterWordQuestion(wordList.filter((word) => word.language === Q));
    setFilterWordAnswer(wordList.filter((word) => word.language === A));
  };

  useEffect(() => {
    getWordList();
  }, []);
  const arrayCreateEditMatch = [];
  const questionArrayEdite = [];
  var changeWord = props?.item?.answer[0]?.word;
  var questionEdite = props.item ? props.item.words : [];

  // arrayCreateEditMatch.push({
  //   word_id: item.word_id._id,
  //   word: item.word_id.word,
  //   priority: item.priority,
  // })
  if (questionEdite?.length !== 0) {
    questionEdite.forEach((word) => {
      questionArrayEdite.push({
        word_id: word.word_id._id,
        word: word.word,
        priority: word.priority,
      });
    });
  }
  if (changeWord?.length !== 0) {
    changeWord?.forEach((item) =>
      arrayCreateEditMatch.push({
        word_id: item.word_id._id,
        word: item.word,
        priority: item.priority,
      })
    );
  }


  console.log(arrayCreateEditMatch, questionArrayEdite, "array")
  return (
    <div className="Match">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 title_of_questions">
            <TextField
              variant="filled"
              id="standard-basic "
              // value={title}
              className="text_bold"
              label={<p>عنوان سوال</p>}
              onChange={handletitle}
            />
          </div>
          <div className="col-12">
            <div className="text-right">
              <span className="text_bold">
                لیست کلمـات را به همراه جواب درست رو به روی هم قرار دهید.
              </span>
            </div>
          </div>
          <div className="col-12 col-lg-6"></div>
          <div className="col-12 col-lg-6 pt-4 ltr">
            <div className="row">
              <div className="col-6">
                <div className="text-center mb-3">
                  <span>جواب</span>
                  <div
                    className={`d-flex justify-content-center align-items-center`}
                  >
                    <button
                      className={`btn ${langAnswer === "fa" ? "text_bold" : ""
                        }`}
                      onClick={() => changeWords("fa", "en")}
                    >
                      Fa
                    </button>
                    <span>|</span>
                    <button
                      onClick={() => changeWords("en", "fa")}
                      className={`btn ${langAnswer === "en" ? "text_bold" : ""
                        }`}
                    >
                      En
                    </button>
                  </div>
                </div>
                <div className="d-flex justify-content-center">
                  <Autocomplete
                    multiple
                    autoComplete
                    className=""
                    id="tags-standard"
                    filterSelectedOptions
                    defaultValue={
                      arrayCreateEditMatch !== [] &&
                      arrayCreateEditMatch?.map((item) => item)
                    }
                    options={
                      filterWordAnswer &&
                      filterWordAnswer.map((option) => option)
                    }
                    onChange={(e, v) => AnswerHandler(e, v)}
                    getOptionLabel={(option) => option.word}
                    getOptionDisabled={(options) =>
                      answers.length > 3 ? true : false
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="standard"
                        className="input_pad"
                        placeholder={
                          langAnswer === "fa" ? "انتخاب جواب" : "Chose answer"
                        }
                        autoComplete="off"
                      />
                    )}
                  />
                </div>

                {/* {
                                    answer &&
                                    answer.map((item, index) => {
                                        <div key={index} className="d-flex justify-content-center align-items-cnenter">
                                            <div className="add_box_answer mx-5">

                                            </div>
                                        </div>
                                    })
                                } */}
                {/* {
                                    InputisOpen ?
                                        <div className="d-flex justify-content-center align-items-cnenter">
                                            <div className="add_box_answer mx-5">

                                            </div>
                                        </div>
                                        :
                                        <div onClick={() => setInputIsOpen(true)} className="d-flex justify-content-center align-items-cnenter">
                                            <div className="add_box_answer mx-5">
                                                < AddIcon className="text_gray" fontSize="large" />
                                            </div>
                                        </div>
                                } */}
              </div>
              <div className=" col-6 ">
                <div className="text-center mb-3">
                  <span>سوال</span>
                  <div
                    className={`d-flex justify-content-center align-items-center`}
                  >
                    <button
                      className={`btn ${langQuestion === "fa" ? "text_bold" : ""
                        }`}
                      onClick={() => changeWords("en", "fa")}
                    >
                      Fa
                    </button>
                    <span>|</span>
                    <button
                      onClick={() => changeWords("fa", "en")}
                      className={`btn ${langQuestion === "en" ? "text_bold" : ""
                        }`}
                    >
                      En
                    </button>
                  </div>
                </div>
                <div className="d-flex justify-content-center">
                  <Autocomplete
                    multiple
                    className=""
                    id="tags-standard"
                    filterSelectedOptions
                    defaultValue={
                      questionArrayEdite !== [] &&
                      questionArrayEdite?.map(
                        (item) => item
                      )
                    }
                    options={
                      filterWordQuestion &&
                      filterWordQuestion.map((option) => option)
                    }
                    onChange={(e, v) => QuestionsHandler(e, v)}
                    getOptionLabel={(option) => option.word}
                    getOptionDisabled={(options) =>
                      questions.length > 3 ? true : false
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="standard"
                        className="input_pad"
                        placeholder={
                          langQuestion === "fa"
                            ? "انتخاب سوال"
                            : "Chose question"
                        }
                      />
                    )}
                  />
                </div>
              </div>
              {/* <Carts openflashCartModal={openflashCartModal} setSelectedWord={setSelectedWord} selectedWord={selectedWord} Words={wordList} /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
