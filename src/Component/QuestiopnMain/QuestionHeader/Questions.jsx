import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import { BaseUrl } from "../../../Constant/Config";
import { useSelector, useDispatch } from "react-redux";
import No_Image from "../../../assets/img/No_Image.png";

import Loading from "../../Error Modal/Loading";
import Error from "../../Error Modal/ErrorModal";
import Modal from "react-modal";
import {
  setCheckpoint_id,
  setQuestion,
  setEmptyArray,
  setCorrectAnswerNew,
  setWordsMain,
  setWordInAnswer,
  setTitle,
  settypeMainQuestion,
  setCorrectAnswer,
  setVoiseBoolean,
} from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import EditeQuestion from "../EditQuestion";
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';




const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));
export default function Questions() {
  const [accardion, setAccardion] = useState(false);
  const classes = useStyles();
  const history = useHistory();
  // State
  const [data, setData] = useState([]);
  const state = useSelector((state) => state.user);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [question_id, setQuestion_id] = useState("");
  const [childBoolean, setChaildBoolean] = useState();
  const [edit_id, setEdit_id] = useState();
  const [showEdit, setShowEdit] = useState(false);
  // modal
  const [removeModalIsOpen, setRemoveModalIsOpen] = useState(false);

  // open Remove modal functions
  function openRemoveModal() {
    setRemoveModalIsOpen(true);
  }

  //close add modal function
  function closeRemoveModal() {
    setRemoveModalIsOpen(false);
  }

  const RemoveStyles = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      margin: "auto",
      width: "400px",
      height: "220px",
      zIndex: "10",
      borderRadius: "30px",
      // animationName: 'Modal_animation',
      // animationDuration: '.6s'
    },
  };
  var filterImgFlashcart;
  const getquestion = () => {
    setLoading(true);
    axios
      .post(
        `http://46.227.68.244:3050/api/v1/admin/lesson?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          checkpoint_id: sessionStorage.getItem("checkpoint"),
          skill_id: sessionStorage.getItem("skill"),
          level_id: sessionStorage.getItem("level"),
          lesson_id: sessionStorage.getItem("lesson"),
        }
      )
      .then((res) => {
        dispatch(setQuestion(res.data.data.questions.reverse()));

        setLoading(false);
      })
      .catch((error) => {
        return (
          error?.response,
          error?.response?.data?.data,
          setLoading(false),
          error?.response?.data?.status === 401
            ? history.push("/login")
            : history.push("/")
        );
      });
  };

  const DeleteQuestion = (Question_id) => {
    setLoading(true);
    axios
      .post(
        `${BaseUrl}/api/v1/admin/questionDelete?api_token=${localStorage.getItem(
          "token"
        )}`,
        {
          checkpoint_id: sessionStorage.getItem("checkpoint"),
          skill_id: sessionStorage.getItem("skill"),
          level_id: sessionStorage.getItem("level"),
          lesson_id: sessionStorage.getItem("lesson"),
          question_id: Question_id,
        }
      )
      .then((res) => {
        dispatch(setQuestion(res.data.data.questions.reverse()));
        setLoading(false);
        closeRemoveModal();
      })
      .catch((err) => {
        return (
          setError(err.response.data.data),
          setLoading(false),
          closeRemoveModal(),
          setErrorModal(true),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };

  useEffect(() => {
    getquestion();
    setData(state.Questions);
  }, []);

  // useEffect(() => {
  //   handleClickedit();
  // }, [edit_id])

  // const handleClickedit = () => {
  //   setAccardion(!accardion);
  //   if (document.querySelector(`#id_${edit_id}`) !== null) {
  //     var el = document.querySelector(`#id_${edit_id}`);
  //     if (accardion) {
  //       el.classList.add("EditRow");
  //     }
  //     // else {
  //     //   return el.classList.remove("EditRow");
  //     // }
  //   }
  // };

  useEffect(() => {
    dispatch(setVoiseBoolean(accardion));
  }, []);
  return (
    <>
      <div>
        {state.Questions &&
          state.Questions.map((item, index) => {
            console.log(item.answer[0].option, "itemitemitem");
            let Typename = "";
            switch (item.type) {
              case "F": {
                Typename = "Flashcard";
                break;
              }
              case "WB": {
                Typename = "Wordbank";
                break;
              }
              case "S": {
                Typename = "Speaking";
                break;
              }
              case "M": {
                Typename = "Match";
                break;
              }
              case "L": {
                Typename = "Listening";
                break;
              }
              case "T": {
                Typename = "Typing";
                break;
              }
              case "B": {
                Typename = "Blank";
                break;
              }
              case "BT": {
                Typename = "Blanktype";
                break;
              }
              case "SC": {
                Typename = "Single-Choise";
                break;
              }
            }
            const correct_answerFromApi = item.answer[0].correct_answer.map(
              (item) => item
            );
            var arraynew = [];

            var wordFromApiArray = [];
            var optionInApiData = [];

            const wordFromApi = item.answer[0].word.map((item) => item);
            const wordAnswerSpeaking = item.answer[0].word.map((item) => item);
            const wordAnswer = item.words.map((item) =>
              arraynew.push({ word_id: item?.word_id?._id, word: item?.word })
            );
            wordFromApi.map((item) =>
              wordFromApiArray.push({
                word: item?.word,
                word_id: item?.word_id?._id,
              })
            );
            item.answer[0].option.map((item) =>
              optionInApiData.push({ title: item.title, correct: item.correct })
            );

            return (
              <>
                <div key={index} className={`${showEdit && edit_id === item._id && "d-none"} question_box px-4`}>
                  <div className="row px-4">
                    <div className="w-100 d-flex justify-content-between align-items-center px-2">
                      <div className="d-flex align-items-center">
                        <span className="text_gray_q text_bold">
                          نوع سوال :
                        </span>
                        <span className="text_bold pt-1 mr-2">{Typename}</span>
                      </div>
                      <div
                        className="">
                        <CreateOutlinedIcon
                          color="action"
                          fontSize="large"
                          className="pointer"
                          onClick={() => {
                            item.type === "F" || item.type === "T" || item.type === "WB" ||item.type === "S" ||item.type === "M" ? setShowEdit(true):setShowEdit(false);
                            
                            setEdit_id(item._id);
                            // handleClickedit(item._id);
                            dispatch(
                              setCorrectAnswerNew(correct_answerFromApi)
                            );
                            dispatch(
                              setWordsMain(
                                item.type === "S"
                                  ? arraynew
                                  : item.type === "F"
                                    ? wordFromApi
                                    : wordFromApiArray
                              )
                            );

                            dispatch(setTitle(item.title));
                            dispatch(settypeMainQuestion(item.type));
                          }}
                        />
                        <DeleteOutlinedIcon
                          onClick={() => {
                            setQuestion_id(item._id);
                            setRemoveModalIsOpen(true);
                          }}
                          color="action"
                          fontSize="large"
                          className="pointer"
                        />
                      </div>
                    </div>
                  </div>
                  <hr className="hr" />
                  <div className="row d-flex align-items-center pb-4">
                    <div className="col-12 col-md-2 my-2">
                      <div className="d-flex justify-content-center align-items-center">
                        <div className="image_box">
                          {item.type === "F" ? (
                            ((filterImgFlashcart = item?.answer[0]?.word.filter(
                              (word) =>
                                word?.word === item?.answer[0].correct_answer[0]
                            )),
                              (filterImgFlashcart, "filterImgFlashcart"),
                              filterImgFlashcart.length > 0 &&
                                filterImgFlashcart[0].word_id?.picture ? (
                                <img
                                  className="image_style"
                                  src={`${BaseUrl}${filterImgFlashcart[0].word_id?.picture}`}
                                  alt=""
                                />
                              ) : (
                                <img
                                  className="image_style"
                                  src={No_Image}
                                  alt="No_Image"
                                />
                              ))
                          ) : item.picture ? (
                            <img
                              className="image_style"
                              src={`${BaseUrl}${item.picture.picture}`}
                              alt=""
                            />
                          ) : (
                            <img
                              className="image_style"
                              src={No_Image}
                              alt="No_Image"
                            />
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="col-12 col-md-5 my-2">
                      <div className="d-sm-flex justify-content-center align-items-center textStyle">
                        <span className="text_gray_q text_bold d-block">
                          عنوان سوال :
                        </span>
                        <span className="text_bold mr-2">{item.title}</span>
                      </div>
                    </div>
                    <div className="col-12 col-md-5 my-2">
                      <div className="d-sm-flex justify-content-center align-items-center textStyle">
                        <span className="text_gray_q text_bold d-block">
                          جواب درست :
                        </span>
                        <span className="text_bold mr-2">
                          {item.type === "M"
                            ? item.answer[0].word.map(
                              (item2, index) =>
                                " " +
                                item2.word +
                                (item.answer[0].word.length - 1 === index
                                  ? ""
                                  : " ،")
                            )
                            : item.answer[0].correct_answer.map(
                              (item2, index) =>
                                " " +
                                item2 +
                                (item.answer[0].correct_answer.length - 1 ===
                                  index
                                  ? ""
                                  : " ،")
                            )}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                {
                  showEdit && edit_id === item._id &&
                  <>
                    <div className="col-12 p-0 ">
                      <EditeQuestion
                        item={item}
                        type={item.type}
                        title={item.title}
                        imginApiQuestion={item.answer[0].word}
                        id={item._id}
                        boolean={accardion}
                        setShowEdit={setShowEdit}
                      />
                    </div>
                  </>
                }
              </>
            );
          })}
      </div>
      { loading && <Loading isOpen={loading} />}
      {
        errorModal && (
          <Error isOpen={errorModal} error={error} close={setErrorModal} />
        )
      }

      {
        removeModalIsOpen && (
          <Modal
            isOpen={removeModalIsOpen}
            onRequestClose={closeRemoveModal}
            style={RemoveStyles}
            closeTimeoutMS={200}
          >
            <div className="row d-flex align-items-center center px-4">
              <div>
                <CloseIcon
                  fontSize="large"
                  className="pointer"
                  onClick={closeRemoveModal}
                />
                <span className="mr-2 text_bold">حذف سوال</span>
              </div>
            </div>
            <div className="row justify-content-center align-items-center text_remove_Modal mt-4">
              آیا شما قصد حذف کردن این سوال را دارید؟
          </div>
            <div className="row d-flex justify-content-center align-items-center mt-5">
              <button
                onClick={() => {
                  return DeleteQuestion(question_id), closeRemoveModal();
                }}
                className="btn btn-outline-success px-5 mx-3"
              >
                بلــه
            </button>
              <button
                onClick={closeRemoveModal}
                className="btn btn-danger px-5 mx-3"
              >
                خیـر
            </button>
            </div>
          </Modal>
        )
      }
    </>
  );
}
