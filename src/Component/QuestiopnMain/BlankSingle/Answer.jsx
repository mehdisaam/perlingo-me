import React, { useState, useRef } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";
import {
  setValueState,
  updateArray,
  setCorrectAnswerNew,
  setCorrectAnswer,
  setFlashcardCorrectAnswer,
} from "../../../redux/action/user";
import CloseIcon from "@material-ui/icons/Close";
import classnames from "classnames";
import {
  fade,
  ThemeProvider,
  withStyles,
  makeStyles,
  createMuiTheme,
} from "@material-ui/core/styles";
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "green",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00ccff",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderWidth: "2px",
        borderRadius: "10px",
      },
      "&:hover fieldset": {
        borderColor: "#777777",
        borderRadius: "10px",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00ccff",
        borderRadius: "10px",
      },
    },
    "& MuiAutocomplete-optio": {
      direction: "ltr",
    },
  },
})(TextField);
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  margin: {
    margin: theme.spacing(1),
  },
}));
const Answer = (props) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  console.log("user", user);
  const [value, setValue] = useState("");
  const [checkedRadio, setCheckedRadio] = useState();
  const [correctAnswer, setCorrentAnswer] = useState([]);
  const [checkedState, setChankedIndex] = useState(props.index);
  const [chakedStateValue, setChakedValue] = useState(props.index);
  const [focus, setFocus] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const [valueRadio, setValueRadio] = useState();
  const [valueTextBox, setTextBox] = useState();
  const [arrayTest, setArrayTest] = useState(
    props.addAnswer === undefined ? 0 : props.addAnswer?.length + 1
  );
  // const [stateObj, setStateObj] = useState([]);
  // const [temp, setTemp] = useState([]);
  const { forwardedRef, ...refTest } = props;
  const handleChangeInput = () => {
    if (value) {
      return props.testClick();
    } else {
      return null;
    }
  };
  console.log(checked, "checked");
  console.log(valueTextBox, "gggggaassddd");
  // type Inptut text
  const handleChange = (event) => {
    setChecked(false);
    setTextBox(event.target.value);
    let inputs = document.querySelectorAll(
      ".test-class .MuiOutlinedInput-input"
    );
    let Radios = document.querySelectorAll(".main-change-radio input");

    for (let Radio of Radios) {
      Radio.checked = false;
    }
    setChankedIndex(props.index);

    const correctAnswerArray = [];

    for (let input of inputs) {
      if (user.nweCorrectAnswer.length > 0) {
        correctAnswerArray.push({
          title: input.value,
          correct: event.target.value === valueRadio ? true : false,
        });
      } else {
        correctAnswerArray.push({
          title: input.value,
          correct: false,
        });
      }
    }
    console.log(correctAnswerArray, "correctAnswerArray");
    var result = correctAnswerArray?.findIndex(
      (correct) => correct.correct === true
    );

    if (user.answers.length === 0) {
      dispatch(setCorrectAnswer(correctAnswerArray));
    } else {
      let x = user.correctAnswerArray.find((item) => {
        return item.id === props.callValue;
      });
      let radioch = checked;
      if (x) {
        x.value = event.target.value;

        dispatch(updateArray(user.correctAnswerArray));
      } else {
        dispatch(setCorrectAnswer(correctAnswerArray));
        dispatch(setCorrectAnswerNew([correctAnswerArray[result].title]));
      }
    }
    dispatch(setCorrectAnswer(correctAnswerArray));
  };

  // change radio
  const handleChangeRadio = (event) => {
    setValueRadio(event.target.value);
    console.log(event.target.value, "fdfdfdsfdsfdsfsdfsdfsdfsdf");
    let inputs = document.querySelectorAll(
      ".test-class .MuiOutlinedInput-input"
    );
    setChakedValue(props.index);
    const correctAnswerArray = [];

    for (let input of inputs) {
      correctAnswerArray.push({
        title: input.value,
        correct: event.target.value === input.value ? true : false,
      });
    }
    var result = correctAnswerArray?.findIndex(
      (correct) => correct.correct === true
    );

    console.log(result, "noTestArray");
    if (user.answers.length === 0) {
      dispatch(setCorrectAnswerNew([correctAnswerArray[result]?.title]));
      dispatch(setCorrectAnswer(correctAnswerArray));
    }

    let radioch = checked;
    obj = {
      title: event.target.value,
      correct: radioch,
    };
    if (user.answers.length === 0) {
      dispatch(setValueState(obj));
    } else {
      let x = user.answers.find((item) => {
        return item.id === props.callValue;
      });

      if (x) {
        x.value = event.target.value;
        x.correct = radioch;
        dispatch(updateArray(user.answers));
      } else {
        dispatch(setValueState(obj));
      }
    }
    for (let i = 0; i < user.answers.length; i++) {
      if (user.answers[i].id === props.callValue) {
        user.answers[i].correct = event.target.checked;
      } else {
        user.answers[i].correct = !event.target.checked;
      }
    }
    dispatch(updateArray(user.answers));
  };
  var obj;
  var tabindex;
  const handleKeyPers = (event, index) => {
    if (event.key === "Enter") {
      handleChangeInput();
      let radioch = checked;
      obj = {
        title: event.target.value,
        correct: radioch,
      };
      if (user.answers.length === 0) {
        dispatch(setValueState(obj));
      } else {
        let x = user.answers.find((item) => {
          return item.id === props.callValue;
        });

        if (x) {
          x.value = event.target.value;
          x.correct = radioch;
          dispatch(updateArray(user.answers));
        } else {
          dispatch(setValueState(obj));
        }
      }
      if (props.tabIndex === props.callValue) {
        tabindex = props.callValue + 1;
      }
    }

    // if (event.key === "Enter" && props?.callValue === props?.index) {
    //   // setTimeout(function(){  test.current.focus(); }, 1000);
    //   forwardedRef.focus();
    // }
  };

  return (
    <div className="col-12 p-0 answer-main mt-2">
      <button
        type="button"
        className="close-answer"
        onClick={() => props?.deleteIcon()}
      >
        <CloseIcon
          className={classnames(
            props?.addAnswer?.length < 2 ? "classTest mr-1" : null
          )}
        />
      </button>

      <CssTextField
        id={"outlined-basic" + arrayTest}
        label={<p>پاسخ سوال</p>}
        variant="outlined"
        onChange={(e) => handleChange(e)}
        onKeyDown={(e) => handleKeyPers(e)}
        className="test-class"
        value={valueTextBox}
      />
      <div className="form-check p-0">
        {/* <input
          className='form-check-input'
          type='radio'
          value={value}
          name='flexRadioDefault'
          defaultChecked={checked}
          onChange={handleChangeRadio}
          id='flexRadioDefault1'
        /> */}
        {/* <label class="radio">
          <span class="radio__input">
            <input type="radio" name="radio" />
            <span class="radio__control"></span>
          </span>
        </label>
        <span className="checkmark"></span> */}
        <label className="main-change-radio">
          <input
            type="radio"
            onChange={handleChangeRadio}
            // tabIndex={props.tabIndex}
            name="radio"
            value={valueTextBox}
          />
          <span className="checkmark"></span>
        </label>
      </div>
    </div>
  );
};

export default Answer;
