import React, { useState, useEffect, useRef } from "react";
import Aotucomplte from "../Aotucomplte";
import axios from "axios";

import TextField from "@material-ui/core/TextField";
import LoadingMain from "../../Loading/Loading";
import Answer from "./Answer";
import ChoseWordLang from "../ChoseWordLang";
import { ButtonGroup, Button } from "react-bootstrap";
import classnames from "classnames";
import iconError from "../../../assets/img/perlingo-buttonn.gif";
import { useDispatch, useSelector } from "react-redux";
import { setTitle, setValidation } from "../../../redux/action/user";
import SelectWord from "../BlankType/Aotucomplete";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

const BlanckSingleChoise = (props) => {
  const history = useHistory();
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [blankFromApi, setBlankFromApi] = useState();
  const [addAnswer, setAddAnswer] = useState([
    <Answer callValue={1} testClick={() => addAnswerHandler()} />,
  ]);
  const [selectedWord, setSelectedWord] = useState([]);
  const [callValue, setCallValue] = useState(1);
  const [loadingModal, setLoadingModal] = useState(false);
  const [onChangeHeader, setonChangeHeader] = useState();
  const [value, setValue] = React.useState("female");
  const [wordList, setWordList] = useState([]);
  const [chooseLang, setChooseLang] = useState();
  // const [langFa, setLangefa] = useState();
  // const [langEng, setLangEng] = useState();
  const [titlef, setTitlef] = useState("");
  // const handleChange = (event) => {
  //   setValue(event.target.value);
  // };

  let test;
  const addAnswerHandler = (value) => {
    setCallValue(callValue + 1);
    setAddAnswer([...addAnswer, test]);
  };

  const deleteaddAnswerHandler = (e) => {
    setAddAnswer(addAnswer.splice(e, 1));

    setAddAnswer([...addAnswer]);
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  // function

  const handletitle = (e) => {
    dispatch(setTitle(e.target.value));
    setTitlef(e.target.value);
  };

  useEffect(() => {
    getWordList();
  }, []);
  const handleChooseLang = (chooseLang) => {
    setChooseLang(chooseLang);
  };

  // useEffect(() => {
  //   if (chooseLang === "fa") {
  //     setBlankFromApi(langFa);
  //   } else {
  //     setBlankFromApi(langEng);
  //   }
  // }, [chooseLang, langFa, langEng]);

  const handleChangeHeader = (e, value) => {
    dispatch(setTitle(e.target.value));
    setonChangeHeader(e.target.value);
  };
  const errorModalBlank = user.correctAnswer.filter(
    (item) => item.correct === true
  );

  return (
    <>
      {loadingModal ? <LoadingMain /> : null}
      <div className="col-12  blank-main">
        <div className="col-12 text-right header-type">
          <TextField
            variant="filled"
            id="standard-basic "
            value={onChangeHeader}
            label={<p>عنوان سوال</p>}
            onChange={handleChangeHeader}
            autoComplete="off"
          />
          {user.validation ? <div className="col-12 "></div> : null}
        </div>
        <div className="d-block col-12 my-5 text-right  d-md-flex justify-content-between align-items-center p-0 ">
          <p className="m-0">
            کلمات مورد نظر رو به ترتیب برای عنوان سوال انتخاب کنید و جای خالی رو
            مشخص کنید
          </p>
          {/* <div className="lang-main-question  text-right">
            <ButtonGroup>
              <Button
                onClick={() => handleChooseLang("fa")}
                className={classnames(chooseLang === "fa" ? "active-lang" : "")}
              >
                {" "}
                Fa
              </Button>
              <Button
                onClick={() => handleChooseLang("en")}
                className={classnames(chooseLang === "en" ? "active-lang" : "")}
              >
                {" "}
                En
              </Button>
            </ButtonGroup>
          </div> */}
          <ChoseWordLang />
        </div>
        <div className="col-12 p-0 aotu-blank-content mb-5">
          <SelectWord
            setWordList={setSelectedWord}
            Words={wordList}
            selectedWord={selectedWord}
            setSelectedWord={setSelectedWord}
            type="blanck-choies"
          />
        </div>

        <div className="col-12  box-answer mt-3">
          <div className=" text-right col-6">
            <p>پاسخ های خود را وارد کنید و گزینه درست را انتخاب کنید </p>
          </div>
          <div className="add-div-answer col-md-6">
            {addAnswer.map((item, index) => {
              return (
                <Answer
                  addAnswer={addAnswer}
                  callValue={callValue}
                  testClick={() =>
                    addAnswer.length > 3 ? null : addAnswerHandler()
                  }
                  onSelected={`callValue 1`}
                  index={index + 1}
                  value={addAnswer}
                  deleteIcon={() =>
                    addAnswer.length < 2 ? null : deleteaddAnswerHandler(index)
                  }
                />
              );
            })}

            {addAnswer.length > 3 ? null : (
              <button
                className="add-class-style-button-add"
                type="botton"
                onClick={(item) => {
                  return addAnswer.length > 4
                    ? null
                    : addAnswerHandler(callValue);
                }}
              >
                <span>+</span> افزودن پاسخ جدید
              </button>
            )}
          </div>
        </div>
        {errorModalBlank.length === 0 ? (
          <div className="style-text" style={{ top: "100%" }}>
            <div className="style-gif float-left ">
              {" "}
              <img src={iconError} alt={iconError} />
            </div>
            <p> لطفا یکی از گزینه هارا انتخاب کنید</p>
          </div>
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default BlanckSingleChoise;
