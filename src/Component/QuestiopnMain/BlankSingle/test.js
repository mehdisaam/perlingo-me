import React, { useState } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";
import { setValueState, updateArray } from "../../../redux/action/user";
import CloseIcon from "@material-ui/icons/Close";
const Answer = (props) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  console.log("user", user);
  const [value, setValue] = useState("");
  const [checkedRadio, setCheckedRadio] = useState();
  const [focus, setFocus] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const [arrayTest, setArrayTest] = useState(
    props.addAnswer === undefined ? 0 : props.addAnswer?.length + 1
  );
  const _onFocus = () => {
    if (!focus) {
      setFocus(true);
    }
  };
  const handleChangeInput = () => {
    if (value) {
      return props.testClick();
    } else {
      return null;
    }
  };
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleChangeRadio = (event) => {
    setChecked(event.target.checked);
    for (let i = 0; i < user.answers.length; i++) {
      if (user.answers[i].id === props.callValue) {
        user.answers[i].correct = event.target.checked;
      } else {
        user.answers[i].correct = !event.target.checked;
      }
    }
    dispatch(updateArray(user.answers));
  };
  var obj;

  const handleKeyPers = (event) => {
    if (event.key === "Enter") {
      console.log(checkedRadio, "checkedRadio");
      handleChangeInput();
      let radioch = checked;
      obj = {
        id: props.callValue,
        value: event.target.value,
        correct: radioch,
      };

      if (user.answers.length === 0) {
        dispatch(setValueState(obj));
      } else {
        let x = user.answers.find((item) => {
          return item.id === props.callValue;
        });

        if (x) {
          x.value = event.target.value;
          x.correct = radioch;
          dispatch(updateArray(user.answers));
        } else {
          dispatch(setValueState(obj));
        }
      }
    }
    _onFocus();
  };
  return (
    <div className="col-12  answer-main mt-2">
      <button
        type="button"
        className="close-answer"
        onClick={() => props.deleteIcon()}
      >
        <CloseIcon />
      </button>
      <TextField
        id={"outlined-basic" + arrayTest}
        label={<p>پاسخ سوال</p>}
        variant="outlined"
        onChange={handleChange}
        onKeyDown={(e) => handleKeyPers(e)}
      />
      <div className="form-check">
        {/* <input
          className='form-check-input'
          type='radio'
          value={value}
          name='flexRadioDefault'
          defaultChecked={checked}
          onChange={handleChangeRadio}
          id='flexRadioDefault1'
        /> */}
        <label class="radio">
          <span class="radio__input">
            <input type="radio" name="radio" />
            <span class="radio__control"></span>
          </span>
        </label>
        <span className="checkmark"></span>
      </div>
    </div>
  );
};

export default Answer;

// const handleChange = (event) => {
//     setChecked(false);
//     setTextBox(event.target.value);
//     let inputs = document.querySelectorAll(
//       ".test-class .MuiOutlinedInput-input"
//     );

//     let Radios = document.querySelectorAll(".main-change-radio input");

//     for (let Radio of Radios) {
//       Radio.checked = false;
//     }
//     setChankedIndex(props.index);

//     const correctAnswerArray = [];

//     for (let input of inputs) {
//       if (user.nweCorrectAnswer.length > 0) {
//         correctAnswerArray.push({
//           title: input.value,
//           correct: event.target.value === valueRadio ? true : false,
//         });
//       } else {
//         correctAnswerArray.push({
//           title: input.value,
//           correct: false,
//         });
//       }
//     }
//     var result = correctAnswerArray?.findIndex(
//       (correct) => correct.correct === true
//     );

//     if (user.answers.length === 0) {
//       dispatch(setCorrectAnswer(correctAnswerArray));
//     } else {
//       let x = user.correctAnswerArray.find((item) => {
//         return item.id === props.callValue;
//       });
//       let radioch = checked;
//       if (x) {
//         x.value = event.target.value;

//         dispatch(updateArray(user.correctAnswerArray));
//       } else {
//         dispatch(setCorrectAnswer(correctAnswerArray));
//         dispatch(setCorrectAnswerNew([correctAnswerArray[result].title]));
//       }
//     }
//     dispatch(setCorrectAnswer(correctAnswerArray));
//   };
