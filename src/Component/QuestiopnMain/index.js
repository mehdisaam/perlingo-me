import React, { useState, useEffect, useRef } from "react";
import FooterQuestion from "./FooterQuestion";
import HeaderQuestion from "./HeaderQuestion";
import Header from "../Header/Header";
import BreadCrumb from "./BreadCrumb";
import Questions from "./QuestionHeader/Questions";
import axios from "axios";
import { BaseUrl } from "../../Constant/Config";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setWORDLIST } from "../../redux/action/user";
const QuestionMain = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const Aref = useRef();
  const executeScroll = () => scrollToRef(Aref);
  const scrollToRef = (ref) =>
    window.scrollTo({
      top: ref.current.offsetTop,
      behavior: "smooth",
    });

  const [viceFromFooter, setViceFromFooter] = useState();
  const location = useLocation();
  const [typeVoice, setTypeVoice] = useState();
  const [createQuestion, setCreateQuestion] = useState(false);
  const [scrollTop, setScrollTop] = useState(false);
  const takeTypePage = (e) => {
    setViceFromFooter(e);
  };
  const takeTypeVoioce = (e) => {
    setTypeVoice(e);
  };

  const scrolltop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
    setScrollTop(false);
  };

  const getWordList = () => {
    axios
      .get(
        `${BaseUrl}/api/v1/admin/word/list?api_token=${localStorage.getItem(
          "token"
        )}`
      )
      .then((res) => {
        dispatch(setWORDLIST(res.data.data));
      })
      .catch((err) => {
        // return err.response.data.status === 401 ? history.push("/login") : null;
      });
  };
  useEffect(() => {
    getWordList();
  }, []);

  useEffect(() => {
    createQuestion && executeScroll();
    scrollTop && scrolltop();
  }, [createQuestion, scrollTop]);
  // useEffect(() => {
  //   dispatch(setTypeForFotterArray(changeSelectOption));
  // }, []);
  return (
    <>
      <Header />
      <BreadCrumb
        firstParent={"مدیریت ایستگاه ها"}
        secondParent={`سوال درس ${sessionStorage.getItem(
          "periority"
        )} از مرحله ${sessionStorage.getItem("priority_level")}`}
        className="p-0"
      />

      <div className="question container pr-3 pt-md-0 pr-md-0 pl-md-0 pb-5">
        <div
          onClick={() => {
            setCreateQuestion(true);
            createQuestion && executeScroll();
          }}
          className="d-flex justify-content-start align-items-center pointer mt-5"
        >
          <span className="text_gray add-class-style ">+ افزودن سوال جدید</span>
        </div>
        <Questions />
      </div>
      {createQuestion && (
        <div id="idsec" className="main-question container" ref={Aref}>
          <HeaderQuestion
            setScrollTop={setScrollTop}
            viceFromFooter={viceFromFooter}
            setCreateQuestion={setCreateQuestion}
          />
          <FooterQuestion
            takeTypePage={takeTypePage}
            takeTypeVoioce={takeTypeVoioce}
          />
        </div>
      )}
    </>
  );
};

export default QuestionMain;
