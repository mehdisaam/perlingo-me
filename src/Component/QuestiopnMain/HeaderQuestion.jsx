import React, { useState, useEffect } from "react";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import SaveIcon from "@material-ui/icons/Save";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CloseIcon from "@material-ui/icons/Close";
import Flashcart from "./Flashcart/Flashcart";
import BlanckSingleChoise from "./BlankSingle/BlanckSingleChoise";
import axios from "axios";
import loadingGif from "../../assets/img/loading.gif";
import { AouthProviderContext } from "../../Context/context";
import { BaseUrl } from "../../api-config/base-url";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import {
  setCheckpoint_id,
  setQuestion,
  setTypeForFotterArray,
  setEmptyArray,
} from "../../redux/action/user";
import iconError from "../../assets/img/perlingo-buttonn.gif";
import BlankTypeMain from "./BlankType/BlankTypeMain";
import TypeMain from "./Typing/TypeMain";
import Wordbank from "./Wordbank/Wordbank";
import Multicoice from "./Multichoice/Multicoice";
import Listening from "./Listening/Listening";
import Speaking from "./Speaking/Speaking";
import Match from "./Match/Match";
import Ellipsis from "@bit/joshk.react-spinners-css.ellipsis";
import ErrorModal from "../Error Modal/ErrorModal";
import Loading from "../Error Modal/Loading";
import Modal from "react-modal";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
const HeaderQuestion = (props) => {
  const history = useHistory();
  const [loading1, setLoading1] = useState(false);
  const dispatch = useDispatch();
  const [calHeaderValue, setCalHeaderValue] = useState();
  const counter = useSelector((state) => state.user);
  const location = useLocation();
  const [changeSelectOption, setSelectOption] = useState("F");
  const [loadin, setLoading] = useState(false);
  const [statusOkState, setStatusOk] = useState(false);
  const [errorHandler, setErrorHandler] = useState(false);
  const [data, setData] = useState([]);
  const [errorHandlerModal, setErrorHandlerModal] = useState(false);
  const [Ch, setCh] = useState("");

  const [textError, setTextModal] = useState("");
  let v_type = ["man", "female"];

  const callbacktest = (e) => {
    setCalHeaderValue(e);
  };

  // const data_location = {
  //   checkpoint: location.state.checkpoint,
  //   skill: location.state.skilId,
  //   lesson: location.state.lesson_id,
  //   level: location.state.level_id,
  // };
  const closeStatusOkModal = () => {
    setStatusOk(false);
  };
  console.log(counter, "counter");
  // useEffect(() => {
  //   // dispatch(setCheckpoint_id(data_location));
  // }, []);
  var errorArraWordBlank = [];

  const postApiAllData = () => {
    setLoading1(true);
    var data = new FormData();
    data.append("checkpoint_id", location.state.checkpoint);
    data.append("lesson_id", location.state.lesson_id);
    data.append("level_id", location.state.level_id);
    data.append("skill_id", location.state.skilId);
    data.append("title", counter?.title);
    data.append("language", counter.typeLanguage);
    data.append("type", changeSelectOption);

    data.append("correct_answer", JSON.stringify(counter.nweCorrectAnswer));
    if (counter.picture !== "") {
      data.append("picture", counter.picture);
    }
    data.append("reviewed_words", JSON.stringify(counter.valueAutocompelte));
    data.append(
      "reviewed_indirectly",
      JSON.stringify(counter.valueRevieweWordsIndirectory)
    );

    data.append("audio", counter.booleanVoice);
    data.append(
      "voice_type",
      counter.typeWrld === "other"
        ? v_type[Math.floor(Math.random() * 2)]
        : counter.typeWrld
    );
    data.append(
      "answer",
      JSON?.stringify([
        { option: counter.correctAnswer, word: counter.wordAnswer },
      ])
    );

    data.append("voice", counter.voiceFileListening);
    data.append("words", JSON.stringify(counter.wordsMain));
    data.append("new_words", JSON.stringify(counter.arraynewworldfooter));

    axios
      .post(
        `${bseurl}/admin/checkpoint/question?api_token=${localStorage.getItem(
          "token"
        )}`,
        data
      )
      .then((res) => {
        return (
          res.status === 200 ? setLoading(false) : setLoading(true),
          res.status === 200 ? setStatusOk(true) : setStatusOk(false),
          dispatch(setQuestion(res.data.data.questions.reverse())),
          props.setCreateQuestion(false),
          props.setScrollTop(true),
          setLoading1(false)
        );
      })

      .catch((error) => {
        return (
          setErrorHandler(true),
          setTextModal(error.response.data),
          setLoading1(false),
          setLoading(false),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };
  // ? setErrorHandlerModal(false)
  // : setErrorHandlerModal(true);

  const onChangeSelect = (e) => {
    setSelectOption(e.target.value);
    dispatch(setEmptyArray(changeSelectOption));
  
  };

  useEffect(() => {
    dispatch(setEmptyArray(changeSelectOption));
    dispatch(setTypeForFotterArray(changeSelectOption));
  }, [dispatch, changeSelectOption]);

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  errorArraWordBlank = counter.wordAnswer.filter((item) => {
    return item.blank === true;
  });
  console.log(counter, "counter.correctAnswer");
  var type = <Flashcart />;
  switch (changeSelectOption) {
    case "F": {
      type = <Flashcart />;
      break;
    }
    case "L": {
      type = <Listening />;
      break;
    }
    // Blank Choise
    case "B": {
      type = <BlanckSingleChoise callbacktest={callbacktest} />;
      break;
    }
    case "WB": {
      type = <Wordbank />;
      break;
    }
    case "S": {
      type = <Speaking />;
      break;
    }
    case "M": {
      type = <Match />;
      break;
    }
    //  Typeing
    case "T": {
      type = <TypeMain />;
      break;
    }
    // Multi chose
    case "SC": {
      type = <Multicoice />;
      break;
    }
    // Blank type
    case "BT": {
      type = <BlankTypeMain />;
      break;
    }

    default:
      break;
  }

  return (
    <>
      {errorHandler ? (
        <ErrorModal
          isOpen={errorHandler}
          error={textError?.data?.map((item) => (
            <p>{item}</p>
          ))}
          close={setErrorHandler}
        />
      ) : null}
      <div className="header-question-main">
        <div className="type-question col-10 col-lg-5  d-flex flex-wrap">
          <p>نوع سوال</p>
          <select className="col-12 col-md-9 select" onChange={onChangeSelect}>
            <option value="F">Flashd Card</option>
            <option value="L">Listening</option>
            <option value="WB">Word bank</option>
            <option value="S">Speaking</option>
            <option value="M">Match</option>
            <option value="T">Typing</option>
            <option value="BT">Blank_type</option>
            <option value="SC">Multi_choise</option>
            <option value="B">Blank Choise</option>
          </select>

          <ExpandMoreIcon />
        </div>
        <div className="icon-question col-2 col-md-1">
          {loadin ? (
            <div className="loading-img">{<Ellipsis />}</div>
          ) : (
            <SaveIcon
              onClick={() => {
                // errorArraWordBlank.length === 0
                //   ? setErrorHandlerModal(true)
                //   : setErrorHandlerModal(false) &&
                //     postApiAllData() &&
                //     setLoading(true);
                postApiAllData() && setLoading(true);
              }}
            />
          )}

          <CloseIcon
            className="pointer"
            onClick={() => {
              props.setCreateQuestion(false);
              props.setScrollTop(true);
            }}
          />
        </div>
      </div>
      {type}
      {loading1 && <Loading isOpen={loading1} />}

      <Modal
        isOpen={statusOkState}
        onRequestClose={closeStatusOkModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <div className="col-12 mb-4">درخواست شما با موفقیت ثبت شد</div>
        <div
          className="btn btn-outline-success col-12 mt-2"
          onClick={closeStatusOkModal}
        >
          تایید
        </div>
      </Modal>
      {/* {errorHandlerModal ? (
        <ErrorModal
          isOpen={errorHandlerModal}
          error={"test"}
          close={setErrorHandlerModal}
        />
      ) : null} */}
    </>
  );
};

export default HeaderQuestion;
