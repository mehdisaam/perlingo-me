import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { useDispatch, useSelector } from "react-redux";
import { Chip } from "@material-ui/core";
import {
  setRevieweWords,
  setRevieweWordsIndirectory,
  setArrayNewWorldFooter,
  setValueState,
  setLanguageType,
  setWordsMain,
  setCorrectAnswerNew,
  setCorrectAnswer,
  setWordInAnswer,
} from "../../redux/action/user";
import {
  fade,
  ThemeProvider,
  withStyles,
  makeStyles,
  createMuiTheme,
} from "@material-ui/core/styles";
// import InputBase from "@material-ui/core/InputBase";
// import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import { green } from "@material-ui/core/colors";
const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "green",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00ccff",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#777777",
      },
      "&:hover fieldset": {
        borderColor: "#777777",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00ccff",
      },
    },
  },
})(TextField);

const useStylesReddit = makeStyles((theme) => ({
  root: {
    border: "1px solid #e2e2e1",
    overflow: "hidden",
    borderRadius: 4,
    backgroundColor: "#fcfcfb",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:hover": {
      backgroundColor: "#fff",
    },
    "&$focused": {
      backgroundColor: "#fff",
      boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
      borderColor: theme.palette.primary.main,
    },
  },
  focused: {},
}));
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

const Aotucomplte = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [tagNewArray, setTagNewArray] = useState();
  const [value, setValue] = useState([]);
  const [filterWord, setFilterWord] = useState([]);
  const [lang, setLang] = useState("fa");
  const [blantBoolean, setBlankBoolean] = useState();
  var newArrayTest = [];
  const onChangeHandler = (e, value) => {
    value?.forEach((item) => {
      newArrayTest.push({
        word_id: item._id,
        word: item.word,
        blank: e === item.word ? true : false,
      });
      props.type === "new-Array" &&
        dispatch(setArrayNewWorldFooter(value.map((item) => item.word)));
      props.type === "reviewed_indirectly" &&
        dispatch(setRevieweWordsIndirectory(value.map((item) => item.word)));
      props.type === "reviewed_words" &&
        dispatch(setRevieweWords(value.map((item) => item.word)));
    });

    if (props.type === "blank-type" || props.type === "blank-choise") {
      setTagNewArray(value);
      if (props.type === "reviewed_words") {
        dispatch(setRevieweWords(value));
      } else if (props.type === "reviewed_indirectly") {
        dispatch(setRevieweWordsIndirectory(value));
      }
      if (props.type === "blank-choise") {
        // dispatch(setArrayNewWorldFooter(value));
        dispatch(setWordsMain(newArrayTest));
      }

      // if (props.type === "blank-type") {
      //   dispatch(setValueState(value));
      // }
      var result = newArrayTest?.findIndex((correct) => correct.blank === true);

      // dispatch(setCorrectAnswerNew(newArrayTest));
      if (props.type === "blank-type") {
        dispatch(setCorrectAnswerNew([newArrayTest[result]?.word]));
      }
      dispatch(setWordInAnswer(newArrayTest));
    }
  };

  var correctAnswerArray = [];
  const handleBlankType = (e, value) => {
    setBlankBoolean(e.target.value);
    onChangeHandler(e.target.value, tagNewArray);
  };
  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    setLang(language);
  };
  useEffect(() => {
    dispatch(setLanguageType(lang));
  }, [lang, dispatch]);

  const handleChange = (event) => {
    setValue(event.target.value);
  };
  var arrayCostome;
  if (user.nweArrayBlankType !== []) {
    arrayCostome = user.nweArrayBlankType;
  }
  if (user.newArrayFooterUi !== []) {
    arrayCostome = user.newArrayFooterUi;
  }

  return (
    <div>
      {props.blankFromApi ? (
        <div
          className={`d-flex justify-content-center align-items-center style-lang languages_${lang}`}
        >
          <button
            className={`btn ${lang === "fa" ? "text_bold" : ""}`}
            onClick={() => handleFilterWord(props.blankFromApi, "fa")}
          >
            Fa
          </button>
          <span>|</span>
          <button
            onClick={() => handleFilterWord(props.blankFromApi, "en")}
            className={`btn ${lang === "en" ? "text_bold" : ""}`}
          >
            En
          </button>
        </div>
      ) : null}
      <Autocomplete
        onChange={(e, v) => onChangeHandler(e, v)}
        multiple
        id="tags-outlined"
        options={
          props.type === "blank-choise" || props.type === "blank-type"
            ? filterWord
            : props.rightWorldGetFromApi ||
              props.indirectlyWorldGetFromApi ||
              arrayCostome
        }
        getOptionLabel={(option) => {
          return filterWord ||
            props.rightWorldGetFromApi ||
            props.indirectlyWorldGetFromApi ||
            arrayCostome
            ? option.word
            : option?.title;
        }}
        filterSelectedOptions
        renderTags={(value, getTagProps) =>
          value.map((option, index) => {
            return (
              <Chip
                variant="outlined"
                label={option.word}
                size="small"
                {...getTagProps({ index })}
                className="test"
                icon={
                  props.type === "blank-choise" ||
                  props.type === "blank-type" ? (
                    <div className="input-radio-submit-word">
                      <input
                        type="radio"
                        name="test"
                        value={option.word}
                        onChange={(e) => handleBlankType(e)}
                      />
                    </div>
                  ) : null
                }
              />
            );
          })
        }
        disabled={
          (props.type === "new-Array" && user.conditionalType === "F") ||
          (props.type === "new-Array" && user.conditionalType === "L") ||
          (props.type === "new-Array" && user.conditionalType === "M")
            ? true
            : false
        }
        renderInput={(params) => {
          return (
            <CssTextField
              placeholder={props.blankFromApi ? "افزودن کلمه" : ""}
              {...params}
              multiLine={true}
              variant={props.blankFromApi ? "filled" : "outlined"}
              label={props.label}
              className={`${classes.margin}"place-holder"`}
              disabled={
                (props.type === "new-Array" && user.conditionalType === "F") ||
                (props.type === "new-Array" && user.conditionalType === "L") ||
                (props.type === "new-Array" && user.conditionalType === "M")
                  ? true
                  : false
              }
            />
          );
        }}
      />
      {/* {props.blankFromApi ? (
        <div className="col-12 float-right radio-style">
          {" "}
          {tagNewArray?.map((item) => (
            <p>
              {
                <FormControl
                  component="fieldset"
                  className="col-12  w-100  text-right"
                >
                  <RadioGroup
                    aria-label="gender"
                    name="gender1"
                    value={value}
                    onChange={handleChange}
                    className="d-flex align-items-center"
                  >
                    <FormControlLabel
                      value={item.word}
                      control={<Radio value={item.word} />}
                    />
                  </RadioGroup>
                </FormControl>
              }
            </p>
          ))}
        </div>
      ) : null} */}
    </div>
  );
};

export default Aotucomplte;
