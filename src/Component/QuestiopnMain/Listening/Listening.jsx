import React, { useState, useEffect } from "react";
import BoxAddword from "../../QuestiopnMain/ChoseWordLang";
import PrePicture from "../../QuestiopnMain/Speaking/Pictures";
import axios from "axios";
import { BaseUrl } from "../../../Constant/Config";
import { useDispatch, useSelector } from "react-redux";
import Voice from "../../AddWord/Voices";
import Correct from "../../QuestiopnMain/Wordbank/CorrectAnswer";
import Checkbox from "@material-ui/core/Checkbox";
import Question from "../Wordbank/AutocompleteWordbank";
import TextField from "@material-ui/core/TextField";
import { setTitle, setValidation } from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

export default function Listening(props) {
  // State
  const titleApi = props?.item?.title;
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector((state) => state.user);
  const [pic_id, setPic_id] = useState(); //sabt id braye ersal be server property picture
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode baraye neshan dadan
  const [wordList, setWordList] = useState([]); // hameye kalame ha
  const [questionWord, setQuestionWord] = useState(); // sabt klmat soal
  const [voiceMan, setVoiceMan] = useState(null); //voice agha
  const [correctAnswer, setCorrectAnswer] = useState([]);
  const [checkbox, setCheckbox] = useState(
    props?.item?.answer[0]?.word.length === 0 ||
      props?.item?.answer[0]?.word.length === undefined
      ? false
      : true
  );
  // console.log(props?.item?.answer[0]?.word.length === 0 ? "false" : "true", "props ansewr")
  const [openbox, setOpenbox] = useState(false);

  const [onChangeHeader, setonChangeHeader] = useState(
    titleApi ? titleApi : ""
  );

  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  const handletitle = (e) => {
    setonChangeHeader(e.target.value);

    dispatch(setTitle(e.target.value));
  };

  const handleChange = (event) => {
    setCheckbox(event.target.checked);
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  useEffect(() => {
    getWordList();
  }, [checkbox]);

  return (
    <div className="Listening">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 title_of_questions">
            <TextField
              variant="filled"
              id="standard-basic "
              value={onChangeHeader}
              className="text_bold"
              label={<p>عنوان سوال</p>}
              onChange={handletitle}
              autoComplete="off"
            />
          </div>
          <div className="col-12 justify-content-between d-block d-lg-flex align-items-center ">
            <div className="col-12 col-lg-3 d-flex align-items-center justify-content-center justify-content-lg-start">
              <PrePicture
                id="Wordbank"
                pic_id={pic_id}
                setPic_id={setPic_id}
                Picturehandler={Picturehandler}
                prePicture={prePicture}
                picture={picture}
                answerinapi={props.item}
              />
            </div>
            <div className="col-12 col-lg-6 p-0">
              <div
                className={`d-flex justify-content-center justify-content-lg-end mt-4 mt-lg-0 ${
                  voiceMan ? "ml-4-addword-after" : "ml-4-addword-before"
                }`}
              >
                <BoxAddword setWordList={setWordList} />
              </div>
              <div className="d-flex justify-content-center align-items-center mt-3">
                <Voice
                  id={"man"}
                  onChangeFunc={setVoiceMan}
                  label={"صدای مورد نظر خود را انتخاب کنید."}
                  answerinapi={props.item}
                />
              </div>
            </div>
            <div className="col-12 col-lg-3"></div>
          </div>
          <div className="col-12 col-lg-3"></div>
          <div className="col-12 col-lg-6 mt-4 pl-lg-4">
            <div className="d-flex justify-content-center">
              <div className="correct_awnser w-100">
                <Correct
                  answerinapi={props.item}
                  setCorrectAnswer={setCorrectAnswer}
                />
              </div>
            </div>
          </div>
          <div
            className="col-12 d-flex align-items-center mt-5  pointer"
            onClick={() => {
              setOpenbox(!checkbox);
              setCheckbox(!checkbox);
            }}
          >
            <Checkbox
              checked={checkbox}
              onChange={handleChange}
              color="primary"
            />
            <span className="text_bold">
              استفاده از بانک کلمات برای پاسخ دهی
            </span>
          </div>

          <div className={`col-12 mt-4 ${checkbox ? "openbox" : "closebox"}`}>
            <div className="d-flex justify-content-center justify-content-lg-start align-items-center text-center">
              <span className="text_bold">
                {" کلمـات مورد نظر را جهت نمـایش در بانک کلمـات انتخـاب کنید. "}
              </span>
            </div>
            <div className="mr-auto w-100 w-lg-50 mt-5 li-wb">
              <Question
                setQuestionWord={setQuestionWord}
                Words={wordList}
                answerinapi={props.item}
                type="listening"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
