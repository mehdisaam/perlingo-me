import React, { useState, useEffect } from "react";
import FooterQuestion from "../FooterQuestion";
import Flashcart from "../Flashcart/Flashcart";
import HeaderQuestion from "../HeaderQuestion";
import Listening from "../Listening/Listening";
import Wordbank from "../Wordbank/Wordbank";
import SaveIcon from "@material-ui/icons/Save";
import Multicoice from "../Multichoice/Multicoice";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { useHistory, useLocation } from "react-router-dom";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import Ellipsis from "@bit/joshk.react-spinners-css.ellipsis";
import Match from "../Match/Match";
import TypeMain from "../Typing/TypeMain";
import Speaking from "../Speaking/Speaking";
import BlankTypeMain from "../BlankType/BlankTypeMain";
import ErrorModal from "../../Error Modal/ErrorModal";
import Loading from "../../Error Modal/Loading";
import Modal from "react-modal";
import CloseIcon from "@material-ui/icons/Close";

import {
  setCheckpoint_id,
  setQuestion,
  setTypeForFotterArray,
  setEmptyArray,
  setCorrectAnswerNew,
} from "../../../redux/action/user";
const EditeQuestion = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const location = useLocation();
  const counter = useSelector((state) => state.user);
  const [loadin, setLoading] = useState(false);
  const [loading1, setLoading1] = useState(false);
  const [statusOkState, setStatusOk] = useState(false);
  const [changeSelectOption, setSelectOption] = useState();
  const [errorHandler, setErrorHandler] = useState(false);
  const [textError, setTextModal] = useState("");
  console.log(props.boolean, "boolean");
  const closeStatusOkModal = () => {
    setStatusOk(false);
  };
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  const correct_answerFromApi = props.item.answer[0].correct_answer.map(
    (item) => item
  );
  const wordFromApi = props.item.answer[0].word.map((item) => item.word);
  const wordAnswer = props.item.words.map((item) => item.word);
  const [accardion, setAccardion] = useState(false);
  const handleClickedit = (id) => {
    setAccardion(!accardion);
    if (document.querySelector(`#id_${id}`) !== null) {
      var el = document.querySelector(`#id_${id}`);
      if (accardion) {
        el.classList.add("EditRow");
      } else {
        return el.classList.remove("EditRow");
      }
    }
  };
  const postApiAllData = () => {
    setLoading1(true);
    var data = new FormData();
    data.append("checkpoint_id", location.state.checkpoint);
    data.append("lesson_id", location.state.lesson_id);
    data.append("level_id", location.state.level_id);
    data.append("skill_id", location.state.skilId);
    data.append("title", counter?.title);
    data.append("language", counter.typeLanguage);
    data.append("type", counter.typeQuestionMain);
    data.append("question_id", props.item._id);
    data.append("correct_answer", JSON.stringify(counter.nweCorrectAnswer));
    if (counter.picture !== "") {
      data.append("picture", counter.picture);
    }
    data.append("reviewed_words", JSON.stringify(counter.valueAutocompelte));
    data.append(
      "reviewed_indirectly",
      JSON.stringify(counter.valueRevieweWordsIndirectory)
    );
    data.append("audio", counter.booleanVoice);
    data.append(
      "voice_type",
      counter.typeWrld === "other"
        ? Math.random("male", "female")
        : counter.typeWrld
    );
    data.append(
      "answer",
      JSON?.stringify([
        {
          option: counter.correctAnswer,
          word: counter.wordAnswer,
        },
      ])
    );
    data.append("voice", counter.voiceFileListening);
    data.append("words", JSON.stringify(counter.wordsMain));
    data.append("new_words", JSON.stringify(counter.arraynewworldfooter));
    axios
      .put(
        `${bseurl}/admin/checkpoint/question?api_token=${localStorage.getItem(
          "token"
        )}`,
        data
      )
      .then((res) => {
        return (
          res.status === 200
            ? (setLoading(false), handleClickedit(props.item._id))
            : setLoading(true),
          res.status === 200 ? setStatusOk(true) : setStatusOk(false),
          dispatch(setQuestion(res.data.data.questions.reverse())),
          props.setCreateQuestion && props.setCreateQuestion(false),
          props.setScrollTop && props.setScrollTop(true),
          props.setShowEdit && props.setShowEdit(false),
          setLoading1(false)
        );
      })
      .catch((error) => {
        return (
          setErrorHandler(true),
          setTextModal(error.response.data),
          setLoading1(false),
          setLoading(false),
          error.response.data.status === 401 ? history.push("/login") : null
        );
      });
  };
  console.log(counter, "counter");
  console.log(props.item, "props.item");
  console.log(props.imginApiQuestion, " props.item =============>");
  return (
    <>
      <div className="main-question EdieSec" id={`id_${props.id}`}>
        {loadin ? (
          <div className="loading-img">{<Ellipsis />}</div>
        ) : (
          <>
            <SaveIcon
              color="action"
              fontSize="large"
              className="pointer"
              onClick={() => {
                // errorArraWordBlank.length === 0
                //   ? setErrorHandlerModal(true)
                //   : setErrorHandlerModal(false) &&
                //     postApiAllData() &&
                //     setLoading(true);
                postApiAllData() && setLoading(true);
              }}
            />
            <CloseIcon
              onClick={() => {
                props.setShowEdit(false);
              }}
              color="action"
              fontSize="large"
              className="pointer"
            />
          </>
        )}
        {props.type === "F" ? (
          <Flashcart
            item={props.item}
            titleApiQuestion={props.title}
            imginApiQuestion={props.imginApiQuestion}
          />
        ) : props.type === "L" ? (
          <Listening item={props.item} />
        ) : props.type === "WB" ? (
          <Wordbank item={props.item} titleApiQuestion={props.title} />
        ) : props.type === "S" ? (
          <Speaking item={props.item} />
        ) : props.type === "M" ? (
          <Match item={props.item} />
        ) : props.type === "T" ? (
          <TypeMain item={props.item} />
        ) : null}
        <FooterQuestion />
      </div>
      {loading1 && <Loading isOpen={loading1} />}
      {errorHandler ? (
        <ErrorModal
          isOpen={errorHandler}
          error={textError?.data?.map((item) => (
            <p>{item}</p>
          ))}
          close={setErrorHandler}
        />
      ) : null}
      <Modal
        isOpen={statusOkState}
        onRequestClose={closeStatusOkModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <div className="col-12 mb-4">درخواست شما با موفقیت ثبت شد</div>
        <div
          className="btn btn-outline-success col-12 mt-2"
          onClick={closeStatusOkModal}
        >
          تایید
        </div>
      </Modal>
    </>
  );
};
export default EditeQuestion;
