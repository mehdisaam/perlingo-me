import React, { useState, useEffect } from "react";
import Addword from "../../AddWord/Addword";
// Icons
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import iconError from "../../../assets/img/perlingo-buttonn.gif";
// material-ui
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import {
  CompassCalibrationOutlined,
  SignalCellularNoSimOutlined,
} from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  setWordInAnswer,
  setCorrectAnswerNew,
  setWordsMain,
  setArrayNewWorldFooter,
  setNewArrayFooterUi,
} from "../../../redux/action/user";
export default function SelectWord(props) {
  // State
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [filterWord, setFilterWord] = useState([]);
  const [filterWordEn, setFilterEn] = useState([]);
  const [filterChangeLangFa, setLangFa] = useState([]);
  const [chosenWord, setChosenWord] = useState([]);
  const [checkedValue, setCheckedVaue] = useState();
  const [lang, setLang] = useState(user.typeLanguage);
  const [langBlankChoise, setLangBlankChoise] = useState("fa");
  const [blantBoolean, setBlankBoolean] = useState();
  const [tagNewArray, setTagNewArray] = useState();
  var newArrayFooter = [];
  var newArrayFooterUi = [];
  var checkedFirstValue;

  // Functions
  const handleBlankType = (e, value, index) => {
    setBlankBoolean(e.target.value);
    onChangeHandler(e.target.value, tagNewArray);
  };

  var answerBlanktypeEdit = props?.answerinapi?.answer[0].word;
  var newArrayTest = [];
  var newArrayanswerBlank = [];
  answerBlanktypeEdit?.map((item) =>
    newArrayanswerBlank.push({
      blank: item.blank,
      word_id: item.word_id && item.word_id._id,
      word: item?.word_id?.word,
    })
  );
  const onChangeHandler = (e, value) => {
    value?.forEach((item) => {
      newArrayTest.push({
        word_id: item._id,
        word: item.word,
        blank: e === item.word ? true : false,
      });
      newArrayFooter.push(item.word);
    });
    user.conditionalType !== "BT" &&
      value.forEach((item) => {
        newArrayFooterUi.push({ word: item.word });
        dispatch(setNewArrayFooterUi(newArrayFooterUi));
      });

    //=========================================================== shold be checked type :)

    dispatch(setArrayNewWorldFooter(newArrayFooter));
    //===================
    if (props.type === "blank-type") {
      dispatch(setWordInAnswer(newArrayTest));
    }
    if (props.type === "blanck-choies") {
      dispatch(setWordsMain(newArrayTest));
    }
    if (props.type === "blanck-choies") {
      let answer = [];
      value.forEach((item) =>
        answer.push({ word: item.word, word_id: item._id })
      );
      // dispatch(setWordInAnswer(answer));
    }
    setTagNewArray(value);

    var result = newArrayTest?.findIndex((correct) => correct.blank === true);

    if (props.type === "blank-type") {
      dispatch(setCorrectAnswerNew([newArrayTest[result]?.word]));
    }
  };

  const SaveSelectedWords = () => {
    props.setSelectedWord(chosenWord);

    let answer = [];
    chosenWord.forEach((item) =>
      answer.push({ word: item.word, word_id: item._id })
    );
    dispatch(setWordInAnswer(answer));
    props.closeflashCartModal();
  };

  const handleAddflashcart = (e) => {
    if (e.keyCode === 13) {
      SaveSelectedWords();
    }
  };

  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    setLangBlankChoise(language);
  };
  const handleChangeEn = (words, language) => {
    setFilterEn(words?.filter((word) => word.language === "en"));
  };
  const handleFilterFa = (words) => {
    setLangFa(words?.filter((word) => word.language === "fa"));
  };
  useEffect(() => {
    handleFilterWord(props.Words, "fa");
    setChosenWord(props.selectedWord);
    handleChangeEn(props.Words, "en");
    handleFilterFa(props.Words, "fa");
  }, [props.Words]);
  const changeLang = () => {};
  useEffect(() => {
    changeLang();
  }, []);
  useEffect(() => {
    setLang(user.typeLanguage);
  }, [user.typeLanguage]);

  const funcCheckedValueFirst = (event) => {
    setCheckedVaue(event);
  };
  user.wordAnswer.filter((item) => (item.blank !== true, "rrwwweeeqqq"));
  const errorModalBlank = user.wordsMain.filter((item) => item.blank === true);

  return (
    <>
      <div
        className="outline-none h-fill"
        // onKeyDown={(e) => handleAddflashcart(e)}
        // tabIndex="0"
      >
        <div className="container">
          <div className="row">
            <div class=" d-block d-md-flex w-100 mt-3 mt-lg-5 ">
              {props.type === "blanck-choies" ? (
                <div
                  className={`d-flex justify-content-center align-items-center style-language languages_${langBlankChoise}`}
                >
                  <button
                    className={`btn ${
                      langBlankChoise === "fa" ? "text_bold" : ""
                    }`}
                    onClick={() => handleFilterWord(props.Words, "fa")}
                    // onChange={() => handleFilterWord(props.Words, "fa")}
                  >
                    Fa
                  </button>
                  <span>|</span>
                  <button
                    onClick={() => handleFilterWord(props.Words, "en")}
                    // onChange={() => handleChangeEn(props.Words, "en")}
                    className={`btn ${
                      langBlankChoise === "en" ? "text_bold" : ""
                    }`}
                  >
                    En
                  </button>
                </div>
              ) : (
                <>
                  {/* <button
                    className={`btn ${lang === "en" ? "text_bold" : ""}`}
                    onChange={() =>
                      handleFilterWord(
                        props.Words,
                        user.typeLanguage === "fa" ? "en" : "fa"
                      )
                    }
                  >
                    En
                  </button>
                  <button
                    className={`btn ${lang === "fa" ? "text_bold" : ""}`}
                    onChange={() =>
                      handleFilterWord(
                        props.Words,
                        user.typeLanguage === "fa" ? "en" : "fa"
                      )
                    }
                  >
                    Fa
                  </button> */}
                </>
              )}
              <Autocomplete
                // defaultValue={
                //   props.selectedWord &&
                //   props.selectedWord.map((option) => option)
                // }
                onChange={(e, v) => onChangeHandler(e, v)}
                className={`rtl flashcart_taginput_${langBlankChoise} w-100`}
                multiple
                filterSelectedOptions
                id="tags-standard"
                options={
                  props.type === "blanck-choies"
                    ? filterWord && filterWord.map((option) => option)
                    : props.type === "blank-type" && lang === "en"
                    ? filterChangeLangFa &&
                      filterChangeLangFa.map((option) => option)
                    : props.type === "blank-type" && lang === "fa"
                    ? filterWordEn && filterWordEn.map((option) => option)
                    : props.Words && props.Words.map((option) => option)
                }
                defaultValue={
                  newArrayanswerBlank && newArrayanswerBlank.map((item) => item)
                }
                getOptionLabel={(option) => option.word}
                getOptionDisabled={(options) => options}
                autoComplete
                renderTags={(value, getTagProps) =>
                  value.map((option, index) => {
                    return (
                      <Chip
                        variant="outlined"
                        label={option.word}
                        size="small"
                        {...getTagProps({ index })}
                        className="test"
                        icon={
                          <div className="input-radio-submit-word">
                            <input
                              type="radio"
                              name="test"
                              value={
                                newArrayanswerBlank ? option.word : option.word
                              }
                              onChange={(e) => handleBlankType(e, index)}
                            />
                          </div>
                        }
                      />
                    );
                  })
                }
                renderInput={(params) => (
                  <TextField
                    className="placeholder_style"
                    {...params}
                    variant="filled"
                    placeholder={
                      langBlankChoise === "fa" ? "انتخاب کلمه " : "Chose word"
                    }
                    autoComplete="off"
                  />
                )}
              />
            </div>
          </div>
          {errorModalBlank.length === 0 ? (
            <div className="style-text">
              <div className="style-gif float-left ">
                {" "}
                <img src={iconError} alt={iconError} />
              </div>
              <p> لطفا یکی از گزینه هارا انتخاب کنید</p>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
}
