import React, { useState, useEffect } from "react";
import Addword from "../../AddWord/Addword";
// Icons
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";

// material-ui
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import { SignalCellularNoSimOutlined } from "@material-ui/icons";
import { useDispatch } from "react-redux";
import {
  setLanguageType,
  setFlashcardAnswer,
  setAllAnswer,
  setWordInAnswer,
  setCorrectAnswerNew,
  setWordsMain,
  setLoadingLang,
  setArrayNewWorldFooter,
  setNewArrayFooterUi,
} from "../../../redux/action/user";
export default function QuestionType(props) {
  // State
  const dispatch = useDispatch();
  const [filterWord, setFilterWord] = useState([]);
  const [chosenWord, setChosenWord] = useState([]);
  const [lang, setLang] = useState("fa");
  const [blantBoolean, setBlankBoolean] = useState();
  const [tagNewArray, setTagNewArray] = useState();
  const [filterWordEn, setFilterEn] = useState([]);
  const [filterChangeLangFa, setLangFa] = useState([]);
  // Functions
  // const handleBlankType = (e, value) => {
  //   setBlankBoolean(e.target.value);
  //   onChangeHandler(e.target.value, tagNewArray);
  // };
  var newArrayTest = [];
  var newArrayFooter = [];
  var newArrayFooterUi = [];
  const onChangeHandler = (e, value) => {
    value?.forEach((item) => {
      newArrayTest.push({
        word_id: item._id,
        word: item.word,
        blank: e === item.word ? true : false,
      });
    });

    value.forEach((item) => {
      newArrayFooter.push(item.word);
    });

    value.forEach((item) => {
      newArrayFooterUi.push({ word: item.word });
    });
    // dispatch(setArrayNewWorldFooter(newArrayFooter));
    dispatch(setNewArrayFooterUi(newArrayFooterUi));
    if (props.type === "blank-type") {
      dispatch(setArrayNewWorldFooter(newArrayFooter));
      dispatch(setCorrectAnswerNew([newArrayTest[result]?.word]));
    }
    setTagNewArray(value);

    var result = newArrayTest?.findIndex((correct) => correct.blank === true);

    let answer = [];
    value?.forEach((item) =>
      answer.push({ word: item.word, word_id: item._id })
    );
    // dispatch(setWordInAnswer(answer));
    dispatch(setWordsMain(answer));
  };
  // Functions

  const SaveSelectedWords = () => {
    props.setSelectedWord(chosenWord);

    props.closeflashCartModal();
  };

  const handleAddflashcart = (e) => {
    if (e.keyCode === 13) {
      SaveSelectedWords();
    }
  };

  const handleFilterWord = (words, language) => {
    setFilterWord(words?.filter((word) => word.language === language));
    dispatch(setLanguageType(language));

    setLang(language);
  };
  const handleChangeEn = (words, language) => {
    setFilterEn(words?.filter((word) => word.language === "en"));
  };
  const handleFilterFa = (words) => {
    setLangFa(words?.filter((word) => word.language === "fa"));
  };
  useEffect(() => {
    handleFilterWord(props.Words, "fa");
    setChosenWord(props.selectedWord);
    handleChangeEn(props.Words, "en");
    handleFilterFa(props.Words, "fa");
  }, [props.Words]);

  const newArrayEditeQuestionBlankType = [];

  const questionDataEditFromApi = props?.answerinapi?.words;
  questionDataEditFromApi?.map((item) =>
    newArrayEditeQuestionBlankType?.push({
      word: item.word_id && item.word_id.word,
      word_id: item.word_id && item.word_id._id,
    })
  );

  return (
    <>
      <div
        className="outline-none h-fill"
      // onKeyDown={(e) => handleAddflashcart(e)}
      // tabIndex="0"
      >
        <div className="container">
          <div className="row">
            <div class="d-md-flex w-100 mt-3 mt-lg-5 ">
              <div
                className={`d-flex justify-content-end align-items-center style-language languages_${lang}`}
              >
                <button
                  className={`btn ${lang === "fa" ? "text_bold" : ""}`}
                  onClick={() => handleFilterWord(props.Words, "fa")}
                >
                  Fa
                </button>
                <span>|</span>
                <button
                  onClick={() => handleFilterWord(props.Words, "en")}
                  className={`btn ${lang === "en" ? "text_bold" : ""}`}
                >
                  En
                </button>
              </div>
              <Autocomplete
                onChange={(e, v) => onChangeHandler(e, v)}
                className={`rtl flashcart_taginput_${lang} w-100`}
                multiple
                filterSelectedOptions
                id="tags-standard"
                options={
                  props.type === "blank-type" || props.type === "Multi"
                    ? filterWord.map((option) => option)
                    : props.Words.map((option) => option)
                }
                defaultValue={
                  newArrayEditeQuestionBlankType &&
                  newArrayEditeQuestionBlankType.map((item) => item)
                }
                autoComplete
                getOptionLabel={(option) => option.word}
                getOptionDisabled={(options) =>
                  chosenWord?.length > 3 ? true : false
                }
                renderInput={(params) => (
                  <TextField
                    className="placeholder_style"
                    {...params}
                    variant="filled"
                    placeholder={
                      lang === "fa" ? "سوال خود را انتخاب کنید " : "Chose word"
                    }
                    autoComplete="off"
                  />
                )}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
