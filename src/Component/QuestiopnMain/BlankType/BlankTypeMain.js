import React, { useState, useEffect } from "react";
import LoadingMain from "../../Loading/Loading";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import { ButtonGroup, Button } from "react-bootstrap";
import ChoseWordLang from "../ChoseWordLang";
import Aotucomplte from "../Aotucomplte";
import ResSearchWorld from "../ResSearchWorld";
import QuestionType from "./QuestionAuto";
import { useDispatch, useSetDispatch } from "react-redux";
import classnames from "classnames";
import SelectWord from "./Aotucomplete";
import { setTitle, setValidation } from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

const BlankTypeMain = (props) => {
  const dispatch = useDispatch();
  const [loadingModal, setLoadingModal] = useState(false);
  const [blankFromApi, setBlankFromApi] = useState();
  const [chooseLang, setChooseLang] = useState("fa");
  const [langFa, setLangefa] = useState();
  const [langEng, setLangEng] = useState();
  const [wordList, setWordList] = useState([]);
  const [selectedWord, setSelectedWord] = useState([]);
  const [onChangeHeader, setonChangeHeader] = useState(
    props.item ? props.item.title : ""
  );
  const history = useHistory();
  // State
  const [titlef, setTitlef] = useState("");

  var className;
  const handleChooseLang = (chooseLang) => {
    setChooseLang(chooseLang);
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) => {
        return error.response.data.status === 401
          ? history.push("/login")
          : null;
      });
  };

  // function

  const handletitle = (e) => {
    dispatch(setTitle(e.target.value));
    setTitlef(e.target.value);
  };

  useEffect(() => {
    getWordList();
  }, []);
  function closeflashCartModal() {
    setFlashCartModal(false);
  }
  const [flashCartModal, setFlashCartModal] = useState(false);
  // useEffect(() => {
  //   if (chooseLang === "fa") {
  //     setBlankFromApi(langFa);

  //     className += "active-default";
  //   } else {
  //     setBlankFromApi(langEng);
  //   }
  // }, [chooseLang, langFa, langEng]);
  const handleChangeHeader = (e, value) => {
    dispatch(setTitle(e.target.value));
    setonChangeHeader(e.target.value);
  };
  return (
    <>
      {loadingModal ? <LoadingMain /> : null}{" "}
      <div className="col-12  blank-main blank-type">
        <div className="col-12 text-right header-type">
          <TextField
            variant="filled"
            id="standard-basic "
            value={onChangeHeader}
            label={<p>عنوان سوال</p>}
            onChange={handleChangeHeader}
            autoComplete="off"
          />
        </div>
        <div className="col-12 my-5 text-right d-md-flex justify-content-between align-items-center p-0 d-block">
          <p className="m-0 col-12 col-md-5">
            کلمات مورد نظر رو به ترتیب برای عنوان سوال انتخاب کنید و جای خالی رو
            مشخص کنید
          </p>
          {/* <div className="lang-main-question  text-right">
            <ButtonGroup>
              <Button
                onClick={() => handleChooseLang("fa")}
                className={classnames(chooseLang === "fa" ? "active-lang" : "")}
              >
                {" "}
                Fa
              </Button>
              <Button
                onClick={() => handleChooseLang("en")}
                className={classnames(chooseLang === "en" ? "active-lang" : "")}
              >
                {" "}
                En
              </Button>
            </ButtonGroup>
          </div> */}

          <ChoseWordLang />
        </div>
        <div className="col-12 p-0 mb-3">
          <QuestionType
            Words={wordList}
            type="blank-type"
            answerinapi={props.item}
          />
        </div>
        <div className="div-like-textarea">
          <div className="col-10 aotu-blank-content">
            <SelectWord
              setWordList={setSelectedWord}
              Words={wordList}
              selectedWord={selectedWord}
              setSelectedWord={setSelectedWord}
              type="blank-type"
              answerinapi={props.item}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default BlankTypeMain;
