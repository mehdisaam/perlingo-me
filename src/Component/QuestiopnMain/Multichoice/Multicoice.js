import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import ChoseWordLang from "../ChoseWordLang";
import ResSearchWorld from "../ResSearchWorld";
import axios from "axios";
import Answer from "../BlankSingle/Answer";
import classnames from "classnames";
import QuestionType from "../BlankType/QuestionAuto";
import { useDispatch, useSelector } from "react-redux";
import { setTitle, setValidation } from "../../../redux/action/user";
import { api_token } from "../../../api-config/tiken-main";
import { bseurl } from "../../../api-config/base-url-main";
import { useHistory } from "react-router";

const Multicoice = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [loadingModal, setLoadingModal] = useState(true);
  const [blankFromApi, setBlankFromApi] = useState();
  const [chooseLang, setChooseLang] = useState("fa");
  const [langFa, setLangefa] = useState();
  const [langEng, setLangEng] = useState();
  const [wordList, setWordList] = useState([]);
  const [onChangeHeader, setonChangeHeader] = useState();
  const [callValue, setCallValue] = useState(1);
  const [addAnswer, setAddAnswer] = useState([
    <Answer callValue={1} testClick={() => addAnswerHandler()} />,
  ]);
  let test;
  const addAnswerHandler = (value) => {
    setCallValue(callValue + 1);
    setAddAnswer([...addAnswer, test]);
  };
  const handleChangeHeader = (e, value) => {
    setonChangeHeader(e.target.value);
    dispatch(setTitle(e.target.value));
  };

  const getWordList = () => {
    axios
      .get(
        `${bseurl}/admin/word/list?api_token=${localStorage.getItem("token")}`
      )
      .then((res) => {
        setWordList(res.data.data);
      })
      .catch((error) => {
        return error?.response?.data?.status === 401
          ? history.push("/login")
          : null;
      });
  };
  useEffect(() => {
    getWordList();
  }, []);
  const deleteaddAnswerHandler = (e) => {
    setAddAnswer(addAnswer.splice(e, 1));

    setAddAnswer([...addAnswer]);
  };
  const classCloseButton = "classTest";

  const answerVlue = props?.item?.answer[0].option;
  console.log(props.blankType, "gggasfdfff");
  return (
    <div className="col-12  blank-main multi-choies">
      <div className="col-12 text-right header-type d-flex align-items-center justify-content-between mb-5">
        <TextField
          variant="filled"
          id="standard-basic "
          value={onChangeHeader}
          label={<p>عنوان سوال</p>}
          onChange={handleChangeHeader}
          autoComplete="off"
        />
      </div>
      <div className="col-12 my-5 text-right d-md-flex justify-content-between align-items-center p-0 ">
        <p className="m-0">
          کلمات مورد نظر رو به ترتیب برای عنوان سوال انتخاب کنید و جای خالی رو
          مشخص کنید
        </p>

        <ChoseWordLang setWordList={setWordList} />
      </div>
      <div className="col-12 p-0 mb-5 mt-4">
        <QuestionType Words={wordList} type="Multi" answerinapi={props.item} />
      </div>
      <div className=" text-right p-0 style-answer-responsev">
        <p>پاسخ های خود را وارد کنید و گزینه درست را انتخاب کنید </p>
      </div>
      <div className="col-12 p-0 box-answer d-flex justify-content-end">
        <div className="add-div-answer p-0 col-md-6 float-left style-answer-responsev">
          {addAnswer.map((item, index) => {
            return (
              <>
                <Answer
                  addAnswer={addAnswer}
                  callValue={callValue}
                  testClick={() =>
                    addAnswer?.length > 4 ? null : addAnswerHandler()
                  }
                  index={index + 1}
                  value={addAnswer}
                  deleteIcon={() =>
                    addAnswer?.length < 2 ? null : deleteaddAnswerHandler(index)
                  }
                  answerinapi={props.item}
                />
              </>
            );
          })}
          {/* {answerVlue && (answerVlue === undefined || answerVlue === []) ? (
            addAnswer && addAnswer?.length > 3 ? null : (
              <button
                type="botton"
                onClick={(item) => {
            
                  return addAnswer !== undefined && addAnswer.length < 3
                    ? addAnswerHandler(callValue)
                    : null;
                }}
              >
                <span>+</span> افزودن پاسخ جدید
              </button>
            )
          ) : answerVlue.length > 3 ? null : (
            <button
              type="botton"
              onClick={(item) => {
                return addAnswer?.length > 3
                  ? null
                  : addAnswerHandler(callValue);
              }}
            >
              <span>+</span> افزودن پاسخ جدید
            </button>
          )} */}
          {addAnswer && addAnswer?.length > 3 ? null : (
            <button
              type="botton"
              className="add-class-style-button-add"
              onClick={(item) => {
                return addAnswer !== undefined && addAnswer.length < 4
                  ? addAnswerHandler(callValue)
                  : null;
              }}
            >
              <span>+</span> افزودن پاسخ جدید
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Multicoice;
