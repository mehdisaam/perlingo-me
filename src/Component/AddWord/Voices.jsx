import React, { useEffect, useState } from "react";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import { useDispatch, useSelector } from "react-redux";
import { setVoiceFile } from "../../redux/action/user";
import { BaseUrl } from "../../Constant/Config";
import { AlertTitle } from "@material-ui/lab";

export default function Voices(props) {
  const dispatch = useDispatch();
  // State
  const user = useSelector((state) => state.user);
  const [sound, setSound] = useState(false);
  const [audio, setAudio] = useState(
    props.answerinapi &&
      user.voiceBoolean === true &&
      props.answerinapi !== undefined
      ? props?.answerinapi?.voice
      : ""
  );
  // functions
  const soundPlayer = (file) => {
    if (file !== undefined) {
      setAudio(new Audio(URL.createObjectURL(file)));
    }
  };

  const playaudio = () => {
    setSound(!sound);
    sound ? audio.pause() : audio.play();
  };
  console.log(props.answerinapi, "propspropspropsprops");
  const shouldempty = () => {
    setAudio(null);
    setSound(false);
    props.setshouldempty(false);
  };

  useEffect(() => {
    audio !== undefined &&
      props.defaultValue &&
      props.defaultValue !== undefined &&
      setAudio(new Audio(`${BaseUrl}${props.defaultValue}`));
    audio !== undefined &&
      props.answerinapi &&
      props.answerinapi.voice !== undefined &&
      setAudio(new Audio(`${BaseUrl}${props.answerinapi.voice}`));
  }, [props.answerinapi, props.defaultValue]);

  useEffect(() => {
    props.shouldempty && shouldempty();
  }, [props.shouldempty]);

  return (
    <div className="upload_voice">
      <div className="d-flex justify-content-center align-items-center w-100">
        <label
          className="pointer w-100"
          for={props.id}
          // onMouseEnter={() => logoPreview != null ? setHoverUpImg(logoPreview) : null}
        >
          <div className="d-flex align-items-center justify-content-center">
            <div
              className={`d-none d-sm-block text-center ${props.w_box} ${
                audio ? "voice_box1_full" : "voice_box1"
              }`}
            >
              <VolumeUpIcon fontsie="large" className="miror mr-1 text_gray" />
              <span className="text_style mx-2 text-center">{props.label}</span>
            </div>
            <div className={`${audio ? "voice_box2_full" : "voice_box2"}`}>
              <span className="text_style mx-4 text-center">انتخاب صدا</span>
            </div>
          </div>
        </label>
        <input
          id={props.id}
          type="file"
          onChange={(e) => {
            soundPlayer(e.target.files[0]);

            props.setVc && props.setVc(true);
            props.onChangeFunc(e.target.files[0]);
            dispatch(setVoiceFile(e.target.files[0]));
          }}
          accept=".mp3"
          hidden
        />
        <span
          className="pointer text_style mr-2 mb-2"
          onClick={audio && playaudio}
        >
          {audio ? (sound ? "توقف" : " پخش ") : null}
        </span>
      </div>
    </div>
  );
}
