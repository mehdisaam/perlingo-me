import React, { useState } from "react";
import Modal from "react-modal";
import TextField from "@material-ui/core/TextField";
import SelectLanguage from "./SelectLanguage";
import Autocomplete from "./Autocomplete_Addword";
import Voice from "./Voices";
import PrePicture from "./PrePicture";
import { BaseUrl } from "../../Constant/Config";
import axios from "axios";
import Loading from "../Error Modal/Loading";
import Error from "../Error Modal/ErrorModal";
import { useDispatch, useSelector } from "react-redux";
import { setWORDLIST } from "../../redux/action/user";

// Icons
import CloseIcon from "@material-ui/icons/Close";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";

// Images
import vcWoman from "../../assets/img/voice_woman.png";
import vcman from "../../assets/img/voice_man.png";
import acWoman from "../../assets/img/active_woman.png";
import acman from "../../assets/img/active_man.png";
import { api_token } from "../../api-config/tiken-main";
import { bseurl } from "../../api-config/base-url-main";
import { useHistory } from "react-router-dom";

export default function Addword(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  // State
  const [picture, setPicture] = useState(); //sabt aks entekhab shode
  const [prePicture, setPrePicture] = useState(); //preview aks entekhab shode
  const [addWord, setAddWord] = useState(false); // modal afzodan kalame
  const [wordName, setWordName] = useState(""); // sabt nam kalame
  const [lang, setLang] = useState("en"); // sabt language
  const [voiceMan, setVoiceMan] = useState(null); //voice agha
  const [voiceWoman, setVoiceWoman] = useState(null); // voice khanom
  const [selectedWord, setSelectedWord] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorModal, setErrorModal] = useState(false);
  const [emptyvc, setEmptyvc] = useState(false);

  // Modal
  function openAddWord() {
    setAddWord(true);
  }
  function closeAddWord() {
    setAddWord(false);
  }
  const AddWordStyle = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: "10",
      backgroundColor: "rgba(0 0 0 / 40%)",
    },
    content: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: "auto",
      width: "70%",
      height: "290px",
      zIndex: "10",
      borderRadius: "30px",
    },
  };

  const clearstate = () => {
    setWordName("");
    setLang("");
    setSelectedWord([]);
    setPicture();
  };
  // functions
  const handleAddWord = (e) => {
    if (e.keyCode === 13) {
    }
  };

  const handleLanguage = (event) => {
    setLang(event.target.value);
  };

  const Picturehandler = (e) => {
    if (e.target.files[0] !== undefined) {
      setPicture(e.target.files[0]);
      setPrePicture(URL.createObjectURL(e.target.files[0]));
    }
  };

  // post word
  const addWordApi = () => {
    setLoading(true);
    let data = new FormData();
    data.append("picture", picture);
    data.append("word", wordName);
    data.append("meanings", JSON.stringify(selectedWord));
    data.append("female", voiceWoman);
    data.append("male", voiceMan);
    data.append("language", lang);
    let headers = { "Content-Type": "multipart/form-data" };
    axios
      .post(
        `${BaseUrl}/api/v1/admin/word?api_token=${localStorage.getItem(
          "token"
        )}`,
        data,
        headers
      )
      .then((res) => {
        props.setWordList && props.setWordList(res.data.data);
        props.setWord && props.setWord(res.data.data.reverse());
        dispatch(setWORDLIST(res.data.data));
        setLoading(false);
        closeAddWord();
        setEmptyvc(true);
        clearstate();
      })
      .catch((err) => {
        setError(err.response.data.data);
        console.log("catch");
        setLoading(false);
        setErrorModal(true);
        // return error.response.data.status === 401
        //   ? history.push("/login")
        //   : null;
      });
  };

  return (
    <>
      <div onClick={openAddWord} className=" text_style_box pointer">
        + افزودن کلمه
      </div>

      {addWord && (
        <Modal
          isOpen={addWord}
          onRequestClose={closeAddWord}
          style={AddWordStyle}
          closeTimeoutMS={200}
        >
          <div
            className="outline-none h-fill"
            onKeyDown={(e) => handleAddWord(e)}
            tabIndex="0"
          >
            <div className="row d-flex justify-content-between align-items center px-4">
              <div>
                <CloseIcon
                  fontSize="large"
                  className="pointer"
                  onClick={closeAddWord}
                />
                <span className="mr-2 text_bold fs_xs_14px">ثبت کلمه جدید</span>
              </div>
              <div onClick={addWordApi} className="pointer">
                <SaveOutlinedIcon fontSize="large" />
                <span className="text_bold d-none d-md-inline">ذخیره</span>
              </div>
            </div>
            <hr className="hr" />
            <div className="addword">
              <div className="row">
                <div className="col-12">
                  <div className="add_word_box">
                    <div className="row px_wordbank pt-3 pb-4 d-flex align-items-center ">
                      <div className="col-12 col-lg-3">
                        <div className="d-flex justify-content-center align-items-center">
                          <PrePicture
                            id={"AddWord"}
                            Picturehandler={Picturehandler}
                            prePicture={prePicture}
                            picture={picture}
                          />
                        </div>
                      </div>
                      <div className="col-12  col-lg-4 style-word-component">
                        <div className="d-flex justify-content-center justify-content-lg-start align-items-center focus_lable">
                          <div>
                            <TextField
                              onChange={(e) => setWordName(e.target.value)}
                              label="نام کلمه"
                              value={wordName}
                              color="success"
                              id="word"
                            />
                          </div>
                          <div className="mr-5 focus_lable_lan">
                            <SelectLanguage
                              handleLanguage={handleLanguage}
                              lang={lang}
                            />
                          </div>
                        </div>
                        <div className="d-flex justify-content-center justify-content-lg-start align-items-center">
                          <Autocomplete
                            value={selectedWord}
                            setSelectedWord={setSelectedWord}
                            id="mean"
                          />
                        </div>
                      </div>
                      <div className="col-12  col-lg-5 style-word-component">
                        <div className="d-flex justify-content-center justify-content-lg-start align-items- mt-3">
                          <Voice
                            shouldempty={emptyvc}
                            setshouldempty={setEmptyvc}
                            id={"man"}
                            onChangeFunc={setVoiceMan}
                            label={"صدای کلمه(آقا)"}
                          />
                        </div>
                        <div className="d-flex justify-content-center justify-content-lg-start align-items-center mt-2 pt-1">
                          <Voice
                            shouldempty={emptyvc}
                            setshouldempty={setEmptyvc}
                            id={"woman"}
                            onChangeFunc={setVoiceWoman}
                            label={"صدای کلمه(خانم)"}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      )}
      {loading && <Loading isOpen={loading} />}
      {errorModal && (
        <Error isOpen={errorModal} error={error} close={setErrorModal} />
      )}
    </>
  );
}
