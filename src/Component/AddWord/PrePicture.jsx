import React from 'react'
import No_Image from '../../assets/img/No_Image.png'

// ICONS
import AddIcon from '@material-ui/icons/Add';

export default function PrePicture(props) {
    return (
        <>
            <label
                className="add_img_box pointer"
                for={props.id}
            // onMouseEnter={() =>}
            >
                {
                    props.prePicture ?
                        (
                            (props.picture?.name?.split('.').pop() === 'jpg' || props.picture?.name?.split('.').pop() === 'png' || props.picture?.name?.split('.').pop() === 'svg') ?
                                <img className="add_img" src={props.prePicture} alt="" />
                                :
                                <img className="add_img" src={No_Image} alt="flashcart_image" />
                        )
                        :
                        < AddIcon className="text_gray" fontSize="large" />

                }

            </label>

            <input
                id={props.id}
                type="file"
                onChange={(e) => props.Picturehandler(e)}
                accept=".jpg , .png" //png bayad begire hatman baraye test jpg gozashte shode
                hidden
            />
        </>
    )
}
