import React, { useEffect, useState } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";

export default function Autocomplete_Addword(props) {
  // State
  const [chosenWord, setChosenWord] = useState([]);

  // functions
  const onChangeHandler = (e, value) => {
    setChosenWord(value);
    props.setSelectedWord(value);
  };

  // useEffect(() => {
  //     setChosenWord(props.meaning);
  // }, [])

  return (
    <>
      <Autocomplete
        defaultValue={props.meaning && props.meaning.map((option) => option)}
        value={props.value}
        onChange={(e, v) => onChangeHandler(e, v)}
        className="rtl flashcart_taginput w-100"
        multiple
        id="tags-filled"
        options={chosenWord && chosenWord.map((option) => option)}
        freeSolo
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant="outlined"
              label={option}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            className="placeholder_style"
            {...params}
            variant="filled"
            placeholder={"معانی کلمه را تایپ کنید، سپس دکمه Enter را فشار دهید"}
          />
        )}
      />
    </>
  );
}
